#include "progressivephotonmapper.h"

#include "defines.h"
#include "scene.h"
#include "camera.h"
#include "ray.h"
#include "intersection.h"
#include "material.h"
#include "timer.h"
#include "image.h"

#include "randomgenerator.h"

#include "sampler.h"
#include "arealight.h"
#include "pointlight.h"
#include "bvhaccelerator.h"
#include "hitpoint.h"

ProgressivePhotonMapper::ProgressivePhotonMapper(Scene *scene, Image *img)
   : Raytracer(scene,img)
{
   mBVHAccelerator = 0;

   mHitPoints.clear();
}

void ProgressivePhotonMapper::forwardTracingPass()
{
  std::cout << "Do the forward tracing pass." << std::endl;

  Timer forwardTracingTimer;

  //get the image width and height
  int width = mImage->getWidth();
  int height = mImage->getHeight();

  Color color;

  //loop over all the pixels in the image
  //and do the forward ray tracing
  //and register the sphere shaped sampling
  //region at each intersections
  //the function takes the pixel index as the
  //input parameter to that it remembers
  //which pixel it belongs to and gather flux
  //from photons interacting near the hit point
#pragma omp parallel for schedule(dynamic,1)
  for(int y = 0; y < height; ++y)
    {
      for(int x = 0; x < width; ++x)
	{
	  //send the pixel index
	  //to create the camera 
	  //ray - primary ray
	  //from eye
	  color = traceFromEye(x,y);
	}
      
#pragma omp critical
      // Print progress approximately every 5%.
      if ((y+1) % (height/20) == 0 || (y+1) == height)
	std::cout << (100*(y+1)/height) << "% done" << std::endl;      
    } 

   std::cout << "Forward tracing pass done in " << forwardTracingTimer.stop() << " seconds" << std::endl;

   std::cout << "Total Hit Points collected: " << mHitPoints.size() << std::endl;


   //////////////////////////// DIAGNSTICS TEST /////////////////////////////

   /*
   std::ofstream outfile("hitpoints.txt");
   std::ostream_iterator<HitPoint*> io(outfile);
   std::copy(mHitPoints.begin(),mHitPoints.end(),io);
   */

   //////////////////////////// DIAGNSTICS TEST /////////////////////////////

   std::cout << "Create the BVH with the hit points.." << std::endl;
   
   
   //initiate the bounding volume hierarchy out of all the hit points
   //to make the look up faster during the photon tracing passes
   mBVHAccelerator = new BVHAccelerator();
   mBVHAccelerator->buildForHitPoints(mHitPoints);
   
   std::cout << "BVH with the hit points is complete." << std::endl;
   

}


void ProgressivePhotonMapper::photonTracingPass()
{
  //now do the photon tracing multiple times
  
  //since we shall get the ray from the light source
  //we need to get the arealight first
  AreaLight *areaLight = mScene->getAreaLight();
  
  //make sure we have the area light
  assert(areaLight != 0);

  Ray photonRay;
  Color photonFlux;


    
  //now lets do the photon tracing   
  //the photon tracing passes is 512
  for(unsigned int passes = 1; passes <= PHOTON_PASSES; ++passes)
    {
      int photonBounceCounter = 0;
      
      fprintf(stderr,"\n");
      
      //each pass shoots 1 million
      //photons from the light source
      //and accumulate photon flux
      for(int photon = 0; photon < PHOTONS_PER_PASS;++photon)
	{
	  //get a sample ray from the pointlight
	  photonRay = areaLight->getSampleRay();

	  //get the point light's photon flux
	  photonFlux = areaLight->getFlux();
	  
	  //now do the photon tracing
	  photonFlux = traceFromLight(photonRay,
				      photonFlux,
				      photonBounceCounter);
	  
	  double j = 100.*(photon + 1)/PHOTONS_PER_PASS;
	  
	  fprintf(stderr, "\rPhotonsPerPass %5.2f%%", j);
	}  // end of number of photons per pass


      //add some bias after each pass to guarantee the convergance of the algorithm
      adjustRadiusAndFlux();
    } // end of number of passes

  //set the value for the current pass
  setPixelPerPass(PHOTON_PASSES * PHOTONS_PER_PASS);
  
}


void ProgressivePhotonMapper::adjustRadiusAndFlux()
{
   //the radius reduction and flux correction happens over here, i believe
   //in order to guarantee the convergence of the algorithm,
   //the radius of each hitpoint needs to be reduced after
   //each photon tracing pass. For this to work, both the total
   //flux and the number of photons need to be compensated
   
   float A;
   float B;

   float s = mHitPoints.size();
   
   for(unsigned int i = 0;i <  mHitPoints.size();++i)
   {
      {
	 A = mHitPoints[i]->mPhotonCount + mHitPoints[i]->mNewPhotonCount ;
	 B = mHitPoints[i]->mPhotonCount + ALPHA * mHitPoints[i]->mNewPhotonCount;
	 mHitPoints[i]->mRadius *= std::sqrt(B/A);
	 mHitPoints[i]->mTotalFlux *= (B/A);
	 mHitPoints[i]->mPhotonCount = B;
	 mHitPoints[i]->mNewPhotonCount = 0;
      }
      
      fprintf(stderr, "\rHitPoint Convergance Pass %5.2f%%", 100.*(i + 1)/s);      
   }
}

void ProgressivePhotonMapper::computeImage()
{

  //the single forward tracing pass
  forwardTracingPass();

  //multiple photon tracing pass
  //from the light source
  photonTracingPass();

  /*
      
      /*
      fprintf(stderr,"\n");
      
      std::cout << "Convergance of all the hitpoints... " << std::endl;

      //the radius reduction and flux correction happens over here, i believe
      //in order to guarantee the convergence of the algorithm,
      //the radius of each hitpoint needs to be reduced after
      //each photon tracing pass. For this to work, both the total
      //flux and the number of photons need to be compensated

      float A;
      float B;
      //#pragma omp parallel for schedule(dynamic, 1),private(A,B)
      for(unsigned int i = 0;i <  mHitPoints.size();++i)
      {
	//#pragma omp critical	 
	 {
	    A = mHitPoints[i]->mPhotonCount + mHitPoints[i]->mNewPhotonCount ;
	    B = mHitPoints[i]->mPhotonCount + ALPHA * mHitPoints[i]->mNewPhotonCount;
	    mHitPoints[i]->mRadius *= std::sqrt(B/A);
	    mHitPoints[i]->mTotalFlux *= (B/A);
	    mHitPoints[i]->mPhotonCount = B;
	    mHitPoints[i]->mNewPhotonCount = 0;
	 }

	 float s = mHitPoints.size();
	 fprintf(stderr, "\rHitPoint Convergance Pass %5.2f%%", 100.*(i + 1)/s);

      }


      fprintf(stderr,"\n");
      */

  /*
      double p = 100.*(passes+1)/PHOTON_PASSES;
      fprintf(stderr, "\rPhotonPass %5.2f%%", p);
  

   }

*/
}


void ProgressivePhotonMapper::setPixelPerPass(unsigned int emittedPhotons)
{
  //photon tracing is complete
  //now we go through each of 
  //the hitpoint and accumulate
  //the radiance in the pixel
  //the hit point belongs to
  
  Color totalFlux;
  Color directIllumination;
  float pixelWeight;
  float radius;
  float photonCount;
  
  Color pixelColor;
  
  for(unsigned int i = 0; i < mHitPoints.size();++i)
    {
      int x = mHitPoints[i]->mPixelX;
      int y = mHitPoints[i]->mPixelY;
      
      totalFlux = mHitPoints[i]->mTotalFlux;
      directIllumination = mHitPoints[i]->mDirectIllumination;
      pixelWeight = mHitPoints[i]->mPixelWeight;
      radius = mHitPoints[i]->mRadius;
      
      pixelColor = (totalFlux / (emittedPhotons * M_PI * radius * radius ) + directIllumination ) * pixelWeight;
      
      mImage->setPixel(x,y,pixelColor);
    }
  
  fprintf(stderr,"\n");
}


//the very first call of the following function will contain the light
//flux from the light
Color ProgressivePhotonMapper::traceFromLight(const Ray &photonRay,
					      const Color &photonFlux,
					      int photonBounceCounter)
{
  //variable to hold the photon intersection 
  //definitely in the world space
  Intersection intersection;
  
  //declare a random instance  
  RandomGenerator random = RandomGenerator::getRandomInstance();
  
  //declare a good starting point as follows
  //which means that there is a 90% chance of continuing
  //and 10% chance of getting absorbed
  float alpha = 0.1;
  
  //decide if the photon bouncing should continue or not
  float probability = 0.0f;
  
  //the variable that stores the reflectivity and
  //refractivity coefficient constants
  float reflectivityCoefficient = 0.0;
  float refractivityCoefficient = 0.0;
  
  //value decides which type of ray to trace
  float whichRayToTrace = 0.0f;
  
  Ray reflectedRay;
  Ray refractedRay;
  
  Color reflectedFlux;
  Color refractedFlux;
  Color directFlux;
  Color outFlux;


  //first of all get the nearest hitpoints
  std::vector<HitPoint*> nearestHitPoints;
  
  //check if the scene is intersected
  bool hit = mScene->intersect(photonRay,intersection);
  
  
  if(hit)
    {
      //std::cout << "We got the hit" << std::endl;
      
      //we have got the scene intersection
      
      //get the reflectivity coefficient of the nearest hit in the scene object
      reflectivityCoefficient = intersection.mMaterial->getReflectivity(intersection);
      
      //get the refractivity coefficient of the nearest hit in the scene object
      refractivityCoefficient = intersection.mMaterial->getTransparency(intersection);
      
      //get the probability value
      //this random number will 
      //decide if the ray absorbed 
      //or should be continued
      probability =  static_cast<float>(random.generateDouble(0,1));
      
      
      //alpha is set to 0.1
      //if the generaed random number is greather than alpha
      //we continue bouncing 3 different ray
      //otherwise the ray is absorbed
      if(probability <  alpha)
	return Color(0.0f,0.0f,0.0f);
      else
	{		  
	  //get a canonical random number to decide which ray to trace
	  whichRayToTrace = static_cast<float>(random.generateDouble(0,1));
	  
	  if(whichRayToTrace < refractivityCoefficient)
	  {
	      //deal with the refraction
	      //refractedRay = intersection.getRefractedRay();
	      //refractedFlux = traceFromLight(refractedRay,photonFlux,++photonBounceCounter);	      
	  }
	  else if(whichRayToTrace < reflectivityCoefficient)
	  {
	     //deal with the specular reflection
	     //reflectedRay = intersection.getReflectedRay();
	     //reflectedFlux = traceFromLight(reflectedRay,photonFlux,++photonBounceCounter);	      
	  }
	  else if(whichRayToTrace < (1 - reflectivityCoefficient - refractivityCoefficient))
	  {
	     //diffuse surface
	     //what happens when the photon hits a diffuse surface
	     
	     nearestHitPoints.clear();
	     
	     //because the direct illumination is already known,
	     //the first bounce of the photon is ignored
	     //for the following bounces,
	     //the photon should determine overlapping hitpoints.
	     //here we use the BVH to quickly determine the overlapping
	     //hitpoint candidates
	     if(photonBounceCounter > 0)
	     {
		{
		   //we do not register the first photon bounce
		   //the photon's flux is accumulated in the overlapping
		   //hitpoints
		   if(mBVHAccelerator->foundNearestHitPoints(intersection,
							     nearestHitPoints))
		   {
		      
		      HitPoint *hitPoint = 0;
		      
		      //browse through all the nearest
		      //hit points and accumulate
		      //the flux
		      for(unsigned int i = 0; i < nearestHitPoints.size();++i)
		      {
			 
			 //get one of the nearest  hitpoints from 
			 //the hitpoint array we have accumulated during 
			 //the forward tracing pass
			 //nearestHitPoints have the indexes of the 
			 //nearest hit point
			 hitPoint = nearestHitPoints[i];
			 
			 Point3D pointWithDirection = hitPoint->mIntersection.mPosition - intersection.mPosition;
			   
			 float distance = Vector3D(pointWithDirection).length();
			 
			 float normalFacingEachOther = hitPoint->mIntersection.mNormal.dot(intersection.mNormal);
			 
			 if(distance <= hitPoint->mRadius &&
			    normalFacingEachOther > 1e-3)
			 {
			    //increment the photon count for this pass
			    hitPoint->mNewPhotonCount += 1;
			    
			    //accumulate the flux from the nearest photons
			    hitPoint->mTotalFlux += photonFlux * hitPoint->mIntersection.mMaterial->evalBRDF(hitPoint->mIntersection,photonRay.dir); 
			 }
		      } //end of the nearest hit point traversal
		   }
		} // end of the omp directive
	     } // end of the bounce counter condition	       
	     
	       //deal with the diffuse reflection	     	      
	       //scatter is diffusely again
	     
	       //when the photon hits the diffuse surface
	       //it scatters in the random direction
	     
	     float pdf = 0.0;
	     
	     //do the diffuse scatter here
	     Vector3D importanceSampleVector = sampleCosine(intersection,pdf);
	     
	     if(pdf > 0.0f)
	     {
		Ray newRay(intersection.mPosition + intersection.mNormal * 0.001f, importanceSampleVector,1e-4f);
		
		double incidentAngle = std::max(intersection.mNormal.dot(newRay.dir),0.0f);
		
		//calculate the photon flux after the diffuse scatter
		//directFlux = ( traceFromLight(newRay,photonFlux,++photonBounceCounter) * intersection.mMaterial->evalBRDF(intersection,newRay.dir) * incidentAngle ) / pdf;
		
		//photon flux after diffuse scatter
		directFlux = ( photonFlux * intersection.mMaterial->evalBRDF(intersection,newRay.dir) * incidentAngle ) / pdf;		  
	       }	      	      
	  } //end of the diffuse bounce condition
	  
	  //accumulate all the flux
	  /*
	  outFlux = directFlux * (1 - reflectivityCoefficient - refractivityCoefficient) +
	     (reflectedFlux * reflectivityCoefficient) +
	     (refractedFlux * refractivityCoefficient);
	  */
	  
	  outFlux = directFlux;
	}   //end of the absorption probability condition
      
      //the output must be divided by (1-alpha)
      //to get the correct result
      outFlux /= (1 - alpha);
    } // end of the hit condition
  
  return outFlux;
}

Vector3D ProgressivePhotonMapper::sampleCosine(const Intersection& is, float& pdf) const
{
   
   Vector3D d;
   
   //get the random instance
   RandomGenerator random = RandomGenerator::getRandomInstance();

   float random_phi = 2 * M_PI * static_cast<float>(random.generateDouble(0,1));
   
   //generate  a random number between 0 and 1
   float random_theta = static_cast<float>(random.generateDouble(0,1));
   

   float x = cos(random_phi) * std::sqrt(random_theta);
   float y = sin(random_phi) * std::sqrt(random_theta);
   float z = std::sqrt(std::max(0.0f,1 - random_theta));


   
   Vector3D w = is.mNormal;
   Vector3D u = (( fabs(w.x) > .1 ? Vector3D(0,1,0) : Vector3D(1,0,0)) % w).normalize();
   Vector3D v = w % u;

   d =( u * x + v * y + w * z ).normalize();
   


   float cos_theta = std::max(d.dot(is.mNormal),0.0f);
   
   //cosine lobe pdf!
   pdf = cos_theta/M_PI;

   return d;     
}

//single pass ray tracing
Color ProgressivePhotonMapper::traceFromEye(int x,int y)
{    
   Color pixelColor;
   Point3D samplePoint;
   
   float px,py;
   Ray ray;
   
   
   //get the number of samples
   unsigned int numSamples = mSampler->getNumberSamples();
   
   float subPixelWeight = 1/(static_cast<float>(numSamples));
   
   for(unsigned int i = 0; i < numSamples; i++)
   {
      //get the sample point in the unit square
      samplePoint = mSampler->sampleUnitSquare();
      
      //update the pixel point to one of the sample points
      //generated in random manner with the chosen sampling 
      //technique
      px = x + samplePoint.x;
      py = y + samplePoint.y;
      
      //create the primary ray through the sample point
      //each new sample create a new ray 
      ray = mCamera->getRay(px,py);
      
      //call the trace function with the primary ray
      //owner pixel index for the subpixel
      //,weight for the subpixel and depth
      pixelColor += trace(ray,x,y,subPixelWeight,1);
      
   }
   
   //average the radiance value with the number of samples taken
   pixelColor /= numSamples;
   
   return pixelColor;
}

Color ProgressivePhotonMapper::trace(const Ray &ray,
				     const int &x,
				     const int &y,
				     const float &weight,
				     int depth)
{
  Color outputColor;
  
  //declare the intersection
  //for the nearest intersection
  //inside the scene
  Intersection intersection;
  
  
  //check the intersection and store the
  //intersection information
  bool hit = mScene->intersect(ray,intersection);
  
  //dummy light vector for the emissive material
  Vector3D dummyLightVec;

  HitPoint *hitPoint = 0;

  if(hit)
    {
       //float reflectivityCoefficient = intersection.mMaterial->getReflectivity(intersection);

       //float refractivityCoefficient = intersection.mMaterial->getTransparency(intersection);

      Color directColor;
      Color reflectedColor;
      Color refractedColor;

      
      /*
      if(reflectivityCoefficient > 0.0f)
	{
	  if(depth != 0)
	    {
	      Ray reflectedRay = intersection.getReflectedRay();
	      reflectedColor += trace(reflectedRay,x,y,weight,depth - 1);
	    }
	}

      if(refractivityCoefficient > 0.0f)
	{
	  if(depth != 0)
	    {
	      Ray refractedRay = intersection.getRefractedRay();
	      refractedColor += trace(refractedRay,x,y,weight,depth - 1);
	    }
	}

      */
      outputColor = ((intersection.mMaterial->emissive()) ? ( intersection.mMaterial->evalBRDF(intersection,dummyLightVec) * intersection.mMaterial->getEmissionFactor()) : Color(0.0f,0.0f,0.0f));

      //direct color for diffuse reflection
      directColor = /* (1 - reflectivityCoefficient - refractivityCoefficient) * */ computeDirectIllumination(intersection);

      //enable the reflection and refraction in whitted style
      //reflectedColor = reflectivityCoefficient * reflectedColor;
      //refractedColor = refractivityCoefficient * refractedColor;

      //accumulate the color after the inclusion of the whitted style
      //reflection and reflraction
      outputColor += directColor  /* + reflectedColor + refractedColor */;


      //we have accumulated the weighted radiance - the direct one 
      //noe initiate the hit point and store them
      hitPoint = new HitPoint();

      //register the intersection
      hitPoint->mIntersection = intersection;
      //register the owner pixel index
      hitPoint->mPixelX = x;
      hitPoint->mPixelY = y;
      
      //store the weight of the pixel
      hitPoint->mPixelWeight = weight;
      
      //register the initial radius 
      hitPoint->mRadius = PHOTON_RADIUS;

      //register the direct illumination
      hitPoint->mDirectIllumination = outputColor;
      
#pragma omp critical 
      mHitPoints.push_back(hitPoint);
    }
  else
    outputColor = Color(0.0f,0.0f,0.0f);

  return outputColor;
}

Color ProgressivePhotonMapper::computeDirectIllumination(const Intersection &intersection)
{

  //declare the color to store the radiance
  //from the direct illumination
  Color outputColor(0.0f,0.0f,0.0f);
   
  ///////////////// POINT LIGHT ////////////////
  //pointer to the point light

  /*
  PointLight *pointLight;


  //get the point light from the scene
  pointLight = mScene->getLight(0);
  
  Ray shadowRay;
  Vector3D lightVec;
  float incidentAngle;
  bool shadowHit = false;
  Color incomingRadiance, BRDF;
  
  shadowRay = intersection.getShadowRay(pointLight);


  //check if the shadow ray intersect 
  //any intersectables in the scene
  //before reaching the light source
  shadowHit = mScene->intersect(shadowRay);


  if(!shadowHit)
  {
     //get the incoming radiance of the light
     incomingRadiance = pointLight->getRadiance();
     
     //calculate the light vector - the irradiance
     lightVec = pointLight->getWorldPosition() - intersection.mPosition;
     lightVec.normalize();
     
     //calculate the incident angle
     incidentAngle = std::max(lightVec.dot(intersection.mNormal),0.0f);
     
     //get the BRDF
     BRDF = intersection.mMaterial->evalBRDF(intersection,lightVec);
     
     //accumulate the color for all the lights 
     outputColor += incomingRadiance * BRDF * incidentAngle;
  }
  */
  ///////////////// POINT LIGHT ////////////////
  


  ///////////////////// AREA LIGHT /////////////////////////

  //pointer to the area light
  AreaLight *areaLight;
  
  //incoming the radiance is basically the emissive radiance from the direct light source
  Color incomingRadiance,BRDF;
  
  Intersection lightIntersection;
  Ray lightRay;
  
  //initialize the shadow hit to false
  bool lightHit = false;
  
  
  //get the area light -  in  the scene
  //the area light interface one or sevral
  //luminaire intersectables
  areaLight = mScene->getAreaLight();

  if(!areaLight)
     return Color(0.0f,0.0f,0.0f);


  //now sample this area light
  //you might consider several samples from differetn light sources

  //get the ray that intends to intersect the luminaire
  //here we sample the light explicitly
  lightRay = intersection.getShadowRay(areaLight);
  
  //check if the shadow ray intersect the scene more specifically
  //the luminaire
  lightHit = mScene->intersect(lightRay,lightIntersection);
  
  //check if the shadow ray hit the luminaire or not
  if(lightHit && lightIntersection.mMaterial->emissive())
    {
      //get the probability density function
      float pdf = areaLight->getPDF(lightIntersection,intersection);
      
      if(pdf > 0.0f)
	{
	  //calculate the radiance 
	  //value only if the pdf value 
	  //is greater than 0
	  
	  pdf = 1.0f/pdf;

	  //calculate the visible point
	  Point3D visiblePoint = lightIntersection.mPosition - intersection.mPosition;
	  
	  //initialize a vector from the visible point
	  Vector3D vectorToLight(visiblePoint);
	  
	  //normalize the vector to the light
	  vectorToLight.normalize();
	  	  
	  //the emissive radiance from the light source
	  incomingRadiance = areaLight->getRadiance();
	  
	  //get the radiance the transfer value
	  float transfer = radianceTransfer(lightIntersection,intersection);
	  
	  //get the BRDF of the nearest interset intersection point
	  BRDF = intersection.mMaterial->evalBRDF(intersection,vectorToLight);
	  
	  //output radiance is the result of the BRDF * Direct incoming radiance from the light source
	  // * radiance transfer function * probability distribution function
	  outputColor = BRDF * incomingRadiance * transfer * pdf;
	}      
    }
  ///////////////////// AREA LIGHT ////////////////////////

  return outputColor;
}


//calculate the radiance transfer between the two visible points
float ProgressivePhotonMapper::radianceTransfer(const Intersection &shadowIntersection,const Intersection &intersection)
{
  
  //initialize the point from the light sample point and the shaded point in the scene
  Point3D visiblePoint = shadowIntersection.mPosition - intersection.mPosition;
  
  //initialize a vector from the vector
  Vector3D vectorToLight(visiblePoint);
  
  //get the length squared
  float lengthSquared = vectorToLight.length2();
  
  //normalize the vector to the light
  vectorToLight.normalize();
  
  //calculate the radiance transfer    
  //make sure to clamp the cosine to zero if they are negative
  float transfer = (std::max(intersection.mNormal.dot(vectorToLight),0.0f) * 
		    std::max(shadowIntersection.mNormal.dot(-vectorToLight),0.0f)) /  lengthSquared;  

  //make sure that the radiance transfer is positive
  return (transfer >= 0.0f) ? transfer : 0.0f;
}


ProgressivePhotonMapper::~ProgressivePhotonMapper()
{
   for(unsigned int i = 0; i < mHitPoints.size();++i)
   {
      if(mHitPoints[i])
      {
	 delete mHitPoints[i];
	 mHitPoints[i] = 0;
      }
   }

   mHitPoints.clear();

   if(mBVHAccelerator)
   {
      delete mBVHAccelerator;
      mBVHAccelerator = 0;
   }
   
}
