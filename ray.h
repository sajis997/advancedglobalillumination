/*
 *  ray.h
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-02-24.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */

#ifndef RAY_H
#define RAY_H


#include "matrix.h"
#include "defines.h"

/**
 * Class representing a ray in 3D space.
 * The ray's attributes are publicly accessible, and consist
 * of the ray origin and direction, plus parametric min/max
 * along the ray. A point along the ray can be found by taking
 * origin + t*dir, where t is the time parameter specifying how
 * far away from the origin we are.
 */
class Ray
{
public:
   
   struct Differential
   {
      Vector3D dx;
      Vector3D dy;
      
      Differential() {}
      Differential(const Vector3D& dx, const Vector3D& dy) : dx(dx), dy(dy) {}
   };
   
   Point3D orig;        ///< Origin of ray.
   Vector3D dir;	///< Direction of ray.   
   
   //the valid interval between each ray
   float minT;		///< Start time of ray.
   float maxT;		///< End time of ray.
   
   Differential dp;	///< Origin ray differential.
   Differential dd;	///< Direction ray differential.
   
   //THE FOLLOWING VARIABLES ARE DECLARED BY SAJJAD
   Vector3D invDir;
   int posNeg[3];
   
   
public:
   /// Default Constructor. The position and direction are left
   /// un-initialized, but the time is set to 0 and infinity respectively.
   /*
     the minT value has to be positive, 0 my cause the problem
   */
   Ray() : minT(0.001f), maxT(INF) { }
   
   Ray(const Point3D& o, const Vector3D& d, float mint=0.001f, float maxt=INF)
      : orig(o), minT(mint), maxT(maxt)
   {
      //the following calculates the direction, inverse direction
      //and flag for the inverse direction
      setDirection(d);
   }
   
   /// Constructor initializing the ray's origin and direction,
   /// and optionally the time parameters. The direction is normalized.
   Ray(const Point3D& o, const Vector3D& d, const Differential& dp = Differential(), const Differential& dd = Differential(), float mint=0.001f, float maxt=INF)
      : orig(o), dp(dp), dd(dd), minT(mint), maxT(maxt)
   {
      setDirection(d);
   }
   
   //THE FOLLOWING FUNCTIONS ARE ADDED BY SAJJAD
   
   //return the point at the parameter - t
   Point3D operator()(float t) const
   {
      return orig + dir * t;
   }
   
   Vector3D inverseDirection() const
   {
    return invDir;
  }
  
  void setDirection(const Vector3D &direction)
  {
    dir = direction; 
    dir.normalize();

    //calculate the inverse direction of the ray
    invDir = Vector3D(1.0f/dir.x,1.0f/dir.y,1.0f/dir.z);
    
    posNeg[0] = (dir.x > 0 ? 0 : 1);
    posNeg[1] = (dir.y > 0 ? 0 : 1);
    posNeg[2] = (dir.z > 0 ? 0 : 1);
  }
  
  /// Destructor.
  ~Ray() { }
};

#endif
