#ifndef COMPARE_TO_MID_H
#define COMPARE_TO_MID_H

#include "aabb.h"
#include "intersectable.h"
#include "bvhprimitiveinfo.h"

class CompareToMid
{      
   float centroid_value;
   int axis;
   
public:      
   CompareToMid(float d,int a)
      : centroid_value(d),
	axis(a)
   {
      
   }
   
   bool operator()(BVHPrimitiveInfo *a)
   {
     //get the centroid value of the intersectable
     //float ca = (a->mBounds.mMin(axis) + a->mBounds.mMax(axis)) * 0.5;
      
     return a->mCentroid(axis) < centroid_value;
   }
};

#endif
