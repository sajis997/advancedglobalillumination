#include "linearkdtreenode.h"


LinearKDTreeNode::LinearKDTreeNode()
{

}

LinearKDTreeNode::~LinearKDTreeNode()
{

}

//get the split axis
unsigned int LinearKDTreeNode::getSplitAxis() const
{
   return mSplitAxis;
}

//check if it is the leaf node or not
bool LinearKDTreeNode::isLeaf() const
{
   return mLeaf;
}

//get the index to the right child node
unsigned int LinearKDTreeNode::getIndex() const
{
   return mIndex;
}

//get the bounding box of the node
AABB& LinearKDTreeNode::getAABB()
{
   return mBbox;
}

//set the bounding box for the node
void LinearKDTreeNode::setAABB(AABB &box)
{
   mBbox = box;
}

void LinearKDTreeNode::makeNode(float splitPos, unsigned int splitAxis, unsigned int index)
{
   mSplitPos = splitPos;
   mSplitAxis = splitAxis;
   mIndex = index;
   mLeaf = false;
}

void LinearKDTreeNode::makeLeaf(unsigned int index)
{
   mIndex = index;
   mLeaf = true;
}
