#ifndef AREALIGHT_H
#define AREALIGHT_H

#include "defines.h"
#include "node.h"
#include "intersectable.h"
#include "material.h"

class Mesh;

class AreaLight : public Node
{
 public:
  
  //the area light contains the primitive
  //the prepare() function for the  primitive
  //takes care of the coordinate transformation
  AreaLight();
   
  virtual ~AreaLight();
  
  
  //get the radiance for the point light
  Color getRadiance() const;
  
  //calcualate the world sample position
  //based on the intersection point
  //we send the intersection point in the
  //world coordinate system
  bool calculateSampleDirection(const Intersection&,Vector3D&);
  
  
  //get the probability density function
  //which is already calculated in the prepare function
  float getPDF(const Intersection&,
	       const Intersection&) const;
  
  
  Color getFlux();
  
  //add an intersectable to behave as an arealight 
  void addToAreaLight(Intersectable*,Material*);
  
  //add a mesh to behave as a area light
  //mesh in turn contains intersectable
  void addToAreaLight(Mesh*);
  
  //generate photon ray from the light source
  //mainly used in photon mapping
  Ray getSampleRay();
  
  
  //return the number of luminaire intersectables
  unsigned int getNumberLuminaireIntersectables() const
  {
    return mLightIntersectables.size();
  }
  
  
  class LuminaireProbability
  {
  public:
    
  LuminaireProbability() : mIntersectable(0),mMaterial(0),mLuminaireProbabilityValue(0.0f),
      mCumulativeDistributionValue(0.0f)
	{
	}
    
    ~LuminaireProbability()
      {
	mIntersectable = 0;
	mMaterial = 0;
      }
    
    //luminaire as an intersectable
    Intersectable *mIntersectable;
    
    //material of the intersectable
    Material *mMaterial;
    
    //probability of choosing the luminaire
    float mLuminaireProbabilityValue;
    
    //cumulative distribution value
    float mCumulativeDistributionValue;
    
  };


  class CompareAreaLight
  {
  public:
    bool operator()(LuminaireProbability *a, LuminaireProbability *b)
    {
      return a->mLuminaireProbabilityValue < b->mLuminaireProbabilityValue;
    }
  };
  
  class CompareToCDF
  {
  public:
    
    //store the canonical random value
    float mRandomValue;
    
    bool operator()(LuminaireProbability *a)
    {
      return   mRandomValue < a->mCumulativeDistributionValue;
    }
  };
  
  /*
    prepare() will browse through all the emissive intersectables
    that will behave as light source ans calculate their probabilistic 
    weight
  */
  void prepare();
  
  
  //the area light contains the reference
  //to the geometric object to be the light source
  //the geometric object in turn contains the emissive material
  //thus providing the illumination
  //in this way the type of geometric object automatically determines
  //the type of light, in principle it allows any type of the geometric
  //object to be a light source
  //SOME OF THE ABOVE STATEMENTS DEMANS CHANGE
  std::vector<LuminaireProbability*> mLightIntersectables;
  
  //store the selected lumianire from the vector
  LuminaireProbability *mSelectedLuminaire;
    
  Color mFlux;
  
};




#endif
