#include "emissive.h"
#include "defines.h"
#include "intersection.h"

Emissive::Emissive(const Color &e,float sc, float r,float t, float n)
  : Material(r,t,n),
    ce(e), // emissive color
    ls(sc) // scaling factor for the emissive color
{

}


Emissive::~Emissive()
{

}

Color Emissive::evalBRDF(const Intersection& is, const Vector3D& L)
{
  Color emissiveColor;
  
  //do the dot product of the following two vectors
  if(-is.mNormal * is.mRay.dir > 0.0f)
    //the ultimate emissive color is multiplication
    //of emissive scaling factor and emissive color
    emissiveColor = ce * ls;
  else 
    emissiveColor = Color(0.0f,0.0f,0.0f);
  
  return emissiveColor;
}

