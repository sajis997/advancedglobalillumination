#ifndef BVH_ACCELERATOR_H
#define BVH_ACCELERATOR_H


#include <stack>
#include "rayaccelerator.h"

class BVHNode;

class BVHAccelerator : public RayAccelerator
{
private:

	struct StackItem
	{
		BVHNode *nodePtr;

		//hit time
		float t;
	};

	std::stack<StackItem> m_nodeStack;

	//the intersectable objects
	std::vector<Intersectable> m_objects;

	void build_recursive();
};

#endif
