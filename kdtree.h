#ifndef KDTREE_H
#define KDTREE_H

#include "defines.h"
#include "matrix.h"
#include "linearkdtreenode.h"
#include "timer.h"
#include "comparepoints.h"

#include <map>
#include <algorithm>
#include <vector>
#include <stack>

using namespace std;

template <typename T>
class KDTree
{
public:

   KDTree();

   ~KDTree();

   void build(const std::map<Point3D,T>&);

   unsigned int size() const;


   // bool contains(const Point<N>& pt) const;
   // Usage: if (kd.contains(pt))
   // ----------------------------------------------------
   // Returns whether the specified point is contained in the KDTree.
   bool contains(const Point3D& pt) const;

   
private:

   //the first two parameters define the boundary of the node
   //the third parameter define the depth of the tree
   unsigned int build_recursive(unsigned int, unsigned int, unsigned int);

   //the following map variable is the local reference to the
   //point and element type map which will be recieved from the function call
   std::map<Point3D,T> mKDTreeData;

   //the following vector is populated inside the build function
   //vector of pointers is helpful while buiding up the tree along
   //with the balancing feature
   std::vector<Point3D*> mKDTreePointList;
   
   std::vector<LinearKDTreeNode*> mKDTreeNodeArray;
};


template <typename T>
KDTree<T>::KDTree()
{
   //clear all the containers
   mKDTreeData.clear();

   mKDTreePointList.clear();

   mKDTreeNodeArray.clear();
}

template <typename T>
KDTree<T>::~KDTree()
{
   
   for(unsigned int i = 0 ; i < mKDTreePointList.size();++i)
   {
      if(mKDTreePointList[i])
      {
	 delete mKDTreePointList[i];
	 mKDTreePointList[i] = NULL;
      }
   }

   mKDTreePointList.clear();
   

   for(unsigned int i = 0; i < mKDTreeNodeArray.size();++i)
   {
      if(mKDTreeNodeArray[i])
      {
	 delete mKDTreeNodeArray[i];
	 mKDTreeNodeArray[i] = NULL;
      }
   }

   mKDTreeNodeArray.clear();
}


template <typename T>
void KDTree<T>::build(const std::map<Point3D,T> &treeData)
{
   //declare a timer object
   //to track the time of BVH construction
   Timer kdtreeTimer;

   //declare an iterator to browse through the map container
   typename std::map<Point3D,T>::const_iterator iter = treeData.begin();

   //browse through  the map contents and insert the key of the map
   //into the vector
   while(iter != treeData.end())
   {
      //store all the points in the map key into the vector
      mKDTreePointList.push_back(const_cast<Point3D*>(&((*iter).first)));
      
      ++iter;
   }


   //reserving the memory for the node
   //each point will have the node
   mKDTreeNodeArray.resize(mKDTreePointList.size(),NULL);   

   //now build the kdtree recursively
   unsigned int numOfNodes = build_recursive(0,mKDTreeNodeArray.size(),0);


   std::cout << "BVH construction done in " << kdtreeTimer.stop() << " seconds" << std::endl;
}

template <typename T>
unsigned int KDTree<T>::build_recursive(unsigned int left_index, unsigned int right_index, unsigned int depth)
{
   assert(left_index != right_index);

   //initialize the new kdtree node
   //yet to decide if the node will be the leaf node
   //or interior node
   LinearKDTreeNode *node = new LinearKDTreeNode();

   //declare a bounding box to hold the bounding box
   //for the points
   AABB pointsBound;
   
   //calculate the bounding box between the left_index and right_index
   //initially it will contain all the points in the scene
   for(unsigned int i = left_index; i < right_index; ++i)
   {
      pointsBound.include(*mKDTreePointList[i]);
   }

   //get the number of points between the left index and
   //right index
   unsigned int numberOfPoints = right_index - left_index;


   //set the bounding box of the current node
   node->setAABB(pointsBound);


   //insert the current node in the node list
   //the memory is already allocated
   mKDTreeNodeArray[depth] = node;


   //this is where we decide if the newly created
   //will be the interior node or the leaf node
   if(numberOfPoints <= 1)
   {
     //flag the node as the leaf node
     node->makeLeaf(left_index);
   }
   else
   {
     //we can partition the node even further

     AABB bound;

     //browse through the points array between
     //left_index and right_index and calculate
     //the bounding box of the points
     for(unsigned int i = left_index; i < right_index; ++i)
       {
	 bound.include(*mKDTreePointList[i]);
       }

     //get the larrgest axis of the bound
     unsigned int splitAxis = bound.getLargestAxis();


     //function object to compare points
     ComparePoints compoints;
     compoints.mSortingDimension = splitAxis;


     //get the split index
     unsigned int split_index = (left_index + right_index) / 2;

     std::nth_element(mKDTreePointList.begin() + left_index,
		      mKDTreePointList.begin() + split_index,
		      mKDTreePointList.begin() + right_index,
		      compoints);

     //declare a static stack to get hold of the parent
     static stack<unsigned int> parentStack;

     //push the depth of the stack
     //store that depth so that we can get back here
     parentStack.push(depth);

     //now recurse over the left part of the tree and left node
     //will always reside next to the parent node and we do not
     //need to do any explicit indexing into it
     depth = build_recursive(left_index,split_index,depth + 1);

     //we then continue with the right part of the tree
     
     //retrieve the depth level
     unsigned int p = parentStack.top();

     //remove the top stack item
     parentStack.pop();

     //get the point in the largest axis
     Point3D *point = mKDTreePointList[split_index];
     
     //initiate the node as interior node
     //split value , split axis and the index to the right child
     //for every split operation we assign the split value and
     //the split axis
     mKDTreeNodeArray[p]->makeNode((*point)(splitAxis),splitAxis,depth + 1);
          
     depth = build_recursive(split_index,right_index,depth + 1);     
     
   }

   return depth;
   
}

template<typename T>
unsigned int KDTree<T>::size() const
{
   return mKDTreeNodeArray.size();
}


#endif
