/*
 *  checker.h
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-03-07.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */
#ifndef __CHECKER_H__
#define __CHECKER_H__

#include "material.h"	
/** Class representing a simple checkerboard material. 
*   This material takes pointers to two other materials in the constructor,
*   and alternately chooses between them based on the texture coordinates.
*   Hence, it gives a square pattern, where the odd/even squares are made of 
*   different materials. It can be used for creating checker boards.
*   The reflectivity and transparency at an intersection point is determined
*   by the material that is used for the square the ray hit. However, only
*   one index of refraction is used for the entire material, and must be
*   set on the Checker material instead of its sub-materials.
*/
class Checker : public Material
{
public:
	Checker(Material* m1, Material* m2, int nu, int nv);
	virtual ~Checker();
	
	Color evalBRDF(const Intersection& is, const Vector3D& L);	
	float getReflectivity(const Intersection& is) const;
	float getTransparency(const Intersection& is) const;
	void setReflectivity(float r);
	void setTransparency(float t);

protected:
	int getType(const Intersection& is) const;
	
protected:
	Material* mMat[2]; ///< The two materials.
	int mNumU;	   ///< Number of squares along u.
	int mNumV;	   ///< Number of squares along v.
};

#endif // __CHECKER_H__
