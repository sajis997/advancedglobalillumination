#ifndef PHOTON_H
#define PHOTON_H

#include "intersection.h"


class Photon
{
public:
   Intersection intersection;
   Color flux;
};


#endif
