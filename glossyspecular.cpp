#include "glossyspecular.h"


GlossySpecular::GlossySpecular()
   : BRDF(),
     ks(0.0f),
     cs(Color(1.0f,1.0f,1.0f))
{

}

GlossySpecular::GlossySpecular(const GlossySpecular &rhs)
   : BRDF(rhs),
     ks(rhs.ks),
     cs(rhs.cs),
     exp(rhs.exp)
{

}

GlossySpecular& GlossySpecular::operator= (const GlossySpecular & rhs)
{
   if(this == &rhs)
      return (*this);

   BRDF::operator= (rhs);

   ks = rhs.ks;
   cs = rhs.cs;
   exp = rhs.exp;

   return (*this);
}

GlossySpecular::~GlossySpecular()
{

}


Color GlossySpecular::f(const Intersection& sr, const Vector3D& wo, const Vector3D& wi) const
{
   return Color(0.0f,0.0f,0.0f);
}
		
Color GlossySpecular::sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi) const
{
   return Color(0.0f,0.0f,0.0f);
}

Color GlossySpecular::sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi, float& pdf) const
{
   return Color(0.0f,0.0f,0.0f);
}
		
Color GlossySpecular::rho(const Intersection& sr, const Vector3D& wo) const
{
   return Color(0.0f,0.0f,0.0f);
}

//set the diffuse reflectivity
//the value is between [0-1]
void GlossySpecular::setReflectivity(const float reflectivity)
{
   ks = reflectivity;
}

float GlossySpecular::getReflectivity() const
{
   return ks;
}
