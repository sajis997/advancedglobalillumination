#ifndef PHOTON_TRACER_H
#define PHOTON_TRACER_H

/*

#include "defines.h"
#include "raytracer.h"
#include "matrix.h"
#include "kdtree.h"


class Ray;
class Intersection;


class PhotonTracer : public Raytracer
{
public:
   PhotonTracer(Scene *scene,Image *img);

   virtual void computeImage();


   ~PhotonTracer();

protected:

   void traceCausticPhoton(const Ray &ray,Color &color);
   void traceGlobalPhoton(const Ray &ray,  Color &color);

private:

   void preprocess();

   class Photon
   {
   public:
      Photon(const Vector3D &incident, const Color &color)
	 : mIncidentDirection(incident),
	   mAlpha(color)
      {

      }

      Vector3D mIncidentDirection;
      Color mAlpha;
      
   };

   //the following class represents photon close to the lookup point
   //we keep track of a photon close to the lookup point
   class CloserPhoton
   {
     public:
      const Photon *photon;
      //the squared distance from the photon to the lookup point
      //in order to more quickly be able to discard the farthest away
      //photon when a closer one is found
      float mDistanceSquared;
   };

   KDTree<Photon> mCausticsTree;

   KDTree<Photon> mRadianceTree;

   //all the photons are accumulated here
   //before sending to the tree
   std::map<Point3D,Photon> mCausticPhotonStorage;

   std::map<Point3D,Photon> mGlobalPhotonStorage;
};

*/

#endif
