#ifndef EMISSIVE_H
#define EMISSIVE_H

#include "material.h"

class Emissive : public Material
{
public:
   //constructor
   //color, intensity constant
   Emissive(const Color &e,float sc, float r = 0.0f,float t = 0.0f, float n = 1.0f);
   
   virtual ~Emissive();
   
   virtual Color evalBRDF(const Intersection& is, const Vector3D& L);
   
   
   virtual bool emissive() const
   {
      return true;
   }


   Color getEmissiveColor() const
   {
      return ce;
   }

   float getEmissiveScalingFactor() const
   {
      return ls;
   }
   
private:
   Color ce; // emissive color
   float ls; // radiance scaling factor
   
};


#endif
