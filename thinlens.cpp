#include "thinlens.h"
#include "sampler.h"
#include "ray.h"
#include "image.h"

ThinLens::ThinLens(Image *img)
  : Camera(img) // super class constructor
{
  mSampler = 0;
}

ThinLens::~ThinLens()
{

}

void ThinLens::setSampler(Sampler *sampler)
{

  if(mSampler)
    {
      delete mSampler;
      mSampler = NULL;
    }
  
  mSampler = sampler;
  mSampler->mapSamplesToUnitDisk();
}


void ThinLens::setRadius(float r)
{
  mRadius = r;
}

void ThinLens::setFocalPlaneDistance(float distance)
{
  mFocalPlaneDistance = distance;
}

Ray ThinLens::getRay(float x, float y) const
{
   
   //initiate the ortonormal frame corresponding to the image plane
   Vector3D right = 2.0f/(float)mImage->getWidth() * mImageExtentX * mRight;
   Vector3D up = -2.0f/(float)mImage->getHeight() * mImageExtentY * mUp;
   Vector3D view = mForward - mImageExtentX * mRight + mImageExtentY * mUp;
   
  //now sample to unit disk
  Point3D samplePointUnitDisk = mSampler->sampleUnitDisk();

  //scale the point within the disk
  Point3D lensPoint = samplePointUnitDisk * mRadius ;

  //initiate the new  origin of the ray 
  //which will be positioned randomly within the disk
  Point3D rayOrigin = mOrigin + lensPoint.x * right + lensPoint.y * up;

  
  //now find the ray direction 
  //hit point on the focal plane
  Point3D hitPoint;

  hitPoint.x = x * mFocalPlaneDistance / mNearPlane;
  hitPoint.y = y * mFocalPlaneDistance / mNearPlane;

  Vector3D d = view + (x - hitPoint.x) * right + (y - hitPoint.y ) * up;


  //length to the view plane from the eye
  float dLength = d.length();
  
  float r = 1.0f / (dLength*dLength*dLength);
  
  Ray::Differential dp(Vector3D(0.0f, 0.0f, 0.0f), Vector3D(0.0f, 0.0f, 0.0f));
  Ray::Differential dd((d.dot(d) * right - d.dot(right) * d) * r, (d.dot(d) * up - d.dot(up) * d) * r);
  
  d /= dLength;
  
  return Ray(rayOrigin, d, dp, dd, mNearPlane, mFarPlane);
  
}
