/*
 *  main.cpp
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-03-20.
 *  Copyright 2006 Lund University. All rights reserved.
 *
*/

#include <iostream>
#include "defines.h"
#include "scene.h"
#include "image.h"
#include "camera.h"
#include "mesh.h"
#include "sphere.h"
#include "diffuse.h"
#include "whittedtracer.h"
#include "pointlight.h"
#include "lightprobe.h"
#include "listaccelerator.h"
#include "bvhaccelerator.h"
#include "texture.h"
#include "phong.h"
#include "cornellscene.h"
#include "hdrprobescene.h"
#include "checker.h"
#include "pathtracer.h"
#include "photontracer.h"
#include "progressivephotonmapper.h"
#include "multijittered.h"
#include "jittered.h"
#include "thinlens.h"

#ifdef WIN32
#include <windows.h>
#endif

using namespace std;

/*
void buildTestScene0(Scene& scene)
{
	
	// Create materials: matte orange, shiny green, 
	// matte gray/white checkerboard, matte red/blue checkerboard.

	Diffuse* matteOrange = new Diffuse(Color(1.0f,0.7f,0.2f));
//	matteOrange->setReflectivity(0.2f);
//	matteOrange->setTransparency(0.5f);
//	matteOrange->setIndexOfRefraction(1.08f);
		
	Phong* shinyGreen = new Phong(Color(0.1f,1.0f,0.2f), Color(1, 1, 1), 50.0f);
//	shinyGreen->setReflectivity(0.5f);
//	shinyGreen->setTransparency(0.98f);
//	shinyGreen->setIndexOfRefraction(1.15f);

	Diffuse* matteBlack = new Diffuse(Color(0.1f, 0.1f, 0.1f));
	Diffuse* matteWhite = new Diffuse(Color(1.0f, 1.0f, 1.0f));
	Checker* matteBlackWhite = new Checker(matteBlack, matteWhite, 12, 12);
//	matteBlackWhite->setReflectivity(0.4f);
	
	Diffuse* matteRed  = new Diffuse(Color(1.0f, 0.1f, 0.1f));
	Diffuse* matteBlue = new Diffuse(Color(0.1f, 0.1f, 1.0f));
	Checker* matteRedBlue = new Checker(matteRed, matteBlue, 8, 8);
//	matteRedBlue->setReflectivity(0.2f);
//	matteRedBlue->setTransparency(1.0f);
//	matteRedBlue->setIndexOfRefraction(1.1f);

	
	// Create objects.
	// First we have a ground plane of size 40x40 in the gray/white checkerboard material.
	// Then we load a mesh (elephant.obj), which is scaled, rotated and translated to the
	// desired location. Finally, we add two spheres. A large in the back and a smaller
	// to the right.
	
	Plane* ground = new Plane(40, 40, matteBlackWhite);		// plane 40x40 units big

	Mesh* elephant = new Mesh("data/elephant.obj", matteOrange);	// load mesh
	elephant->setScale(0.8f);								// scale by a factor 0.8
	elephant->setRotation(0, -70, 0);						// rotate -70 degrees around Y
	elephant->setTranslation(Vector(4, 0, 14));				// translate by (4,0,14)
	
	Sphere* boll1 = new Sphere(4, matteRedBlue);			// sphere of radius 4
	boll1->setTranslation(Vector(13, 4, -2));				// move center to (13,4,-2)

	Sphere* boll2 = new Sphere(8, shinyGreen);				// sphere of radius 8
	boll2->setTranslation(Vector(-7, 8, -4));				// move center to (-7,8,-4)
	
	scene.add(ground);
	scene.add(elephant);
	scene.add(boll1);
	scene.add(boll2);

	
	// Create point light sources by specifying their position, color and intensity 
	// in that order. Three lights are created and added to the scene.
	
	PointLight* light1 = new PointLight(Point( 50, 50,  0), Color(1.0, 1.0, 1.0), 1.1f);
	PointLight* light2 = new PointLight(Point(-10, 60, 50), Color(1.0, 1.0, 1.0), 0.4f);
	PointLight* light3 = new PointLight(Point(-50, 30,-50), Color(1.0, 1.0, 1.0), 0.3f);

	scene.add(light1);
	scene.add(light2);
	scene.add(light3);
  
}

*/



static void buildTestScene(Scene &scene)
{
	// Create materials: matte orange, shiny green, 
	// matte gray/white checkerboard, matte red/blue checkerboard.

	Diffuse* matteOrange = new Diffuse(Color(1.0f,0.7f,0.2f));
	matteOrange->setReflectivity(0.2f);
	matteOrange->setTransparency(0.5f);
	matteOrange->setIndexOfRefraction(1.08f);
		
	Phong* shinyGreen = new Phong(Color(0.1f,1.0f,0.2f), Color(1, 1, 1), 50.0f);
	shinyGreen->setReflectivity(0.5f);
	shinyGreen->setTransparency(0.98f);
	shinyGreen->setIndexOfRefraction(1.15f);

	Diffuse* matteBlack = new Diffuse(Color(0.1f, 0.1f, 0.1f));
	Diffuse* matteWhite = new Diffuse(Color(1.0f, 1.0f, 1.0f));
	Checker* matteBlackWhite = new Checker(matteBlack, matteWhite, 12, 12);
	matteBlackWhite->setReflectivity(0.4f);
	
	Diffuse* matteRed  = new Diffuse(Color(1.0f, 0.1f, 0.1f));
	Diffuse* matteBlue = new Diffuse(Color(0.1f, 0.1f, 1.0f));
	Checker* matteRedBlue = new Checker(matteRed, matteBlue, 8, 8);
	matteRedBlue->setReflectivity(0.2f);
	matteRedBlue->setTransparency(1.0f);
	matteRedBlue->setIndexOfRefraction(1.1f);

	Diffuse *planeMaterial = new Diffuse(Color(1.0f,1.0f,1.0f));
	planeMaterial->setReflectivity(0.75f);

	
	// Create objects.
	// First we have a ground plane of size 40x40 in the gray/white checkerboard material.
	// Then we load a mesh (elephant.obj), which is scaled, rotated and translated to the
	// desired location. Finally, we add two spheres. A large in the back and a smaller
	// to the right.
	
	Mesh* ground = new Mesh("data/plane.obj", planeMaterial);
	ground->setScale(20.0f);


  
	Mesh* elephant = new Mesh("data/elephant.obj", matteOrange);	// load mesh
	elephant->setScale(0.8f);								// scale by a factor 0.8
	elephant->setRotation(0, -70, 0);						// rotate -70 degrees around Y
	elephant->setTranslation(Vector3D(10, 0, 14));				// translate by (4,0,14)
  

	/*
	Mesh *buddha = new Mesh("/media/chole/TrillionDollarBaby/Model/buddha.obj",matteOrange);
	buddha->setScale(11.5f);
	buddha->setRotation(0, 10, 0);						// rotate -70 degrees around Y
	buddha->setTranslation(Vector3D(11, 0, 14));				// translate by (4,0,14)
	*/
	
	Sphere* boll1 = new Sphere(4, matteRedBlue);			// sphere of radius 4
	boll1->setTranslation(Vector3D(13, 4, -2));				// move center to (13,4,-2)

	Sphere* boll2 = new Sphere(8, shinyGreen);				// sphere of radius 8
	boll2->setTranslation(Vector3D(-7, 8, -4));				// move center to (-7,8,-4)
	
	scene.add(ground);
	scene.add(elephant);
	//scene.add(buddha);
	scene.add(boll1);
	scene.add(boll2);

	
	// Create point light sources by specifying their position, color and intensity 
	// in that order. Three lights are created and added to the scene.

  	
	PointLight* light1 = new PointLight(Point3D( 50, 50,  0), Color(1.0, 1.0, 1.0), 1.1f);
	//PointLight* light2 = new PointLight(Point3D(-10, 60, 50), Color(1.0, 1.0, 1.0), 0.4f);
	//PointLight* light3 = new PointLight(Point3D(-50, 30,-50), Color(1.0, 1.0, 1.0), 0.3f);

	scene.add(light1);
	//scene.add(light2);
	//scene.add(light3);
  
}


static void buildElephant(Scene& scene)
{

   /* 
  PointLight *pointLight0 = new PointLight(Point3D(-50.0f, 60.0f, 20.0f), Color(0.4f, 0.7f, 0.7f));
  scene.add(pointLight0);
   */
   //PointLight *pointLight1 = new PointLight(Point3D(70.0f, 140.0f, -7.0f), Color(2.5f, 2.5f, 2.5f));
   PointLight *pointLight1 = new PointLight(Point3D(0.0f, -5.0f, 0.0f), Color(2.5f, 2.5f, 2.5f));
   scene.add(pointLight1);
  

  Diffuse *planeMaterial = new Diffuse(Color(1.0f,1.0f,1.0f));
  planeMaterial->setReflectivity(0.75f);
  
  
  
  Diffuse* matteBlack = new Diffuse(Color(0.1f, 0.1f, 0.1f));
  Diffuse* matteWhite = new Diffuse(Color(1.0f, 1.0f, 1.0f));
  Checker* matteBlackWhite = new Checker(matteBlack, matteWhite, 12, 12);
  matteBlackWhite->setReflectivity(0.4f);
  
  Mesh* plane = new Mesh("data/plane.obj", planeMaterial);
  plane->setScale(20.0f);
  plane->setTranslation(Vector3D(0, -10, 0));
  
  scene.add(plane);

  Diffuse *elephantMaterial0 = new Diffuse(Color(0.4f,0.7f,1.0f));
  //Phong *elephantPhongMaterial0 = new Phong(Color(0.4f,0.7f,1.0f),Color(1.0f,1.0f,1.0f),200.2f);
  //elephantMaterial0->setReflectivity(0.55f);
  // elephantPhongMaterial0->setReflectivity(0.55f);
  Mesh* elephant0 = new Mesh("data/buddha.obj", elephantMaterial0);
  elephant0->setScale(8.1f);
  elephant0->setRotation(0.0f, 10.0f, 0.0f);
  elephant0->setTranslation(Vector3D(-12, -10, 1));
  scene.add(elephant0);
  
//   Diffuse *elephantMaterial1 = new Diffuse(Color(0.4f,1.0f,0.7f));
   Phong *elephantPhongMaterial1 = new Phong(Color(0.7f,0.7f,0.7f),Color(1.0f,1.0f,1.0f),10.7f);   
   
   //elephantMaterial1->setReflectivity(0.20f);
   //elephantMaterial1->setTransparency(0.50f);
   //elephantMaterial1->setIndexOfRefraction(1.1f);
   elephantPhongMaterial1->setReflectivity(0.20f);
   elephantPhongMaterial1->setTransparency(0.70f);
   elephantPhongMaterial1->setIndexOfRefraction(1.1f);
   
   Mesh* elephant1 = new Mesh("data/buddha.obj", elephantPhongMaterial1);
   elephant1->setScale(10.35f);
   elephant1->setRotation(0.0f, 10.0f, 0.0f);
   elephant1->setTranslation(Vector3D(8.0f, -10, 1));
   scene.add(elephant1);


   Diffuse* matteRed  = new Diffuse(Color(1.0f, 0.1f, 0.1f));
   Diffuse* matteBlue = new Diffuse(Color(0.1f, 0.1f, 1.0f));
   Checker* matteRedBlue = new Checker(matteRed, matteBlue, 8, 8);

   Sphere* boll1 = new Sphere(4, matteRedBlue);
   //translate the boll1 in the local coordinate system
   boll1->setTranslation(Vector3D(0.0f,0.0f,20.0f));

   scene.add(boll1);


   /*
     By this time all the elements are still in the local coordinate system
     and are yet to be transformed into the global coordinate system
    */
}


static void buildSimple(Scene& scene, bool scrambled)
{
  PointLight *pointLight0 = new PointLight(Point3D(-20.0f, 20.0f, 60.0f), Color(1.5f, 1.5f, 1.5f));
  scene.add(pointLight0);
  // Setup material
  Diffuse *material = new Diffuse(Color(0.0f,0.2f,1.5f));
  material->setReflectivity(0.4f);
  // Add spheres
  for (int i = 0; i < 80; i++)
    {
      float x, y, z;
      
      if (scrambled)
        {
          x = -39.5f + (float) ((i * 13) % 80);
          y = -39.5f + (float) ((i * 7) % 80);
          z = -39.5f + (float) ((i * 29) % 80);
        }
      else
        {
          x = -39.5f + (float) (i % 80);
          y = -39.5f + (float) (i % 80);
          z = -39.5f + (float) (i % 80);
        }
      
      Sphere* sphere = new Sphere(1.0, material);
      sphere->setTranslation(Vector3D(x, y, z));
      scene.add(sphere);
    }
}


/**
 * Builds the "Spheres" scene.
 */
static void buildSpheres(Scene& scene)
{
   // Add lights  
   PointLight *pointLight1 = new PointLight(Point3D(20.0f, 240.0f, -7.0f), Color(1.5f, 1.5f, 1.5f));
   scene.add(pointLight1);
   
   
   // Setup materials
   Material *material[4];
   
   material[0] = new Diffuse(Color(1.0f,1.0f,1.0f));
   material[1] = new Diffuse(Color(1.0f,0.3f,0.2f));
   material[2] = new Diffuse(Color(0.0f,0.7f,0.1f));
   //material[3] = new Diffuse(Color(0.6f,0.6f,0.6f));
   
   material[3]= new Phong(Color(0.7f, 0.7f, 0.7f),
			  Color(0.9f, 0.9f, 0.9f),
			  100.001,0.7,0.1);
   
   
   //material[0]->setReflectivity(0.5f);
   //material[1]->setReflectivity(0.7f);
   //material[2]->setReflectivity(0.8f);
   //material[3]->setReflectivity(0.5f);
   
   material[0]->setTransparency(0.9f); //0.2
   material[1]->setTransparency(0.9f); //0.5
   material[2]->setTransparency(0.9f); //0.3
   //material[3]->setTransparency(0.8f);
   
   material[0]->setIndexOfRefraction(1.5f);
   material[1]->setIndexOfRefraction(1.5f);
   material[2]->setIndexOfRefraction(1.5f);
   //material[3]->setIndexOfRefraction(2.42f);
   
   // Add spheres
   for (int i = 0; i < 2 * 2 * 2; i++)
   {
      float x = (float) (-6 + 12 * (i & 1));
      float y = (float) (-6 + 12 * ((i >> 1) & 1));
      float z = (float) (-6 + 12 * ((i >> 2) & 1));
      Sphere* sphere = new Sphere(3.5, material[i % 3]);
      sphere->setTranslation(Vector3D(x, y, z));
      scene.add(sphere);
   }
   
   // NOTE: Remove this sphere from the scene when it's time to draw the plane!
   /*
     Sphere* sphere = new Sphere(200, material[3]);
     sphere->setTranslation(Vector3D(0, -210, 0));
     scene.add(sphere); // To be replaced by plane!
   */
   
   // Add plane
   
   Diffuse *planeMaterial = new Diffuse(Color(0.7f,0.7f,0.7f));
   planeMaterial->setReflectivity(0.2f);
   Mesh* plane = new Mesh("data/plane.obj", planeMaterial);
   
   plane->setScale(20.0f);
   plane->setTranslation(Vector3D(0, -10, 0));
   scene.add(plane);
   
   // TODO: Uncomment the following lines for the cliff-hanger assignment
   
   /*
     Diffuse *triSphereMaterial = new Diffuse(Color(1.0f,0.0f,1.0f));
     Mesh* triSphere = new Mesh("data/sphere.obj", triSphereMaterial);
     triSphere->setScale(5.0f);
     triSphere->setTranslation(Vector3D(0, 0, 0));
     scene.add(triSphere);
   */
}


/**
 * Changes the current working directory.
 */
static bool changeDirectory(const char* directory)
{
#ifdef WIN32
	return SetCurrentDirectory(directory) == TRUE;
#else
	return chdir(directory) == 0;
#endif
}

/**
 * Goes up the directory hierarchy until a folder named "data" is found.
 */
static void changeToRootDirectory()
{
	for (int i = 0; i < 64; i++) {
		bool found = changeDirectory("data");
		
		if (!changeDirectory(".."))
			break;
		
		if (found)
			return;
	}
	
	throw runtime_error("unable to find data directory");
}

/**
 * The entry point of the program.
 */
int main(int argc, char* const argv[])
{
   try
   {
      // Set working directory.
      changeToRootDirectory();
      
      //Build scene accelerator.
      BVHAccelerator accelerator;
      
      //declare the scene with to the accelerator
      Scene scene(&accelerator);

      //instantiate a sampler which will be used all over the ray tracer
      //the very same sampling technique is used to sample the light source
      MultiJittered sampler(4);

      
      //build the typical cornell scene
      buildCornellScene(&scene);
      
      //OR create the scene for the HDR rendering
      //buildHDRProbeScene(&scene);

      //buildTestScene(scene);

      
      // Create output image.
      Image output(128,128);
      
            
      Camera *camera = new Camera(&output);

      //////////////////////////////////////FOR THE TEST SCENE////////////////////////////////////////////////////
      /*
      Point3D pos = Point3D(22, 24, 26);				// camera position
      Point3D target = Point3D(0, 2, 0);				// camera target
      Vector3D up = Vector3D(0, 1, 0);				// up vector
      
      camera->setLookAt(pos, target, up, 52.0f);		// setup camera with fov 52 degrees

      //now create the HDR light from the environment map
      LightProbe lightProbe("data/OpenEXRimages/building_probe.exr");
      //set the background of the scene as the light probe
      scene.setBackground(&lightProbe);
      */
      //////////////////////////////////////FOR THE TEST SCENE////////////////////////////////////////////////////
      
      
      //set up the camera for the cornell scene
      setupCornellCamera(camera);
      
      //setupHDRProbeCamera(camera);

      //now create the HDR light from the environment map
      //LightProbe lightProbe("data/OpenEXRimages/stpeters_probe.exr");

      //set the background of the scene as the light probe
      //scene.setBackground(&lightProbe);      


      //add the camera to the scene
      scene.add(camera);
      
      scene.prepare();
      
      // Create a raytracer and start raytracing.
      std::cout << "creating ray tracer" << std::endl;

      //WhittedTracer rt(&scene,&output);      
      //PhotonTracer rt(&scene, &output);
      PathTracer rt(&scene, &output);
      //ProgressivePhotonMapper rt(&scene,&output);

      rt.setSampler(&sampler);
      rt.computeImage();
      
      // Save image.
      output.save("path.png");
      
      
   }
   catch (const std::exception& e) 
   {
      // Print the error and exit.
      std::cerr << "ERROR: " << e.what() << std::endl;
      exit(1);
   }
}
