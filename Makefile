CC = g++ 
CFLAGS = -fopenmp -std=gnu++0x -O2 -g
INCLUDE = -I/usr/include/OpenEXR
LDFLAGS = -LOpenEXR -lHalf -lImath -lIex -lIlmImf -lIlmThread -lz
COBJS=$(patsubst %.cpp,%.o,$(wildcard *.cpp))

EXE = raytracer

all: $(COBJS) 
	$(CC) $(CFLAGS) -o$(EXE) $(COBJS) $(LDFLAGS)

%.o : %.cpp 
	$(CC) $(CFLAGS) -o $@ -c $< $(INCLUDE) 


clean:
	rm -f $(EXE) *.o *~

