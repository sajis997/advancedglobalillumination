/*
 *  pointlight.cpp
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-02-28.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */

#include "defines.h"
#include "pointlight.h"
#include "randomgenerator.h"


using namespace std;
	
/**
 * Default constructor. Creates a white light at origin.
 */
PointLight::PointLight() : mPosition(Point3D(0,0,0)), mColor(Color(1,1,1)), mIntensity(1.0f)
{
}

/**
 * Constructs a light initialized to position p, color c, and intensity i.
 */
PointLight::PointLight(const Point3D& p, const Color& c, float i) : mPosition(p), mColor(c), mIntensity(i)
{
}

/**
 * Destructor. Does nothing.
 */
PointLight::~PointLight()
{
}

/**
 * Prepares the point light for rendering. Its cached attributes
 * (radiance, world position) are computed and stored locally.
 */
void PointLight::prepare()
{
   //calculate the radiance for the point light
   mRadiance = mIntensity * mColor;


   //calculate the flux for the point light
   mFlux = 4 * M_PI * mColor * mIntensity;
   
   //transform the light position from the light space 
   //to world space
   mWorldPos = mWorldTransform * mPosition;
}


Ray PointLight::getSampleRay() const
{
   //declare a random instance  
   RandomGenerator random = RandomGenerator::getRandomInstance();

   float x,y,z;
   
   do
   {
      float x = 2 *  static_cast<float>(random.generateDouble(0,1)) - 1;
      float y = 2 *  static_cast<float>(random.generateDouble(0,1)) - 1;
      float z = 2 *  static_cast<float>(random.generateDouble(0,1)) - 1;
   } while((x * x + y * y + z * z) > 1);


   //initiate a vector from the light's world position
   Vector3D normal = mWorldPos;

   //create the orthonormal basic to transfer the random direction into the world space
   //the random direction we create is in the local coordinate
   //and we transform the local coordinate to world coordinates
   Vector3D w = normal;
   Vector3D u = (( fabs(w.x) > .1 ? Vector3D(0,1,0) : Vector3D(1,0,0)) % w).normalize();
   Vector3D v = w % u;

   Vector3D d =( u * x + v * y + w * z ).normalize();   

   
   return Ray(mWorldPos,d,1e-4f );
      
}
