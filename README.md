# README #

This repository contains the source code that simulate the following effects by one of the photo-realistic rendering techniques (Path Tracing):
[Rendering Stories](http://photorealisticgeneration.blogspot.se/)

* Soft shadow.
* Caustics.
* Reflection & Refraction.
* Advanced Data-structure for ray-primitive intersection test.  

The skeleton of the source is extracted from Lund Graphics Group