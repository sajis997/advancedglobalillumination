/*
 *  triangle.cpp
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-02-22.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#include "defines.h"
#include "ray.h"
#include "triangle.h"
#include "mesh.h"
#include "sampler.h"
#include "randomgenerator.h"

/// Triangle overlap distance to avoid problems at edges.
static const float overlap = 1e-3f;

/**
 * Constructor.
 */
Triangle::Triangle() : mMesh(0), mSampler(0)
{
   
}

/**
 * Constructor initializing the triangle with the given vertices.
 */
Triangle::Triangle(Mesh* owner, const vertex& vtx1, const vertex& vtx2, const vertex& vtx3, Material *mtl) : mMesh(owner), mSampler(0)
{
   mVtx[0] = vtx1;
   mVtx[1] = vtx2;
   mVtx[2] = vtx3;
   mMaterial = mtl;
}

void Triangle::setSampler(Sampler *sampler)
{
   if(mSampler)
   {
      delete mSampler;
      mSampler = 0;
   }


   mSampler = sampler;
}


/**
 * Compute the triangle's face normal from the position of its vertices.
 * The vertices are in counterclockwise order, which mean the normal can
 * be computed as: n = (v1-v0) x (v2-v0).
 */
Vector3D Triangle::getFaceNormal() const
{
   Vector3D e1 = getVtxPosition(1) - getVtxPosition(0);	// find edge v0-v1
   Vector3D e2 = getVtxPosition(2) - getVtxPosition(0);	// find edge v0-v2
   Vector3D n = e1 % e2;				// n = e1 x e2
   n.normalize();
   return n;
}

/**
 * Return the area of the triangle.
 */
float Triangle::getArea() const
{
   Vector3D e1 = getVtxPosition(1) - getVtxPosition(0);	// find edge v0-v1
   Vector3D e2 = getVtxPosition(2) - getVtxPosition(0);	// find edge v0-v2
   Vector3D n = e1 % e2;				// n = e1 x e2
   float a = 0.5f * std::fabs(n.length());		// area = |n| / 2
   return a;
}

// Implementation of the Intersectable interface.

/**
 * Returns true if the ray intersects the triangle.
 * This is useful for quickly determining if it's a hit or miss,
 * but no information about the hit point is returned.
 */
bool Triangle::intersect(const Ray& ray) const
{
   Vector3D e1 = getVtxPosition(1) - getVtxPosition(0);	// e1 = edge v0 to v1
   Vector3D e2 = getVtxPosition(2) - getVtxPosition(0);	// e2 = edge v0 to v2

   Vector3D pvec = ray.dir % e2;			// pvec = cross(dir,e2)
   float det = e1 * pvec;				// determinant = dot(e1,pvec)
   if( det<epsilon && det>-epsilon )			// if det close to zero -> ray lies in the plane of tri
      return false;
	
   float inv_det = 1.0f / det;				// inverse determinant
   Vector3D tvec = ray.orig - getVtxPosition(0);	// distance v0 to ray origin
	
   float u = inv_det * (tvec * pvec);			// u = dot(tvec,pvec) / det
   if(u<-overlap || u>(1.0f+overlap))			// if u outside triangle return
      return false;
   
   Vector3D qvec = tvec % e1;				// qvec = cross(tvec,e1)
   float v = inv_det * (ray.dir * qvec);		// v = dot(dir,qvec) / det
   if(v<-overlap || (u+v)>(1.0f+overlap))		// if v outside triangle return
      return false;
   
   float t = inv_det * (e2 * qvec);			// t = dot(e2,qvec) / det
   return (t>=ray.minT && t<=ray.maxT);		// return true if t is within range of ray	
   
}

/**
 * Returns true if the ray intersects the triangle.
 * Information about the hit point is computed and returned in
 * the Intersection object.
 */
bool Triangle::intersect(const Ray& ray, Intersection& isect) const
{
   
   // TODO: Add triangle intersection test.
   // Remove the "return false"-statement and 
   // uncomment the lines that follows.
   
   // YOUR TEST HERE
   Vector3D e1 = getVtxPosition(1) - getVtxPosition(0);	// e1 = edge v0 to v1
   Vector3D e2 = getVtxPosition(2) - getVtxPosition(0);	// e2 = edge v0 to v2
	
   Vector3D pvec = ray.dir % e2;			// pvec = cross(dir,e2)
   float det = e1 * pvec;				// determinant = dot(e1,pvec)
   if( det<epsilon && det>-epsilon )			// if det close to zero -> ray lies in the plane of tri
      return false;
	
   float inv_det = 1.0f / det;				// inverse determinant
   Vector3D tvec = ray.orig - getVtxPosition(0);	// distance v0 to ray origin
	
   float u = inv_det * (tvec * pvec);			// u = dot(tvec,pvec) / det
   if( u<-overlap || u>(1.0f+overlap) )		// if u outside triangle return
      return false;
	
   Vector3D qvec = tvec % e1;					// qvec = cross(tvec,e1)
   float v = inv_det * (ray.dir * qvec);		// v = dot(dir,qvec) / det
   if( v<-overlap || (u+v)>(1.0f+overlap) )	// if v outside triangle return
      return false;
   
   float t = inv_det * (e2 * qvec);			// t = dot(e2,qvec) / det
   if( t<ray.minT || t>ray.maxT )				// return false if t is outside range
      return false;
   
   // If test passes...
   
   // Compute information about the hit point
   isect.mRay = ray;
   isect.mObject = this;						// Store ptr to the object hit by the ray (this).
   isect.mMaterial = getMaterial();
   if (!isect.mMaterial)
      isect.mMaterial = mMesh->getMaterial();		// Store ptr to the material at the hit point.
   isect.mPosition = ray.orig + t*ray.dir;		// Compute position of intersection
   isect.mNormal = (1.0f-u-v)*getVtxNormal(0) + u*getVtxNormal(1) + v*getVtxNormal(2);
   isect.mNormal.normalize();
   isect.mView = -ray.dir;				// View direction is negative ray direction.
   isect.mFrontFacing = isect.mView.dot(isect.mNormal) > 0.0f;
   if (!isect.mFrontFacing) isect.mNormal = -isect.mNormal;
   isect.mTexture = (1.0f-u-v)*getVtxTexture(0) + u*getVtxTexture(1) + v*getVtxTexture(2);
   isect.mHitTime = t;
   isect.mHitParam = UV(u,v);
   
   return true;     
}


/**
 * Returns the axis-aligned bounding box enclosing the triangle.
 */
void Triangle::getAABB(AABB& bb) const
{
   bb = AABB(getVtxPosition(0), getVtxPosition(1), getVtxPosition(2));
}

void Triangle::prepare()
{
   Vector3D n = getFaceNormal();
   
   for (int i = 0; i < 3; i++)
   {
      const Point3D& p0 = getVtxPosition(i);
      const Point3D& p1 = getVtxPosition((i+1)%3);
      const Point3D& p2 = getVtxPosition((i+2)%3);
      
      Vector3D e = p2 - p1;
		
      Vector3D np = n % e;
      
      float a = np.dot(p1);
      float b = np.dot(p0);
		
      float f = 1.0f / (b-a);
      float d = 1.0f - f*b;
      
      mPlanes[i] = f * np;
      mPlaneOffsets(i) = d;
   }
}

/// Returns the position of vertex i=[0,1,2].
const Point3D& Triangle::getVtxPosition(int i) const
{
   return mMesh->mVtxP[mVtx[i].p];
}

/// Returns the normal of vertex i=[0,1,2].
const Vector3D& Triangle::getVtxNormal(int i) const
{
   return mMesh->mVtxN[mVtx[i].n];
}

/// Returns the texture coordinate of vertex i=[0,1,2].
const UV& Triangle::getVtxTexture(int i) const
{
   return mMesh->mVtxUV[mVtx[i].t];
}

UV Triangle::calculateTextureDifferential(const Point3D& p, const Vector3D& dp) const
{	
   return mPlanes[0].dot(dp)*getVtxTexture(0) + mPlanes[1].dot(dp)*getVtxTexture(1) + mPlanes[2].dot(dp)*getVtxTexture(2);
}

Vector3D Triangle::calculateNormalDifferential(const Point3D& p, const Vector3D& dp, bool isFrontFacing) const
{
   Vector3D n = (mPlanes[0].dot(p)+mPlaneOffsets.x)*getVtxNormal(0) + (mPlanes[1].dot(p)+mPlaneOffsets.y)*getVtxNormal(1) + (mPlanes[2].dot(p)+mPlaneOffsets.z)*getVtxNormal(2);
   Vector3D dn = mPlanes[0].dot(dp)*getVtxNormal(0) + mPlanes[1].dot(dp)*getVtxNormal(1) + mPlanes[2].dot(dp)*getVtxNormal(2);
   
   float sign = isFrontFacing ? 1.0f : -1.0f;
   
   float nl = n.length();
   return sign * (n.dot(n) * dn - n.dot(dn) * n) / (nl*nl*nl);
}


bool Triangle::sampleDirection(const Intersection &intersection,Vector3D &sampleDirection)
{
   //create a random instance
   //get the random instance
   //to generate the canonical random number [0,1)
   RandomGenerator random = RandomGenerator::getRandomInstance();

   float temp =  sqrtf(1.0f - random.generateDouble(0,1));
   float beta =  (1.0f - temp);
   float gamma = random.generateDouble(0,1);


   //now calculate the point on the light
   Point3D lightPoint =  getVtxPosition(0) * (1 - beta - gamma)  +
      getVtxPosition(1) * beta + getVtxPosition(2) * gamma;
   

   Point3D intersectionToLightPoint = lightPoint - intersection.mPosition;


   Vector3D toLight(intersectionToLightPoint);

   toLight.normalize();

   sampleDirection = toLight;

   return true;   
}


float Triangle::getPDF(const Intersection &luminaireIntersection,
		       const Intersection &nonLuminaireIntersection)
{

  return 1.0/getArea();
}

Ray Triangle::sampleRay()
{
  Ray ray;

  return ray;
}
