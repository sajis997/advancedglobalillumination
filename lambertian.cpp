#include "lambertian.h"

Lambertian::Lambertian() 
  : BRDF(),
    kd(0.0),
    cd(Color(0.0f,0.0f,0.0f))
{

}

Lambertian::Lambertian(const Lambertian& lamb)
  : BRDF(lamb),
    kd(lamb.kd),
    cd(lamb.cd)
{

}

Lambertian& Lambertian::operator= (const Lambertian& rhs)
{
   if (this == &rhs)
      return (*this);
   
   BRDF::operator= (rhs);
   
   kd = rhs.kd; 
   cd = rhs.cd;
   
   return (*this);
}
		
Lambertian::~Lambertian(void)
{

}

/*
  there is no sampling here
 */
Color Lambertian::f(const Intersection& sr, const Vector3D& wo, const Vector3D& wi) const
{
  return (kd * cd * invPI);
}


Color Lambertian::sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi) const
{
  return Color(0.0f,0.0f,0.0f);
}
		
Color Lambertian::sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi, float& pdf) const
{
  return Color(0.0f,0.0f,0.0f);
}
		
Color Lambertian::rho(const Intersection& sr, const Vector3D& wo) const
{
  return (kd * cd);
}
			

void Lambertian::setReflectivity(const float kd)
{
  this->kd = kd;
}

float Lambertian::getReflectivity() const
{
  return kd;
}

void Lambertian::setCd(const Color& c)
{
  cd = c;
}

void Lambertian::setCd(const float r, const float g, const float b)
{
  cd.r = r;
  cd.g = g;
  cd.b = b;
}

