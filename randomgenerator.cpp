#include "defines.h"
#include "randomgenerator.h"

using namespace std;

//unsigned long RandomGenerator::seedValue = 0;
unsigned long RandomGenerator::seedValue = 7564231ULL;

unsigned long RandomGenerator::M = 123630823;
unsigned long RandomGenerator::A = 15650138;
unsigned long RandomGenerator::C = 54811;


void RandomGenerator::setSeed(int seed)
{
  srand(seed);
}

RandomGenerator::RandomGenerator()
{
  //srand(unsigned(time(NULL)));
  //seedValue = rand();
}


unsigned long RandomGenerator::generateInt()
{
  lcgRandom = ((A * seedValue) + C ) % M;
  seedValue = lcgRandom;
  return lcgRandom;
}

float RandomGenerator::generateFloat()
{
  return static_cast<float>( generateInt() * invRAND_MAX );
}


unsigned long RandomGenerator::generateInt(unsigned long low, unsigned long max)
{
  /*
    if(low < max)
    return (generateInt() % (max - low)) + low;
    else
    return (generateInt() % (low - max)) + max;
  */
  
  return ( static_cast<unsigned long> (generateDouble(0, max - low + 1) + low));
}

double RandomGenerator::generateDouble(double low, double max)
{
  double number_range = std::fabs(max- low);
  
  if(low < max)
    return ((static_cast<double>(generateInt()) * number_range) / M) + low;
  else
    return ((static_cast<double>(generateInt()) * number_range) / M) + max;
}
