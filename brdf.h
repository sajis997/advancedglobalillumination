#ifndef BRDF_H
#define BRDF_H

#include "defines.h"
#include "color.h"
#include "matrix.h"
#include "intersection.h"
#include "sampler.h"


class BRDF 
{
public:
   
   //void constructor
   BRDF(void);

   //copy constructor
   BRDF(const BRDF&);

   //equality operator
   BRDF& operator= (const BRDF& rhs);
  
   virtual ~BRDF(void);
				
   void setSampler(Sampler* sPtr);  
   
   /*
     return the BRDF itself unless it contains the delta function
   */
   virtual Color f(const Intersection& sr, const Vector3D& wo, const Vector3D& wi) const = 0;
   
   /*
     compute the DIRECTION of the reflected rays for simulating the reflected materials and
     diffuse-diffuse light transport
     NOTE the wi parameter is not constant here, since it will be used to return the direction
     irrdiance
     
     the directions are computed by sampling the BRDF and this is why the base class contains
     the pointer the sampler
   */
   virtual Color sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi) const = 0;
   
   virtual Color sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi, float& pdf) const = 0;
   
   
   // returns the bihemispherical reflectance
   
   virtual Color rho(const Intersection& sr, const Vector3D& wo) const = 0;
   
   
   //get the reflectivity co-eficient of the BRDF
   virtual float getReflectivity() const = 0; 
   
   
   virtual void setReflectivity(const float) = 0;
   
   
protected:
   
   // for indirect illumination
   Sampler *mSampler;
};

#endif


