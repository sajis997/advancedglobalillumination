/*
 *  pathtracer.h
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-03-10.
  *
 */
#ifndef __PATHTRACER_H__
#define __PATHTRACER_H__

#include <fstream>

using namespace std;

#include "raytracer.h"
#include "matrix.h"
/** Class implementing a simple path tracer.
*	The tracePixel() function is called for each pixel in the image,
*	and is responsible for computing the output color by tracing a
*	number of paths (rays) through the scene.
*/

class Ray;
class Intersection;


class PathTracer : public Raytracer
{
public:
   PathTracer(Scene* scene, Image* img);
   
   virtual void computeImage();
   
   ~PathTracer();
   
protected:
   
   
   
   Color tracePixel(int x, int y);

   //the recursive function with some initial recursion depth
   //the last parameter is a flag to decide whether to include the
   //emissive color
   Color trace(const Ray& ray,int depth = 3,int E = 1);

   
   Color computeRadiance(const Intersection &intersection,int depth);

   Color computeDirectIllumination(const Intersection &intersection);
   Color computeIndirectIllumination(const Intersection &intersection, int depth);
  
   Vector3D sampleCosine(const Intersection& is, float& pdf) const;

   //calculate the radiance transfer between the two visible points
   float radianceTransfer(const Intersection&,const Intersection&);


   unsigned int indirectPaths;

};
#endif // __PATHTRACER_H__
