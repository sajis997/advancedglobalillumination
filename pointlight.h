/*
 *  pointlight.h
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-02-28.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */

#ifndef POINTLIGHT_H
#define POINTLIGHT_H

#include "node.h"
#include "ray.h"

/**
 * Class representing a simple point light source.
 * The light source has a position, color and intensity.
 * The getRadiance() function returns the color multiplied
 * by the intensity, and the getWorldPosition() function
 * returns the position of the light source in world coordinates.
 */

class PointLight : public Node
{
public:

   PointLight();

   PointLight(const Point3D& p, const Color& c, float i=1.0f);

   virtual ~PointLight();

   //set the local position for  the point light
   void setPosition(const Point3D& p) { mPosition = p; }

   //set the color for the point light
   void setColor(const Color& c) { mColor = c; }

   //set the intensity for the local light
   void setIntensity(float i) { mIntensity = i; }

   //get the radiance for the point light
   Color getRadiance() const { return mRadiance; }

   //get the world position of the point light
   Point3D getWorldPosition() const { return mWorldPos; }


   //the following function is added by sajjad
   Color getFlux() const { return mFlux; }

   Ray getSampleRay() const;
   
protected:
   void prepare();
   
protected:
   //the radiance scaling factor
   /*
     by changing the following factor we can change the 
     brightness of the light which will be multiplied 
     by the color
   */
   float mIntensity;		///< Intensity (multiplication factor).
   
   
   Color mColor;			///< Color of light.
   
   Point3D mPosition;		///< Position of light (local space).
   Color mRadiance;		///< Cached intensity*color.
   Point3D mWorldPos;		///< Cached world position.

   //THE FOLLOWING ADDITION IS MADE BY SAJJAD
   Color mFlux;
};

#endif
