/*
 *  sphere.cpp
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-02-14.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */

#include "sphere.h"
#include "sampler.h"

#include "randomgenerator.h"


/**
 * Creates a sphere primitive.
 */
Sphere::Sphere() : Primitive(),
		   mRadius(0.5f),
		   mSampler(0)
{
  
}

/**
 * Creates a sphere at origin with radius r and material m.
 */
Sphere::Sphere(float r, Material* m ) : Primitive(m),
					mRadius(r),
					mSampler(0)
{

}

/**
 * Destroys the sphere.
 */
Sphere::~Sphere()
{
}

/**
 * Sets the radius of the sphere.
 */
void Sphere::setRadius(float r)
{
   mRadius = r;
}


void Sphere::setSampler(Sampler *sampler)
{
   //if the sampler is already there
   //we release it first before assigning
   //the new one
   if(mSampler)
   {
      delete mSampler;
      mSampler = NULL;
   }
   
   mSampler = sampler;
}

/**
 * Prepare sphere for rendering. This function computes the world->object
 * transform (inverse of the mWorldTransform matrix), so that intersection
 * tests can be performed in object space.
 */
void Sphere::prepare()
{
   // Compute inverse world transform.
   mInvWorldTransform = mWorldTransform.inverse();
}

/**
 * Append all intersectable geometry in this object to the array.
 * In this case, the Sphere itself is intersectable, so just add the this pointer.
 */
void Sphere::getGeometry(std::vector<Intersectable*>& geometry)
{
   geometry.push_back(this);
}

/**
 * Compute the two solutions to the quadratic A*t^2 + B*t + C = 0 and return
 * true if there are real solutions, and false if no real solutions.
 */
bool Sphere::solveQuadratic(float A, float B, float C, float& t0, float& t1) const
{
   // The solutions are given by:
   //
   //   t0 = (-B - sqrt(B^2-4AC) / 2A
   //   t1 = (-B + sqrt(B^2-4AC) / 2A
   //
   // We first compute the discriminant B^2-4AC, if this is negative,
   // there are no real solutions and we return false.
   // This method of solving the quadratic can be numerically
   // instable if B is close to the B^2-4AC, to avoid problems
   // a more stable form can be used (see PRT book).	
   
   float d = B*B - 4.0f*A*C;
   
   if (d < 0.0f)
      return false;
   
   d = std::sqrt(d);
   
   t0 = (-B - d) / (2.0f*A);
   t1 = (-B + d) / (2.0f*A);
   return true;
}

// Implementation of the Intersectable interface.

/**
 * Returns true if the ray intersects the sphere.
 */
bool Sphere::intersect(const Ray& ray) const
{
  // First, translate the ray to object space.
  // because the ray is in the world space
  Point3D o = mInvWorldTransform * ray.orig;
  Vector3D d = mInvWorldTransform * ray.dir;
  
  // Compute polynom coefficients.
  float A = d.x*d.x + d.y*d.y + d.z*d.z;
  float B = 2.0f * ( d.x*o.x + d.y*o.y + d.z*o.z );
  float C = o.x*o.x + o.y*o.y + o.z*o.z - mRadius*mRadius;
  
  // Solve quadratic equation for ray enter/exit point t0,t1 respectively.
  float t0, t1;
  
  if(!solveQuadratic(A,B,C,t0,t1)) return false;	// no real solutions -> ray missed
  
  if(t0>ray.maxT || t1<ray.minT) return false;	// sphere before/after ray
  if(t0<ray.minT && t1>ray.maxT) return false;	// ray inside sphere

  return true;
}

/**
 * Returns true if the ray intersects the sphere. 
 * Information about the hit is returned in the Intersection object.
 */
bool Sphere::intersect(const Ray& ray, Intersection& isect) const
{
  // First, translate the ray to object space.
  Point3D o = mInvWorldTransform * ray.orig;
  Vector3D d = mInvWorldTransform * ray.dir;
  
  // Compute polynom coefficients.
  float A = d.x*d.x + d.y*d.y + d.z*d.z;
  float B = 2.0f * ( d.x*o.x + d.y*o.y + d.z*o.z );
  float C = o.x*o.x + o.y*o.y + o.z*o.z - mRadius*mRadius;
  
  // Solve quadratic equation for ray enter/exit point t0,t1 respectively.
  float t0, t1;

  if(!solveQuadratic(A,B,C,t0,t1)) return false;	// no real solutions -> ray missed
  
  if(t0>ray.maxT || t1<ray.minT) return false;	// sphere before/after ray	
  if(t0<ray.minT && t1>ray.maxT) return false;	// ray inside sphere
  
  // Compute hit position & normal at hit point.
  float t = t0<ray.minT ? t1 : t0;		// ray hit time
 
  Point3D p = o + t*d;				// hit point in object space
  Vector3D n = p;				// since sphere is centered about origin in object space,
  n /= mRadius;
  
  // Compute spherical coordinates (theta,phi) in the range [0,1] for use as texture coords.
  float u = std::atan2(-p.z,p.x) / (2.0f*M_PI);	// phi (angle around perimeter, CCW seen from top)
  if(u<0.0f) u+=1.0f;
  float v = std::acos(p.y/mRadius) / M_PI;		// theta (angle from top of sphere)
  
  if (v != v) // Check for NaN
    v = p.y > 0.0f ? 0.0f : 1.0f;
  
  // Compute info about the intersection point.
  isect.mRay = ray;
  isect.mObject = this;					// The object by the ray (this object itself).
  isect.mMaterial = getMaterial();		// Store ptr to the material.
  isect.mHitTime = t;						// Store hit time parameter t.

  //transform the intersection point and normal at the intersection point
  //in the world coordinate system
  isect.mPosition = mWorldTransform * p;	// Store world space hit point.
  isect.mNormal = mWorldTransform * n;	// Store world space normal.

  isect.mNormal.normalize();	// just in case
  isect.mView = -ray.dir;					// View direction is negative ray direction.
  isect.mFrontFacing = isect.mView.dot(isect.mNormal) > 0.0f;
  if (!isect.mFrontFacing) isect.mNormal = -isect.mNormal;
  isect.mTexture = UV(u,v);				// Use spherical coordinates as texture coords.
  isect.mHitParam = UV(u,v);				// Store spherical coordinates.
  
  return true;
}

/**
 * Computes an axis-aligned bounding box enclosing the sphere in world space.
 */
void Sphere::getAABB(AABB& bb) const
{
   // To account for non-uniform scaling and arbitrary rotation/translation,
   // we compute the location of the eight corners of a box enclosing the sphere
   // in object space, and then setup the box that includes all corners translated
   // to world space. This works but is not always optimal.
   bb = AABB();
   
   for(int i = 0; i < 8; i++)
   {
      Point3D p( (float)(2*(i&1)-1) * mRadius,
		 (float)(2*((i&2)>>1)-1) * mRadius,
		 (float)(2*((i&4)>>2)-1) * mRadius );
      bb.include(mWorldTransform * p);
   }	
}

UV Sphere::calculateTextureDifferential(const Point3D& p, const Vector3D& dp) const
{
   Point3D lp = mInvWorldTransform * p;
   Vector3D ldp = mInvWorldTransform * dp;
   
   float du = (ldp.z*lp.x - lp.z*ldp.x) / (lp.x*lp.x + lp.z*lp.z) * (-0.5f/M_PI);
   float dv = ldp.y/(mRadius*sqrtf(1.0f - lp.y*lp.y/(mRadius*mRadius))) * (-1.0f/M_PI);
   
   return UV(du, dv);
}

Vector3D Sphere::calculateNormalDifferential(const Point3D& p, const Vector3D& dp, bool isFrontFacing) const
{
   float sign = isFrontFacing ? 1.0f : -1.0f;
   return sign * dp / mRadius;
}

//get the sample point from the sphere
//based intersection point
bool Sphere::sampleDirection(const Intersection &intersection,Vector3D &sampleDirection)
{
  //get the random instance
  //to generate the canonical random number [0,1)
  RandomGenerator random = RandomGenerator::getRandomInstance();   

  
  //declare a sphere center
  Point3D center = Point3D(0.0f,0.0f,0.0f);
  
  //transform the sphere center in the world space
  Point3D centerWorld = mWorldTransform * center;
  
  
  //get the intersection position in the world space
  Point3D intersectionPoint = intersection.mPosition ;
  
  //create a point from the intersection point to the center of the sphere   
  Point3D intersectionToCenter =  centerWorld - intersectionPoint;
   
  //create the coordinate system for sampling - w, u, and v
  //initialize a vector from the difference of points
  Vector3D w = Vector3D(intersectionToCenter);
  
  //get the length between the intersection point and
  //sphere center
  float d = w.length();
  
   
  //if the length is less than 
  //the radius then return false   
  if((d*d - mRadius*mRadius) < 1e-4f)
    {       
      float n1 = static_cast<float>(random.generateDouble(0,1));       
      float n2 = static_cast<float>(random.generateDouble(0,1));

      float z = 1.f - 2.f * n1;
      float r = sqrtf(std::max(0.f, 1.f - z*z));
      float phi = 2.f * M_PI * n2;
      float x = r * cosf(phi);
      float y = r * sinf(phi);
      
      //return the uniform direction instead       
      sampleDirection = Vector3D(x,y,z);
      
      return true;
    }
  
  float sin_alpha_max = (mRadius * mRadius) / (d*d);
  float cos_alpha_max = sqrt(std::max(1 - sin_alpha_max,0.0f));
  
  //generate two canonical numbers    
  float eps1 = static_cast<float>(random.generateDouble(0,1));
  float eps2 = static_cast<float>(random.generateDouble(0,1));

     
  float cos_alpha = 1 + eps1 * cos_alpha_max - eps1;
  float sin_alpha = sqrt(std::max(1 - cos_alpha * cos_alpha,0.0f));
  float phi = 2 * M_PI * eps2;
  
  float cos_phi = cos(phi);
  float sin_phi = sin(phi);
  
  //the cos_phi and sin_phi azimuthal and polar angles in the local 
  //local coordinate system
  //the sampling direction is in the local coordinate system    
  w.normalize();   
  
  
  /*
  // FIRST OPTION  
  Vector3D v = w.cross(intersection.mNormal);
  v.normalize();
  
  Vector3D u = v.cross(w);
  u.normalize();
  */
  
  //SECOND OPTION - is better than the FIRST ONE
  //i managed to remove those artifacts from the surface
  //of the luminaire
  Vector3D n(1.0f,0.0f,0.0f);
  Vector3D m(0.0f,1.0f,0.0f);
  
  Vector3D u = w.cross(n);
  
  if(u.length() < ONB_EPSILON)
    u = w.cross(m);
  
  u.normalize();
  
  Vector3D v = w.cross(u);
  v.normalize();
  
  //get the vector to the light source 
  Vector3D l = (u * cos_phi * sin_alpha) + 
    (v * sin_phi * sin_alpha) + 
    (w * cos_alpha);
  
  //store the direction to the light
  sampleDirection = l;
  
  //sample found , so return true
  return true;
}

//compute and return the area of  the sphere
float Sphere::getArea() const
{
   return M_PI * mRadius * mRadius;
}


float Sphere::getPDF(const Intersection &luminaireIntersection,
		     const Intersection &nonLuminaireIntersection)
{
  //declare a sphere center at the origin
  Point3D center = Point3D(0.0f,0.0f,0.0f);
  
  //transform the sphere center in the world space
  Point3D centerWorld = mWorldTransform * center;
    
  //get the intersection position in the world space
  Point3D intersectionPoint = nonLuminaireIntersection.mPosition ;
  
  //create a point from the intersection point to the center of the sphere   
  Point3D intersectionToCenter =  centerWorld - intersectionPoint;
   
  //create the coordinate system for sampling - w, u, and v
  //initialize a vector from the difference of points
  Vector3D w = Vector3D(intersectionToCenter);
  
  //get the length between the intersection point and
  //sphere center
  float d = w.length();
  
  float sin_alpha_max = mRadius / d;
  float cos_alpha_max = sqrt(std::max(1 - sin_alpha_max * sin_alpha_max,0.0f));
  
  
  //uniform density is calculated and stored
  float uniformDensity = 1.0f / (2 * M_PI * (1 - cos_alpha_max));


  //get the point between the nearest intersection point and light's sample point
  Point3D viewpointToLight(luminaireIntersection.mPosition - nonLuminaireIntersection.mPosition);
  
  //instantiate a vector to the light source sample point
  Vector3D vectorToLight(viewpointToLight);
  
  //get the squared length of the vector 
  float lengthSquared = vectorToLight.length2();
  
  //normalize the vector
  vectorToLight.normalize();
  

  float cos_theta_prime = std::max(-vectorToLight.dot(luminaireIntersection.mNormal),0.0f);
  
  //calculate the probability density function
  float pdf = uniformDensity  * cos_theta_prime / lengthSquared;
  
   //probability must be NON-ZERO 
  return pdf;

}


//get the sampled ray from the intersectable
//choose a random position over the intersectable
//and choose a random direction from the random
//position
Ray Sphere::sampleRay()
{
  //first get a random position over the surface over the sphere
  RandomGenerator random = RandomGenerator::getRandomInstance();
  
  
  //generate two canonical random numbers
  float eps1 = static_cast<float>(random.generateDouble(0,1));
  float eps2 = static_cast<float>(random.generateDouble(0,1));
  
  //calculate the spherical coordinates
  float theta = 2 * M_PI * eps1;
  float phi = acos(2 * eps2 - 1);
  
  //get the sample point in the cartesian coordinates
  float x = mRadius * cos(theta) * sin(phi);
  float y = mRadius * sin(theta) * sin(phi);
  float z = mRadius * cos(phi);
  
  
  //trandform the generated random sample point 
  //into the world coordinated system
  
  Point3D samplePointLocal = Point3D(x,y,z);
  
  
  //we already got the point , now get the random direction 
  //the sphere will act as a diffuse luminaire
  
  Vector3D n = samplePointLocal;
  n /= mRadius;
  
  //transform the normal to the world coordinate system
  Vector3D normal = mWorldTransform * n;
  normal.normalize();
  
  //create the orthonormal basic to transfer
  //the random direction into the world space
  //the random direction we create is in the local coordinate
  //and we transform the local coordinate to world coordinates
  Vector3D w = normal;
  Vector3D u = (( fabs(w.x) > .1 ? Vector3D(0,1,0) : Vector3D(1,0,0)) % w).normalize();
  Vector3D v = w % u;  
  
  //the following phi and theta is for the spherical coordinate system
  //the cosine weighted sampling so that the random light ray direction
  //stays around the normal
  //the random direction creation is mentioned in the photon mapping book
  //float random_phi = 2 * M_PI * static_cast<float>(random.generateDouble(0,1));
   
  //generate  a random number between 0 and 1
  //float random_theta = std::acos(std::sqrt(static_cast<float>(random.generateDouble(0,1))));


  float random_phi = 2 * M_PI * static_cast<float>(random.generateDouble(0,1));
  
  //generate  a random number between 0 and 1
  float random_theta = static_cast<float>(random.generateDouble(0,1));
  
  
  x = cos(random_theta) * sin(random_phi);
  y = sin(random_theta) * sin(random_phi);
  z = cos(random_phi);

  //the random direction is tansformed to the world coordinates
  //and normalized
  Vector3D d =( u * x + v * y + w * z ).normalize();   
  
  
  //So at last we got both the sample point and the direction 
  //so initiate a ray from both of them and return the ray
  
  //transform the point to the world coordinate
  Point3D samplePointWorld = mWorldTransform * samplePointLocal;
  
  Ray ray(samplePointWorld , d , 1e-4f);
  
  return ray;
}


 /*
float Sphere::getPDF(const Intersection &luminaireIntersection,
		     const Intersection &nonLuminaireIntersection)
{

  float pdf = 0.0f;

  //////////////////////////////////////////////////////////////////////
  //declare a sphere center at the origin
  Point3D center = Point3D(0.0f,0.0f,0.0f);
  
  //transform the sphere center in the world space
  Point3D centerWorld = mWorldTransform * center;
  
   
  //get the intersection position in the world space
  Point3D intersectionPoint = nonLuminaireIntersection.mPosition ;
  
  //create a point from the intersection point to the center of the sphere   
  Point3D intersectionToCenter =  centerWorld - intersectionPoint;
  
  //create the coordinate system for sampling - w, u, and v
  //initialize a vector from the difference of points
  Vector3D w = Vector3D(intersectionToCenter);
  
  //get the length between the intersection point and
  //sphere center
  float d = w.length();
  
  
  if( (d * d - mRadius * mRadius) < 1e-4f)
    {
      //calculate the visible point
      Point3D visiblePoint = luminaireIntersection.mPosition - nonLuminaireIntersection.mPosition;
      
      //initialize a vector from the visible point
      Vector3D vectorToLight(visiblePoint);
      
      //normalize the vector to the light
      vectorToLight.normalize();
      
      Ray ray(nonLuminaireIntersection.mPosition,vectorToLight,1e-3f);

      Intersection intersect;
      
      if(!this->intersect(ray,intersect))
	{
	  return 0.0f;
	}
      
      // Convert light sample weight to solid angle measure
      Point3D hitPoint = intersect.mPosition - nonLuminaireIntersection.mPosition;
      
      Vector3D vec(hitPoint);
      
      float d = vec.length2();
      
      float pdf =  d/(fabsf(intersect.mNormal.dot(vectorToLight)) * getArea());

      if(isinf(pdf)) pdf = 0.0;

      return pdf;
    }

   
   float sin_alpha_max = (mRadius * mRadius) / (d*d);
   float cos_alpha_max = sqrt(std::max(1 - sin_alpha_max,0.0f));


   //uniform density is calculated and stored
   pdf = 1.0f / (2 * M_PI * (1 - cos_alpha_max));


   //probability must be NON-ZERO 
   return pdf;

}
*/
