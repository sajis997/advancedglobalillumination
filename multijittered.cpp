#include "multijittered.h"
#include "randomgenerator.h"

MultiJittered::MultiJittered(void)
  : Sampler()
{

}

MultiJittered::MultiJittered(const int num_samples)
  : Sampler(num_samples)
{
  generateSamples();
}

MultiJittered::MultiJittered(const int num_samples, const int m)
  : Sampler(num_samples,m)
{
  generateSamples();
}

MultiJittered::MultiJittered(const MultiJittered& mjs)
  : Sampler(mjs)
{
  generateSamples();
}

MultiJittered& MultiJittered::operator= (const MultiJittered& rhs)
{
	if (this == &rhs)
		return (*this);
  
	Sampler::operator=(rhs);
  
	return (*this);
}

MultiJittered::~MultiJittered(void)
{

}

void MultiJittered::generateSamples()
{
   //get the random instance
   RandomGenerator random = RandomGenerator::getRandomInstance();
   
   // num_samples needs to be a perfect square  
   unsigned int n = static_cast<unsigned int>(sqrt(static_cast<float>(mNumSamples)));
   
   //get the subcell width 
   float subcell_width = 1.0 / (static_cast<float> (mNumSamples));
   
   
   // fill the samples array with dummy points to allow us to 
   // use the [ ] notation when we set the 
   // initial patterns
   
   Point3D fill_point;
   for (unsigned int j = 0; j < mNumSamples * mNumSets; j++)
      mSamples.push_back(fill_point);
   
   
   // distribute points in the initial patterns	
   for (unsigned int p = 0; p < mNumSets; p++) 
      for (unsigned int i = 0; i < n; i++)		
	 for (unsigned int j = 0; j < n; j++) 
	 {
	    mSamples[i * n + j + p * mNumSamples].x = (i * n + j) * subcell_width + static_cast<float>(random.generateDouble(0, subcell_width));
	    mSamples[i * n + j + p * mNumSamples].y = (j * n + i) * subcell_width + static_cast<float>(random.generateDouble(0, subcell_width));
	 }
   
   
   // shuffle x coordinates  
   for (unsigned int p = 0; p < mNumSets; p++) 
      for (unsigned int i = 0; i < n; i++)		
	 for (unsigned int j = 0; j < n; j++) 
	 {
	    unsigned int k = static_cast<unsigned int>(random.generateInt(j, n-1));
	    
	    float t = mSamples[i * n + j + p * mNumSamples].x;
	    
	    mSamples[i * n + j + p * mNumSamples].x = mSamples[i * n + k + p * mNumSamples].x;
	    mSamples[i * n + k + p * mNumSamples].x = t;
	 }
   
   
   // shuffle y coordinates	
   for (unsigned int p = 0; p < mNumSets; p++)
      for (unsigned int i = 0; i < n; i++)		
	 for (unsigned int j = 0; j < n; j++) 
	 {
	    unsigned int k = static_cast<unsigned int>(random.generateInt(j, n-1));
	    
	    float t = mSamples[j * n + i + p * mNumSamples].y;
	    
	    mSamples[j * n + i + p * mNumSamples].y = mSamples[k * n + i + p * mNumSamples].y;
	    mSamples[k * n + i + p * mNumSamples].y = t;
	 }
}
