#include "hitpoint.h"


/// Write elements to an output stream.
std::ostream& operator<<(std::ostream& os, const HitPoint& h)
{
   os << h.mIntersection.mPosition;

   return os;
}


HitPoint::HitPoint()
   : mPixelX(0),
     mPixelY(0),
     mPixelWeight(0.0f),
     mRadius(0.0f),
     mPhotonCount(0.0f),
     mNewPhotonCount(0)     
{

}

HitPoint::~HitPoint()
{

}
