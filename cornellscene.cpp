/*
 *  cornellscene.cpp
 *  prTracer
 *
 *  Created by Rasmus Barringer on 2011-04-01.
 *  Copyright 2011 Lund University. All rights reserved.
 *
 */

#include "cornellscene.h"
#include "diffuse.h"
#include "phong.h" 
#include "sphere.h"
#include "mesh.h"
#include "emissive.h"
#include "arealight.h"

void buildCornellScene(Scene* scene)
{
   //create the material for the walls
   Diffuse* red = new Diffuse(Color(0.7f, 0.1f, 0.1f));
   Diffuse* blue = new Diffuse(Color(0.1f, 0.1f, 0.7f));
   Diffuse* white = new Diffuse(Color(0.7f, 0.7f, 0.7f));

   //white->setReflectivity(0.3);
   //all of the above declared material are perfectly diffuse

   
   /*
     all of the following affine transformation in the local coordinate system
     the scene - prepare() function will transform the elements in the world
     coordinate system
    */
   
   Mesh* ground = new Mesh("data/plane.obj", white);
   ground->setScale(150.0f);

   Mesh* side1 = new Mesh("data/plane.obj", red);
   side1->setScale(150.0f);
   side1->setRotation(180.0f, 0.0f, 90.0f);
   side1->setTranslation(-60, 60, 0.0f);
   
   Mesh* side2 = new Mesh("data/plane.obj", blue);
   side2->setScale(150.0f);
   side2->setRotation(0.0f, 0.0f, 90.0f);
   side2->setTranslation(60, 60, 0.0f);
   
   Mesh* side3 = new Mesh("data/plane.obj", white);
   side3->setScale(150.0f);
   side3->setRotation(90.0f, 0.0f, 0.0f);
   side3->setTranslation(0.0f, 60, -60);
   
   Mesh* roof = new Mesh("data/plane.obj", white);
   roof->setScale(150.0f);
   roof->setRotation(180.0f, 0.0f, 0.0f);
   roof->setTranslation(0.0f, 120, 0.0f);
   
   Diffuse* sphereWhite = new Diffuse(Color(1.0f, 1.0f, 1.0f));
   sphereWhite->setTransparency(0.9);
   sphereWhite->setIndexOfRefraction(1.5);



   Phong *spherePhong = new Phong(Color(0.7f, 0.7f, 0.7f),
				  Color(0.9f, 0.9f, 0.9f),
				  100.001,0.9);
   
   
   //Sphere* ball1 = new Sphere(16.5f, sphereWhite);
   //ball1->setTranslation(Vector3D(23.5f, 16.5f, 31.0f));
   
     
   
   Sphere* ball2 = new Sphere(16.5f, sphereWhite);
   ball2->setTranslation(Vector3D(-22.5f, 16.5f, 11.0f));

   //ball2->setTranslation(Vector3D(-12.5f, 16.5f, 40.0f));
   
   //enough with the point light, lets do something realistic light source
   //PointLight* light = new PointLight(Point3D(0.0f, 108.0f, 0.0f), Color(1.0f, 1.0f, 1.0f), 50.0f);
   
   //add the point light
   //scene->add(light);
   
   //Mesh *smoothCube = new Mesh("data/smoothcube.obj",white); 
   //smoothCube->setScale(1.8f);// scale by a factor 0.8
   //smoothCube->setRotation(0, -150, 0);	// rotate -70 degrees around Y
   //smoothCube->setTranslation(Vector3D(-23.5f, 16.5f, 0.0f));   

   Mesh* elephant = new Mesh("data/elephant.obj", spherePhong);	// load mesh
   elephant->setScale(3.8f);								// scale by a factor 0.8
   elephant->setRotation(0, -150, 0);						// rotate -70 degrees around Y
   elephant->setTranslation(Vector3D(23.5f, 0.0f, 11.0f));
   
   
   //create an diffuse material
   //with the emissive color and intensity
   Diffuse *lightEmission1 = new Diffuse(Color(1,1,1));
   lightEmission1->setEmissionFactor(30.0f);


   /*
     Create the area light primitive
     and add the emissive material to it
     THE LIGHT PRIMITIVE IS SPHERE
   */

   
   Sphere *sphericalLightPrimitive = new Sphere(10,lightEmission1);
   sphericalLightPrimitive->setTranslation(Vector3D(0.0f, 105.0f, 0.0f));
   

   /*
   Sphere *sphericalLightPrimitive1 = new Sphere(16.5,lightEmission2);
   sphericalLightPrimitive1->setTranslation(Vector3D(-22.5f, 16.5f, 0.0f));
   */
   
   /*
     create a rectangular light primitive and add the emissive material
     to it
    */
   
   /*
   Mesh *rectangularLightPrimitive = new Mesh("data/plane.obj",lightEmission2);
   rectangularLightPrimitive->setRotation(180.0f, 0.0f, 0.0f);
   rectangularLightPrimitive->setTranslation(0.0f, 120, 0.0f);
   */
   
   /*
     Now create the area light 
    */
   AreaLight *areaLight = new AreaLight();
   //now add  the primitive to the area light which will act as luminaire
   areaLight->addToAreaLight(sphericalLightPrimitive,lightEmission1);
   
   //add the area light to the scene
   scene->add(areaLight);


   //add the area light primitive into the scene
   scene->add(sphericalLightPrimitive);

   
   scene->add(ground);
   
   scene->add(side1);
   scene->add(side2);
   scene->add(side3);
   scene->add(roof);
      
   scene->add(elephant);
   //scene->add(smoothCube);
   
   
   //scene->add(ball1);
   scene->add(ball2);
   
}

void setupCornellCamera(Camera* camera)
{ 
   Vector3D up(0.0f, 1.0f, 0.0f);
   Point3D pos(0.0f, 60.0f, 180.0f);
   Point3D target(0.0f, 60.0f, 0.0f);
   camera->setLookAt(pos, target, up, 52.0f); 
}
