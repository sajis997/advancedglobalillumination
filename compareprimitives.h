#ifndef COMPARE_PRIMITIVES_H
#define COMPARE_PRIMITIVES_H

#include "aabb.h"
#include "intersectable.h"
#include "bvhprimitiveinfo.h"

class ComparePrimitives
{
public:
   
   //x,y, or z - numbered as 0,1,2
   int m_sorting_dimension;
   
   bool operator()(BVHPrimitiveInfo *a, BVHPrimitiveInfo *b)
   {
      return a->mCentroid(m_sorting_dimension) < b->mCentroid(m_sorting_dimension);
   }
};

#endif


