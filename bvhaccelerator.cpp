#include <iomanip>
#include <algorithm>
#include <cassert>
#include <typeinfo>
#include <map>

#include "bvhaccelerator.h"
#include "bvhnode.h"
#include "linearbvhnode.h"
#include "compareprimitives.h"
#include "comparetomid.h"
#include "comparetobucket.h"
#include "comparepoints.h"
#include "hitpoint.h"
#include "hitpointinfo.h"


#include "timer.h"

BVHAccelerator::BVHAccelerator(unsigned int primitivePerNode, const string &splitMethod)
   :m_numberNodes(0), //initialize the number of nodes to ZERO
    root(NULL) // root is NULL
{
  //store the primitives per node in the accelerator's 
  //primate element
  m_maxPrimitivePerNode = primitivePerNode;
  
  if(splitMethod == "middle")
    m_splitMethod = SPLIT_MIDDLE;
  else if(splitMethod == "equal")
    m_splitMethod = SPLIT_EQUAL_COUNTS;
  else if(splitMethod == "sah")
    m_splitMethod = SPLIT_SAH;
}

BVHAccelerator::~BVHAccelerator()
{
   //browse through the node list and clear the memory
   for(unsigned int i = 0; i < m_nodeList.size();++i)
   {
      if(m_nodeList[i])
      {
        delete m_nodeList[i];
        m_nodeList[i] = NULL;
      }
   }

   //clear the list now
   m_nodeList.clear();

   for(unsigned int i = 0; i < m_objects.size();++i)
   {
      if(m_objects[i])
      {
	 delete m_objects[i];
	 m_objects[i] = NULL;
      }
   }
   
   //delete the object list as well
   m_objects.clear();

   //clear the vector of pointers
   m_hitPoints.clear();
}

//initialize the build data array for primitives
void BVHAccelerator::build(const std::vector<Intersectable*> &objects)
{
   //declare a timer object
   //to track the time of BVH construction
   Timer bvhTimer;
   
   //create the temporary bounding box
   AABB tmpBox;

   //browse through the intersectables list
   for(unsigned int i = 0 ; i < objects.size();++i)
   {
      //get the bounding box of the primitive
      objects[i]->getAABB(tmpBox);


      //insert the primitive info into the vector
      //we create a new BVHPrimitiveInfo with a pointer to the intersectable
      //and the bounding box  of the intersectable

      //inside the bvhprimitiveinfo constructor we calculate the centroid of the primitive
      //so this for loop create the centroid of each of the primitive inside the scene

      //inside the constructor we calculate the centroid of the intersectables
      m_objects.push_back(new BVHPrimitiveInfo(*objects[i],tmpBox));
   }
      
   //reserve the memory for the node list
   //in that reservation we assume that each primitive
   //will be allocated with a node in the list 
   m_nodeList.resize(2*m_objects.size() - 1,NULL);

   //TEST
   m_numberNodes = build_recursive(0,m_objects.size(),0);

   //since the root in alreay initialize to be added in to the list
   //we increment the number of nodes
   m_numberNodes += 1;
   
   std::cout << "BVH construction done in " << bvhTimer.stop() << " seconds" << std::endl;
}


void BVHAccelerator::buildForHitPoints(const std::vector<HitPoint*> &hitPoints)
{
   //declare a timer object
   //to track the time of BVH construction
   Timer bvhTimer;
   
   //browse through the intersectables list
   /*
   for(unsigned int i = 0 ; i < hitPoints.size();++i)
   {
      //copy the hit points to the
      //local vector we declared in the class declaration
      m_hitPoints.push_back(hitPoints[i]);
      }*/

   m_hitPoints.reserve(hitPoints.size());
   std::copy(hitPoints.begin(),hitPoints.end(),back_inserter(m_hitPoints));
   
      
   //reserve the memory for the node list
   //in that reservation we assume that each primitive
   //will be allocated with a node in the list 
   m_nodeList.resize(2*m_hitPoints.size() - 1,NULL);

   //TEST
   m_numberNodes = build_recursive_hit_points(0,m_hitPoints.size(),0);

   //since the root in alreay initialize to be added in to the list
   //we increment the number of nodes
   m_numberNodes += 1;
   
   std::cout << "BVH construction done in " << bvhTimer.stop() << " seconds" << std::endl;
}


unsigned int BVHAccelerator::build_recursive_hit_points(unsigned int left_index,unsigned int right_index,int depth)
{
   //make sure that the left and right index are not the same
   //initial call will contain the whole points in the hit point vector
   assert(left_index != right_index);
   
   //initialize a new node 
   //which will be flagged as the interior node 
   //or the leaf node later down the code
   LinearBVHNode *node = new LinearBVHNode();


   //calculate the number of points
   //between right index and left index
   unsigned int  nPoints = right_index - left_index;


   //we have to split more between the left_index
   //right_index and the splitting has to be done
   //based on the primitive centroids - is a point
   
   //the following bounding box will contain the
   //world bounding box of all the hit points
   //we have accumulated in the eye tracing process
   AABB pointsBound;
   
   //browse through and get the bounds of the centroid
   //of all the intersectables
   //the VERY FIRST CALL will build the world bounding box
   for(unsigned int i = left_index; i < right_index;++i)
   {	 
      //expand or contract the bounding box 
      pointsBound.include(m_hitPoints[i]->mIntersection.mPosition);
   }
     
   //set the bounding box for the current node
   //may it be the leaf node or the interior node
   node->setAABB(pointsBound);
   
   //insert the current node in the input parameter
   //into the node list, this is where all the nodes
   //and their children get assigned into the list
   //we have allocated in the build() function
   m_nodeList[depth] = node;



   /*
     check if the number of primitives 
     between the right and left index
     less the value initialzed with 
   */
   if(nPoints <= m_maxPrimitivePerNode)
   {      
      //we initiate the current node as the leaf node
      //with the primitives from m_objects[left_index]
      //to m_objects[right_index]
      node->makeLeaf(left_index,nPoints);
   }
   else
   {
      //NOT A LEAF NODE
      //SO IT MEANS WE SHALL BE HAVING
      //BOTH THE LEFT AND RIGHT CHILD NODE
      //we have to create the interior node
      
      //get the largest axis along the axis
      //in which the points' bounding box
      //is widest
      int largest_axis = pointsBound.getLargestAxis();


      //partition the primitives into two sets and build children
      //initialize the partition index which will be updated
      //with one of the splitting method
      unsigned int split_index = (right_index + left_index)/2;


      //we also need to check if all the centroid points are at the same position
      //which means if centroid bounds have zero volume, then we stop the recursion
      //create the leaf node with the primitives, it is worth checking before hand
      //using any of the splitting method, since none of the splitting method is effective
      //here( UNUSUSAL CASE)      
      if(pointsBound.mMax(largest_axis) == pointsBound.mMin(largest_axis))
      {	 
	 //it will hold the bounding box
	 //of the leaf node
	 AABB leafBox;
	 
	 //browse through the intersectables
	 for(unsigned int i = left_index;i < right_index;++i)
	 {
	    //expand or contract the bounding box 
	    leafBox.include(m_hitPoints[i]->mIntersection.mPosition);
	    
	 }
	 
	 node->setAABB(leafBox);
	 
	 //we initiate the created node
	 //as the leaf node of the current node
	 node->makeLeaf(left_index,(right_index-left_index));
	 
	 //simply return from here
	 //there is no point of going any further
	 return depth;
      }


	    
      // midpoint of the points along the splitting axis
      float pmid = 0.5f * (pointsBound.mMin(largest_axis) + pointsBound.mMax(largest_axis));
      
      ComparePoints mid(pmid,largest_axis);
      
      //partition the primitives along the largest axis
      //in the ascending order
      std::vector<HitPoint*>::iterator bound = std::partition(m_hitPoints.begin() + left_index,
							      m_hitPoints.begin() + right_index,
							      mid);
      
      //pull out the split index from the iterator
      split_index = bound  - (m_hitPoints.begin());
      
      
      //for lots of primitives with large overlapping bounding
      //boxes, this may fail to partition, in that case do not
      //break, and fall through to SPLIT_EQUAL_COUNTS
      assert(split_index != left_index);
      assert(split_index != right_index);

      //declare a static stack to get
      //hold of the parent
      static stack<int> parentStack;
      
  
      //push the depth into the stack
      //store that depth so that we can get back here
      parentStack.push(depth);
  
      //now recurse over the left part of the tree and left node
      //will always reside next to the parent node and we do not
      //need to do any explicit indexing into it
      depth = build_recursive_hit_points(left_index,split_index,depth + 1);
  
      //we then continue with the right part of the tree
      
      //retrieve the depth level
      int p = parentStack.top();
  
      //remove the top stack item
      parentStack.pop();
  
      //initiate the node as interior node
      m_nodeList[p]->makeNode(depth+1,right_index - split_index,largest_axis);
      
      
      depth = build_recursive_hit_points(split_index,right_index,depth + 1);      
                  
   } // end of the else  - takes care of the interior node

   return depth;
}


bool BVHAccelerator::foundNearestHitPoints(const Intersection &photonIntersection,
					   const float &searchRadius,
					   vector<HitPoint*> &hitPoints)
{
   //declare a list and iterator to traverse the
   //point cloud arranged inside the BVH
   std::list<unsigned int> nodeToProcess;
   std::list<unsigned int>::iterator nodeIterator;

   //initialize the node index to ZERO
   //so it initially the tree traversal
   //with the root of the tree
   unsigned int nodeIndex = 0;

   //initially flag the boolean to false
   //which might be flagged to true if
   //nearest hit points are found
   bool foundHitPoints = false;
   
   //get the root node from the list
   LinearBVHNode *node = m_nodeList[nodeIndex];

   //get the bounding box of the root node
   AABB &box = node->getAABB();

   //check if the given point in the parameter list
   //is found inside the bounding box of the root node
   if(box.inside(photonIntersection.mPosition))
   {
      //the point is found inside the bounding box
      //so store the node for further processing

      nodeToProcess.push_back(nodeIndex);

      //since the nodeToProcess is populate  the following
      //while loop will keep running until and unless
      //the list is empty
      while(nodeToProcess.size() > 0)
      {
	 //point to the beginning of the list
	 nodeIterator = nodeToProcess.begin();


	 if(m_nodeList[*nodeIterator]->isLeaf())
	 {

	    AABB &leafBox = m_nodeList[*nodeIterator]->getAABB();

	    if(leafBox.inside(photonIntersection.mPosition))
	    {
	       //the node in question is the leaf node
	       //so we browse through all the points inside the leaf node
	       //and find the points that satisfy the criteria for the
	       //ideal hit points we found in the ray tracing pass
	       
	       //get the index of the very first primitive - here it is the point
	       unsigned int firstPointPrimitiveOffset = m_nodeList[*nodeIterator]->getIndex();
	       

	       for(unsigned int i = 0;
		   i < m_nodeList[*nodeIterator]->getNumberObjects();
		   ++i)
	       {		  
		  ///////////////////////////////////////////////////////
		  hitPoints.push_back(m_hitPoints[i + firstPointPrimitiveOffset]);		  
		  //////////////////////////////////////////////////////
	       } //end of for
	       
	       foundHitPoints = true;
	    }
	 }
	 else
	 {
	    //get the index of the right child of the current node
	    nodeIndex = m_nodeList[*nodeIterator]->getIndex();

	    //get the right child
	    LinearBVHNode *rightChild = m_nodeList[nodeIndex];

	    //get the bounding box of the right child
	    AABB &rightBox = rightChild->getAABB();

	    //check if the photon bounce point is found
	    //inside the bounding box of the right child
	    if(rightBox.inside(photonIntersection.mPosition))
	    {
	       //insert the node for further processing
	       nodeToProcess.push_back(nodeIndex);	       
	    }

	    //get the index of the left child node of the current node
	    //left child is always residing right next to the parent node
	    //in this case the current node
	    nodeIndex = (*nodeIterator) + 1;

	    //get the left child by the left node index
	    LinearBVHNode *leftChild = m_nodeList[nodeIndex];

	    //get the boundary box of the left child
	    AABB &leftBox = leftChild->getAABB();
	    
	    if(leftBox.inside(photonIntersection.mPosition))
	    {
	       //insert the node for further processing
	       nodeToProcess.push_back(nodeIndex);	       
	    }
	 }

	 //the current node processed by the iterator
	 //is removed
	 nodeToProcess.erase(nodeIterator);
      }
   }
   
   return foundHitPoints;
}


unsigned int BVHAccelerator::build_recursive(unsigned int left_index,unsigned int right_index,int depth)
{

   //make sure that the left and right index are not the same
   assert(left_index != right_index);
   
   //initialize a new node 
   //which will be flagged as the interio node 
   //or the leaf node later down the code
   LinearBVHNode *node = new LinearBVHNode();
   
   //calculate the bounding box 
   //between the right_index and
   //left_index
   AABB tmpBound;
   AABB primitivesBound;
   
   //browse through the intersectables between right_index
   //and left_index and calculate their corresponding bounding box
   for(unsigned int i = left_index;i < right_index;++i)
   {
      //get the primitive bounding box
      tmpBound = m_objects[i]->mBounds;
      
      //include the intersectables
      //bounding box into the leaf
      //nodes bounding box
      primitivesBound.include(tmpBound);
   }
   
   
   //calculate the number of primitives 
   //between right index and left index
   unsigned int  nPrimitives = right_index - left_index;
   
   
   //set the bounding box for the current node
   //may it be the leaf node or the interior node
   node->setAABB(primitivesBound);
   
   //insert the current node in the input parameter
   //into the node list, this is where all the nodes
   //and their children get assigned into the list
   //we have allocated in the build() function
   m_nodeList[depth] = node;
   
   /*
     check if the number of primitives 
     between the right and left index
     less the value initialzed with 
   */
   if(nPrimitives <= m_maxPrimitivePerNode)
   {      
      //we initiate the current node as the leaf node
      //with the primitives from m_objects[left_index]
      //to m_objects[right_index]
      node->makeLeaf(left_index,nPrimitives);
      
   }
   else
   {

      //we have to split more between the left_index
      //right_index and the splitting has to be done
      //based on the primitive centroids - is a point

      //the following bounding box will contain the
      //centroid of all the primitives between left_index
      // and right_index
      AABB centroidBounds;
      
      //browse through and get the bounds of the centroid
      //of all the intersectables
      for(unsigned int i = left_index; i < right_index;++i)
      {

	 //expand or contract the bounding box 
	 centroidBounds.include(m_objects[i]->mCentroid);
      }
      
      
      //NOT A LEAF NODE
      //SO IT MEANS WE SHALL BE HAVING
      //BOTH THE LEFT AND RIGHT CHILD NODE
      //we have to create the interior node
      
      //get the largest axis along the axis
      //in which the centroid's bounding box
      //is widest
      int largest_axis = centroidBounds.getLargestAxis();
      
      //std::cout << " Largest split axis: " << largest_axis << std::endl;
      
      //partition the primitives into two sets and build children
      //initialize the partition index which will be updated
      //with one of the splitting method
      unsigned int split_index = (right_index + left_index)/2;

      //we also need to check if all the centroid points are at the same position
      //which means if centroid bounds have zero volume, then we stop the recursion
      //create the leaf node with the primitives, it is worth checking before hand
      //using any of the splitting method, since none of the splitting method is effective
      //here( UNUSUSAL CASE)      
      if(centroidBounds.mMax(largest_axis) == centroidBounds.mMin(largest_axis))
	{	 
	  //it will hold the bounding
	  //box of all the intersectables
	  //within the range
	  AABB tmpBox;
	  
	  //it will hold the bounding box
	  //of the leaf node
	  AABB leafBox;
	  
	  //browse through the intersectables
	  for(unsigned int i = left_index;i < right_index;++i)
	    {
	      tmpBox = m_objects[i]->mBounds;
	      
	      //include the intersectables
	      //bounding box into the leaf
	      //nodes bounding box
	      leafBox.include(tmpBox);
	    }
	 
	  node->setAABB(leafBox);
	  
	  //we initiate the created node
	  //as the leaf node of the current node
	  node->makeLeaf(left_index,(right_index-left_index));
	  
	  //simply return from here
	  //there is no point of going any further
	  return depth;
	}
     
     
      switch(m_splitMethod)
	{
	case SPLIT_MIDDLE:
	  {
	    
	    // midpoint of the priitive centroids along the splitting axis
	    float pmid = 0.5f * (centroidBounds.mMin(largest_axis) + centroidBounds.mMax(largest_axis));
	    
	    CompareToMid mid(pmid,largest_axis);
	    
	    //partition the primitives along the largest axis
	    std::vector<BVHPrimitiveInfo*>::iterator bound = std::partition(m_objects.begin() + left_index,
									    m_objects.begin() + right_index,
									    mid);
	    
	    //pull out the split index from the iterator
	    split_index = bound  - (m_objects.begin());
	    
	    
	    //for lots of primitives with large overlapping bounding
	    //boxes, this may fail to partition, in that case do not
	    //break, and fall through to SPLIT_EQUAL_COUNTS
	    if(split_index != left_index &&
	       split_index != right_index)
	      break;
	  }
	case SPLIT_EQUAL_COUNTS:
	  {	    
	    ComparePrimitives comp;
	    comp.m_sorting_dimension = largest_axis;
	    
	    split_index = (left_index + right_index) / 2;
	    
	    std::nth_element(m_objects.begin() + left_index,
			     m_objects.begin() + split_index,
			     m_objects.begin() + right_index,
			     comp);
	    break;
	  }
	case SPLIT_SAH:	 
	  {
	    
	    
	    if(nPrimitives <= 4)
	      {
		//partition primitives into the equal sized subsets
		ComparePrimitives comp;
		comp.m_sorting_dimension = largest_axis;
		
		split_index = (left_index + right_index) / 2;
		
		std::nth_element(m_objects.begin() + left_index,
				 m_objects.begin() + split_index,
				 m_objects.begin() + right_index,
				 comp);	       
	      }
	    else
	      {
		//Allocate the bucket info for the SAH partition buckets
		//this will be allocated every time the recursive call is made
		const int nBuckets = 12;

		struct BucketInfo
		{
		  BucketInfo()
		  {
		    count = 0;
		  }
		  
		  int count;
		  AABB bounds;
		};
		
		//at every call we shall be having 12 buckets for
		//splitting between the right_index and left_index
		BucketInfo buckets[nBuckets];
		
		//initialize the BucketInfo  for SAH partition buckets
		for(unsigned int i = left_index; i < right_index; ++i)
		  {
		    int b = nBuckets * ((m_objects[i]->mCentroid(largest_axis) - centroidBounds.mMin(largest_axis)) / (centroidBounds.mMax(largest_axis) - centroidBounds.mMin(largest_axis)));
		    
		    //std::cout << "While initializing the bucket: b =" << b << std::endl;
		    
		    if(b == nBuckets) b = nBuckets  - 1;		   
		    
		    assert(b >= 0 && b < nBuckets);
		    
		    buckets[b].count++;
		    buckets[b].bounds.include(m_objects[i]->mBounds); 
		  }
		
		
		//compute costs for splitting after each bucket
		float cost[nBuckets - 1];
		
		//for each buckets we have to count the number of primitives
		//and bounds of all their respective bounding boxes
		for(int i = 0; i < nBuckets - 1; ++i)
		  {
		    AABB b0,b1;
		    
		    int count0 = 0, count1 = 0;
		    
		    for(int j = 0; j <= i;++j)
		      {
			b0.include(buckets[j].bounds);
			count0 += buckets[j].count;
		      }
		    
		    for(int j = i+1; j < nBuckets;++j)
		      {
			b1.include(buckets[j].bounds);
			count1 += buckets[j].count;
		      }
		    
		    //initialize the cost to store the estimated SAH cost
		    cost[i] = 0.125 + (count0 * b0.getArea() + count1 * b1.getArea()) / primitivesBound.getArea();
		  }
		
		//find bucket to split at that minimizes SAH metric
		float minCost = cost[0];
		unsigned int minCostSplit = 0;
		
		//given all the costs, a linear scan through the cost array
		//finds the partition with the minimum cost
		for(int i = 1; i < nBuckets-1;++i)
		  {
		    if(cost[i] < minCost)
		      {
			minCost = cost[i];
			minCostSplit = i;
		      }
		  }
		
		//either create leaf or split the primitives at the selected SAH bucket
		if(nPrimitives > m_maxPrimitivePerNode || minCost < nPrimitives)
	        {
		  CompareToBucket cmpBucket(minCostSplit,nBuckets,largest_axis,centroidBounds);
		  
		  //partition the primitives along the largest axis
		  std::vector<BVHPrimitiveInfo*>::iterator bound = std::partition(m_objects.begin() + left_index,m_objects.begin() + right_index,cmpBucket);
		  
		  //pull out the split index from the iterator
		  split_index = bound  - (m_objects.begin());
		}
		else
		  {
		    //create the leaf node
		    //it will hold the bounding
		    //box of all the intersectables
		    //within the range
		    AABB tmpBox;
		    
		    //it will hold the bounding box
		    //of the leaf node
		    AABB leafBox;
		    
		    //browse through the intersectables
		    for(unsigned int i = left_index;i < right_index;++i)
		      {
			tmpBox = m_objects[i]->mBounds;
			
			//include the intersectables
			//bounding box into the leaf
			//nodes bounding box
			leafBox.include(tmpBox);
		      }
		    
		    node->setAABB(leafBox);
		    
		    //we initiate the created node
		    //as the leaf node of the current node
		    node->makeLeaf(left_index,nPrimitives);
		    
		    //simply return from here
		    //there is no point of going any further
		    return depth;		  
		  }
	  }
	}	 
      break;
    default:
      break;
    }
  
  //std::cout << "After SAH: " << split_index << std::endl;
  
  //make sure that none of the sides are empty
  assert(std::abs( static_cast<int>(split_index)  - static_cast<int>(left_index)) > 0);
  assert(std::abs( static_cast<int>(right_index) - static_cast<int>(split_index)) > 0);
  
   //declare a static stack to get
   //hold of the parent
  static stack<int> parentStack;
  
  
  //push the depth into the stack
  //store that depth so that we can get back here
  parentStack.push(depth);
  
  //now recurse over the left part of the tree and left node
  //will always reside next to the parent node and we do not
  //need to do any explicit indexing into it
  depth = build_recursive(left_index,split_index,depth + 1);
  
  //we then continue with the right part of the tree
  
  //retrieve the depth level
  int p = parentStack.top();
  
  //remove the top stack item
  parentStack.pop();
  
  //initiate the node as interior node
  m_nodeList[p]->makeNode(depth+1,right_index - split_index,largest_axis);

  
  depth = build_recursive(split_index,right_index,depth + 1);
}

return depth;
}

bool BVHAccelerator::intersect(const Ray &ray)
{
   //store the ray's minimum and maximum extent
   float minT = ray.minT;
   float maxT = ray.maxT;
   
   //declare a multi-map and an iterator
   //to browse through it
   std::multimap<float,unsigned int> check;
   std::multimap<float,unsigned int>::iterator it;
   
   unsigned int nodeNum = 0;
   
   //get the root node- the very first node in the list 
   //is the root node
   //we confirm this issue while insertion
   LinearBVHNode *node = m_nodeList[nodeNum];
   
   //get the bounding box of the root node
   AABB &box = node->getAABB();
   
   
   if(box.intersect(ray,minT,maxT))   //if the ray intersects the bounding box
   {     
      /*
        the root node bounding box is intersected
        so we insert it inside the map as an indication 
        that it will be further processed

	1. the closest intersection time
	2. the node that is intersected
      */
      check.insert(std::pair<float,unsigned int>(minT,nodeNum));
      
      /*
	goes through the following loop
	as long as the multimap is not empty
       */
      while(check.size() > 0)
      {
	 /*
            the map is populated
          */

	 //point to the beginning of the map
	 it = check.begin();

	 //check if the node is the leaf node 
	 if(m_nodeList[(*it).second]->isLeaf())
	 {
	    //get the index to the first primitive inside the leaf node
	    unsigned int firstPrimitiveOffset = m_nodeList[(*it).second]->getIndex();
	    
	    //browse through all the objects inside the leaf node and check for
	    //intersection
	    for(unsigned int i = 0; i < m_nodeList[(*it).second]->getNumberObjects(); ++i )
	    {
	       if(m_objects[firstPrimitiveOffset + i ]->mIntersectable->intersect(ray))
		  return true;
	    }
	 }
	 else
	 {
	    //dealing with the interior node now
            
	    //get the index of the  right child of the current node
	    nodeNum = m_nodeList[(*it).second]->getIndex();

	    //now get the right child 
	    LinearBVHNode *rightChild = m_nodeList[nodeNum];
            
	    //get the bounding box of the right child
	    AABB &rightBox = rightChild->getAABB();
            
	    /*
	      right box is intersected, we insert into the map
	      so that it will be processed
	    */
	    if(rightBox.intersect(ray,minT,maxT))	  
	       check.insert(std::pair<float,unsigned int>(minT,nodeNum));
	    
            
	    //get the index of the left child node of the current node
	    //left child is always residing next to the parent node
	    nodeNum = (*it).second + 1;
	    
	    LinearBVHNode *leftChild = m_nodeList[nodeNum];
            
	    AABB &leftBox = leftChild->getAABB();
            
	    if(leftBox.intersect(ray,minT,maxT))
	       check.insert(std::pair<float,unsigned int>(minT,nodeNum));	    
	 }
          
	 /*
	   the current node is processed and it can be removed 
	   from the map 
	 */
	 check.erase(it);
      }      
   }
  
   return false;
  
}

bool BVHAccelerator::intersect(const Ray &ray,Intersection &intersection)
{
   //initialize the minimum time and maximum time
   //from the ray we get as the input parameter
   float minT = ray.minT;
   float maxT = ray.maxT;

   //initialize the intersection hit time to INFINITY
   intersection.mHitTime = INF;

   //declare a multi-map and an iterator for it
   std::multimap<float,unsigned int> check;
   std::multimap<float,unsigned int>::iterator it;

   //declare and define the node index
   unsigned int nodeNum = 0;

   //get the root node from the node list
   //this node happened to be the current node 
   LinearBVHNode *node = m_nodeList[nodeNum];
   
   //get the bounding box of the current node
   AABB &box = node->getAABB();
   
   if(box.intersect(ray,minT,maxT))
   {
      //the bounding box of the current node is intersected
      
      //so we create a pair structure with the intersection time and node that is intersected
      //and insert the pair inside the map
      check.insert(std::pair<float,unsigned int>(minT,nodeNum));
      
      while(check.size() > 0)
      {
	 //the map is not empty
	 
	 //point to the beginning of the map
	 it = check.begin();
         

	 /*
	 if(intersection.mHitTime != INF)
            if(it->first > intersection.mHitTime)
	       return true;
	 */
         
	 if(m_nodeList[(*it).second]->isLeaf())
	 {
	    unsigned int firstPrimitiveOffset = m_nodeList[(*it).second]->getIndex();
            
	    for(unsigned int i = 0; i < m_nodeList[(*it).second]->getNumberObjects(); ++i )
	    {
	       Intersection currentIs;
               
	       if(m_objects[firstPrimitiveOffset + i ]->mIntersectable->intersect(ray,currentIs))
	       {
		  if(currentIs.mHitTime < intersection.mHitTime)
		  {
		     intersection = currentIs;
		  }
	       }
	    }
	 }
	 else
	 {
	    //dealing with the interior node now
            
	    //get the right child of the current node
	    nodeNum = m_nodeList[(*it).second]->getIndex();
            
	    LinearBVHNode *rightChild = m_nodeList[nodeNum];
            
            
	    AABB &rightBox = rightChild->getAABB();
            
	    if(rightBox.intersect(ray,minT,maxT))
	       check.insert(std::pair<float,unsigned int>(minT,nodeNum));
	    
            
	    //get the left child node of the current node
	    nodeNum = (*it).second + 1;
            
	    LinearBVHNode *leftChild = m_nodeList[nodeNum];
            
	    AABB &leftBox = leftChild->getAABB();
            
	    if(leftBox.intersect(ray,minT,maxT))
	       check.insert(std::pair<float,unsigned int>(minT,nodeNum));	    
	 }
	 
	 check.erase(it);
      }      
   }
   
   return intersection.mHitTime != INF;
}


//some debug information
void BVHAccelerator::print()
{
   std::cout << "Printing the nodes....." << std::endl;
   std::cout << "World Bounds: " << std::endl;
   std::cout << "Min: " << root->getAABB().mMin;
   std::cout << "Max: " << root->getAABB().mMax << std::endl;

   print_rec(root,0);
}


void BVHAccelerator::print_rec(LinearBVHNode *node, int depth)
{
   std::cout << std::setw(depth * 2) << ' ';

   if(node->isLeaf())
      std::cout << "Leaf<Primitives: " << node->getNumberObjects()
        << ", First primitive:" << node->getIndex() << std::endl;
   else
   {
      std::cout << "Node<Primitives: "<< node->getNumberObjects() << ">"  << std::endl;

      LinearBVHNode *left = m_nodeList[depth + 1];

      LinearBVHNode *right = m_nodeList[node->getIndex()];

      /*
      BVHNode *left = node->getLeft();
      BVHNode *right = node->getRight();
      */
      
      print_rec(left,depth + 1);

      print_rec(right,depth + 1);
   }
}
