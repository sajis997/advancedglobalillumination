/*
 *  phong.h
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-03-06.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */
#ifndef __PHONG_H__
#define __PHONG_H__

#include "material.h"



/** Class representing a simple phong material.
*	The BRDF is computed at an intersection point by evaluating
*	Phong's shading model. The material has a diffuse and a specular
*	component, which can have different colors. The phong exponent
*	specifies the amount of shininess, ranging from perfectly diffuse
*	to highly specular.
*/
class Phong : public Material
{
public:
   Phong(const Color& d, const Color& s, float exp, float r=0.0f, float t=0.0f, float e=0.0f, float n=1.0f);
   virtual ~Phong();
   
   Color evalBRDF(const Intersection& is, const Vector3D& L);
   
   virtual bool emissive() const
   {
      return false;
   }
   
protected:

   Color mDiffColor;	///< The diffuse color. Here the reflectivity is expressed as the three color bands
   Color mSpecColor;	///< The specular color.Here the specularity is expressed as the three color bands

   float mExponent;		///< The Phong exponent (shininess).
};


#endif // __PHONG_H__
