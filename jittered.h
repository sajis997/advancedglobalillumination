#ifndef JITTERED_H
#define JITTERED_H

#include "sampler.h"


class Jittered : public Sampler
{
private:
   
   virtual void generateSamples();

public:
   Jittered();

   Jittered(const int numSamples);

   Jittered(const int numSamples, const int m);

   Jittered(const Jittered&);

   Jittered& operator=(const Jittered &rhs);

   virtual ~Jittered();
};


#endif
