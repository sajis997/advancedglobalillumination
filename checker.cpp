/*
 *  checker.cpp
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-03-07.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */
#include "defines.h"
#include "intersection.h"
#include "checker.h"

using namespace std;


	
/** Constructs a checkerboard material consisting of two sub-materials,
*   one for odd and one for even squares. The unit square is divided into
*   nu and nv squares along the u and v directions respectively.
*/
Checker::Checker(Material* m1, Material* m2, int nu, int nv)
: Material()
, mNumU(nu)
, mNumV(nv)
{
   if(!m1 || !m2)
      throw std::runtime_error("(Checker::Checker) null pointer");
   
   mMat[0] = m1;
   mMat[1] = m2;
}


/** Destructor. Does nothing. */
Checker::~Checker()
{

}


/** Returns the BRDF at the intersection is for the light direction L. */
Color Checker::evalBRDF(const Intersection& is, const Vector3D& L)
{
   return mMat[ getType(is) ]->evalBRDF(is,L);
}


/** Returns the type of the checker at the intersection point, 0 or 1, 
*	where 0 is even squares and 1 is odd.
*/
int Checker::getType(const Intersection& is) const
{
   int iu = (int)std::floor(is.mTexture.u * (float)mNumU);
   int iv = (int)std::floor(is.mTexture.v * (float)mNumV);
   return (iu+iv)%2==0 ? 0 : 1;	// to handle negative iu/iv
}


/** Returns the reflectivity of the material in the range [0,1], 
*	where 0 means not reflective at all, and 1 gives a perfect mirror.
*	We override the default implementation in the Material class, because
*	we need to loop up the reflectivity based on the texture coordinates.
*/
float Checker::getReflectivity(const Intersection& is) const
{
	return mMat[ getType(is) ]->getReflectivity(is);
}


/**	Returns the transparency of the material in the range [0,1],
*	where 0 is fully opaque, and 1 is fully transparent.
*	We override the default implementation in the Material class, because
*	we need to loop up the transparency based on the texture coordinates.
*/
float Checker::getTransparency(const Intersection& is) const
{
	return mMat[ getType(is) ]->getTransparency(is);
}


/** Sets the reflectivity of the material by setting the reflectivity 
*   of the two sub-materials.
*/
void Checker::setReflectivity(float r)
{
	mMat[0]->setReflectivity(r);
	mMat[1]->setReflectivity(r);
}


/** Sets the transparency of the material by setting the transparency 
*	of the two sub-materials.
*/
void Checker::setTransparency(float t)
{
	mMat[0]->setTransparency(t);
	mMat[1]->setTransparency(t);
}

