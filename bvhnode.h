#ifndef BVH_NODE_H
#define BVH_NODE_H

#include "aabb.h"

class BVHNode
{
private:
    AABB m_bbox;

    bool m_leaf;

    unsigned int m_numberObjects;

    //if leaf node , then index to the intersectables
    unsigned int m_index;

    BVHNode *m_left;

    BVHNode *m_right;

public:

    BVHNode();
    ~BVHNode();

    void setAABB(AABB&);

    void makeLeft(BVHNode*,unsigned int );

    BVHNode* getLeft() const;

    void makeRight(BVHNode*,unsigned int);

    BVHNode* getRight() const;

    bool isLeaf() const;


    void makeLeaf(unsigned int,unsigned int);


    AABB& getAABB();

    unsigned int getIndex() const;

    unsigned int getNumberObjects() const;
    void setNumberObjects(unsigned int);
};


#endif
