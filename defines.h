/*
 *  defines.h
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-03-10.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */

#ifndef DEFINES_H
#define DEFINES_H

#include <stack>
#include <stdexcept>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <limits>
#include <cmath>
#include <vector>
#include <map>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <iomanip>
#include <ctime>

//the following three header files 
//are still at the experiment level
//which will be the defacto standard
//during the next revision
#include <chrono>
#include <limits>
#include <random>

//#include "mtrand.h"


#include "matrix.h"
#include "color.h"
#include "diffuse.h"

#ifndef M_PI
/// Define pi.
static const float M_PI = 3.14159265358979323846f;
#endif


static const float invPI  = 0.3183098861837906715f;

/// Define positive infinity for floats.
static const float INF = std::numeric_limits<float>::infinity();	

/// Small value.
static const float epsilon = 1e-6f;

/// Default material is a gray diffuse material.
static Diffuse DEFAULT_MATERIAL = Diffuse(Color(0.7f,0.7f,0.7f));

///////////////////////// THE FOLLOWINGS ARE ADDED BY SAJJAD //////////


static const float ONB_EPSILON = 0.01; 

static const float 	invRAND_MAX = 1.0 / (float)RAND_MAX;


// Global Inline Functions

///////////////////////// SAJJAD'S INCLUSION ENDS HERE ///////////////////


/// Returns a uniform random number in the range [0,1).
static float uniform() { return (float)std::rand()/(1.f+RAND_MAX); }

/// Helper function for converting an int to a string.
static std::string int2str(int i)
{
	std::ostringstream oss;
	oss << i;
	return oss.str();
}

/// Swaps two floats.
static void swap(float& a, float& b) { float t=a; a=b; b=t; }

/// Round value to nearest integer.
#define ROUND(x) (std::floor((x)+0.5))

/// Returns the maximum of the two elements.
template<class T> T max(T a, T b) { return a>b ? a : b; }

/// Returns the minimum of the two elements.
template<class T> T min(T a, T b) { return a<b ? a : b; }


// PHOTON MAPPING RELATED CONSTANTS
static const int ray_pm_emitted_caustics = 25000;
static const int ray_pm_emitted_radiance = 500;

static const int ray_pm_estimation_caustics = 100;
static const int ray_pm_estimation_radiance = 500;


//PROGRESSIVE PHOTON MAPPING RELATED CONSTANTS
static const float ALPHA = 0.7f;
static const unsigned int PHOTONS_PER_PASS = 100000;
static const unsigned int PHOTON_PASSES = 2;
static const float PHOTON_RADIUS = 0.5f;

#endif
