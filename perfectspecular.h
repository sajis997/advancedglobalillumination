#ifndef PERFECT_SPECULAR_H
#define PERFECT_SPECULAR_H

#include "brdf.h"

class PerfectSpecular : public BRDF
{
public:

   PerfectSpecular();

   PerfectSpecular(const PerfectSpecular&);

   PerfectSpecular& operator= (const PerfectSpecular &rhs);

   virtual ~PerfectSpecular();

   virtual Color f(const Intersection& sr, const Vector3D& wo, const Vector3D& wi) const;
   
   virtual Color sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi) const;
   
   virtual Color sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi, float& pdf) const;
   
   virtual Color rho(const Intersection& sr, const Vector3D& wo) const;


   virtual float getReflectivity() const;

   virtual void setReflectivity(float r);

private:

   //reflection co-efficient
   float kr;

   //the reflection color
   Color cr;
   
};


#endif
