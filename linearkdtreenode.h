#ifndef LINEARKDTREENODE_H
#define LINEARKDTREENODE_H

#include "aabb.h"

class LinearKDTreeNode
{
 private:

  //if interior node, index to the right child node
  //if leaf node, index to the first item in the photon vector
  unsigned int mIndex;

  //hold the split axis for the node
  unsigned int mSplitAxis;

   //number of objects inside the node
  //the following parameter will help to decide
  //if the node will be further split or not
  //if the number of objects is less than or equal to 1
  //we label the node as the leaf node
   unsigned int mNumberObjects;
   
   //boolean flag as an indicator of a leaf node or not
   bool mLeaf;
   
   //bounding box of the node
   AABB mBbox;
   
   //the split position value
   float mSplitPos;
   
public:
   
   LinearKDTreeNode();
   
   ~LinearKDTreeNode();
   
   //get the split axis
   unsigned int getSplitAxis() const;
   
   //check if it is the leaf node or not
   bool isLeaf() const;
   
   //get the index to the right child node
   unsigned int getIndex() const;
   
   //get the bounding box of the node
   AABB &getAABB();
   
   //set the bounding box for the node
   void setAABB(AABB &box);

   void makeNode(float splitPos, unsigned int splitAxis,unsigned int  index);
   
   void makeLeaf(unsigned int index);
};

#endif
