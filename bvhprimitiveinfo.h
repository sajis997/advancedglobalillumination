#ifndef BVHPRIMITIVEINFO_H
#define BVHPRIMITIVEINFO_H

#include "aabb.h"
#include "matrix.h"
#include "intersectable.h"


//the following class stores the information of each intersectables
// the pointer to the intersectable itself
// the centroid of the intersectable
// the bounding box of the intersectable
class BVHPrimitiveInfo
{
 public:
 BVHPrimitiveInfo(const Intersectable &i,const AABB &b)
   : mIntersectable(const_cast<Intersectable*>(&i)),
    mBounds(b)
    {
       //calculate and store the centroid value
       //of the primitive intersectable
      mCentroid = (b.mMin + b.mMax) * 0.5f; 
    }
  
  //pointer to the intersectable
  Intersectable *mIntersectable;
  
  //centroid of the primitive's bounding box
  Point3D mCentroid;
  
  //the bounding box of the primitive
  AABB mBounds;   
};

#endif
