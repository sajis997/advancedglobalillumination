#ifndef GLOSSY_SPECULAR_H
#define GLOSSY_SPECULAR_H

#include "brdf.h"


class GlossySpecular : public BRDF
{
public:

   GlossySpecular();

   GlossySpecular(const GlossySpecular &rhs);

   GlossySpecular& operator= (const GlossySpecular & rhs);
   

   virtual ~GlossySpecular();


   void setSampler(Sampler *sample,float exponent);
   
   virtual Color f(const Intersection& sr, const Vector3D& wo, const Vector3D& wi) const;
   
   virtual Color sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi) const;
   
   virtual Color sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi, float& pdf) const;
   
   virtual Color rho(const Intersection& sr, const Vector3D& wo) const;
   
   //set the diffuse reflectivity
   //the value is between [0-1]
   virtual void setReflectivity(const float kd);
   
   virtual float getReflectivity() const;
      
private:
   //specular reflectivity
   float ks;

   Color cs;

   //specular exponent
   float exp;
};


#endif
