#include "arealight.h"
#include "emissive.h" 
#include "sphere.h"
#include "mesh.h"
#include "triangle.h"

#include "randomgenerator.h"

AreaLight::AreaLight(): mSelectedLuminaire(0)
{
  //clear area light luminaires
  //the following list contain several
  //light intersectables
  mLightIntersectables.clear();
}


AreaLight::~AreaLight()
{
  //empty the vector
  mLightIntersectables.clear();
  
  //set the selected luminaire to NULL
  mSelectedLuminaire = 0;
}

//add an intersectable to behave as an arealight 
void AreaLight::addToAreaLight(Intersectable *intersectable,Material *material)
{
  //declare a structure storing the luminaire's material
  //and the luminaire's intersectable itself
  LuminaireProbability *luminaireProb = new LuminaireProbability();
  
  luminaireProb->mIntersectable = intersectable;
  luminaireProb->mMaterial = material;
  
  //insert the luminaires property into the vector
  mLightIntersectables.push_back(luminaireProb);
}

//add a mesh to behave as a area light
//mesh in turn contains intersectable
void AreaLight::addToAreaLight(Mesh *mesh)
{
  //pull out the triangle list from the mesh
  //but the face list does not contain any material reading throug the 
  //loadOBJ()
  std::vector<Triangle> faceList = mesh->getFaceList();

  //get the material of the mesh
  Material *material = mesh->getMaterial();

  //make sure that the material is not NULL
  assert(material != 0);

  LuminaireProbability *luminaireProb = 0;

  //browse through the triangle list of the mesh
  //and store the reference in the material 
  for(unsigned int i = 0; i < faceList.size();++i)
    {
       luminaireProb = new LuminaireProbability();

       //the following two fields are populated here
       //and the rest of the elements are calculated
       //inside the prepare() function
       luminaireProb->mIntersectable = &faceList[i];
       luminaireProb->mMaterial = material;

       mLightIntersectables.push_back(luminaireProb);
   }   
}

/*
  prepare() will browse through all the emissive intersectables
  that will behave as light source ans calculate their probabilistic 
  weight
*/
void AreaLight::prepare()
{
   //browse through the vector and calculate the
   //weight for the selection probability
   
   //calculate the total intensity of all light source
   float totalIntensity = 0.0f;
   
   //browse through the luminaire intersectables and add up 
   //the total intensity of all the intersectables
   //here the intensity is the emission factor
   for(unsigned int i = 0; i < mLightIntersectables.size(); ++i)
   {
      totalIntensity += mLightIntersectables[i]->mMaterial->getEmissionFactor();
   }
   
   //make sure that the total intensity is not ZERO
   //to avoid division by ZERO
   assert(totalIntensity != 0);

   float cumulativeDistributionValue = 0.0f;
   
   for(unsigned int i = 0; i < mLightIntersectables.size();++i)
   {
      //for one luminaire the probability will always be 1
      mLightIntersectables[i]->mLuminaireProbabilityValue = mLightIntersectables[i]->mMaterial->getEmissionFactor()/totalIntensity;

      //for one luminaire the cumulative value will also be 1
      cumulativeDistributionValue += mLightIntersectables[i]->mLuminaireProbabilityValue;

      mLightIntersectables[i]->mCumulativeDistributionValue = cumulativeDistributionValue;
   }
}

//calcualate the world position based on the intersection point
bool AreaLight::calculateSampleDirection(const Intersection &intersection,Vector3D &sampleVector)
{

  //choose the light intersectables based on the weight of the probaiblity
  //that we have calculated in the prepare() function
  //create a random instance
  RandomGenerator random = RandomGenerator::getRandomInstance();
  
  float selectionProbability = 0.0f;
  
  //   CompareAreaLight cmpAreaLight;
  
  //sort the light intersectables based on the luminaire probability value
  //   std::sort(mLightIntersectables.begin(),mLightIntersectables.end(),cmpAreaLight);
  
  std::vector<LuminaireProbability*>::iterator it;
  
  //create a canonical random number between 0 and 1
  selectionProbability = static_cast<float>(random.generateDouble(0,1));
  
  //selectionProbability = distribution(generator);
  

  //A LITTLE WORK AROUND BECAUSE THE STL find_if is not working here
  //VITA-LINUX LAB
   for(unsigned int i = 0; i < mLightIntersectables.size();++i)
     {
       if(mLightIntersectables[i]->mCumulativeDistributionValue > selectionProbability)
	 {
	   mSelectedLuminaire = mLightIntersectables[i];
	   break;
	 }
     }

   //THE FOLLOWING SNIPPET WORKS FINE IN MY SYSTSEM
   /*
   CompareToCDF cdf;
   //store the canonical random value
   cdf.mRandomValue = selectionProbability;

   it = std::find_if(mLightIntersectables.begin(),mLightIntersectables.end(),cdf);

   assert(it != mLightIntersectables.end());

   //store the selected luminaire
   mSelectedLuminaire = *it;
   */

   //put the assertion here instead
   assert(mSelectedLuminaire != 0);

   //so the luminaire is selected mat it be triangle or sphere
   //and we call the virtual function to get the sampling direction from the
   //light source
   mSelectedLuminaire->mIntersectable->sampleDirection(intersection,sampleVector);

   /*
   if(mSelectedLuminaire != 0)
    {
       if(mSelectedLuminaire->mIntersectable != 0)
        mSelectedLuminaire->mIntersectable->sampleDirection(intersection,sampleVector);
    }
   */
   
   return true;
}

float AreaLight::getPDF(const Intersection &luminaireIntersection,
                        const Intersection &nonLuminaireIntersection) const
{
   //make sure we have found the luminaire intersection
   assert(mSelectedLuminaire != 0);

   return mSelectedLuminaire->mIntersectable->getPDF(luminaireIntersection,nonLuminaireIntersection);

}



//get the radiance for the point light
Color AreaLight::getRadiance() const
{
   //make sure that we have selected the luminaire intersectable
   assert(mSelectedLuminaire != 0);

   Intersection dummyIntersection;
   Vector3D dummyLightVector;

   return mSelectedLuminaire->mMaterial->evalBRDF(dummyIntersection,dummyLightVector) * mSelectedLuminaire->mMaterial->getEmissionFactor() * mSelectedLuminaire->mLuminaireProbabilityValue;
}


Color AreaLight::getFlux()
{
  //make sure that we have selected the luminaire intersectable
  assert(mSelectedLuminaire != 0);
  
  mFlux = mSelectedLuminaire->mIntersectable->getArea() * M_PI * getRadiance(); 
  
  return mFlux;
}


//generate photon ray from the light source
//mainly used in photon mapping
Ray AreaLight::getSampleRay()
{
  //choose the light intersectables based on the weight of the probaiblity
  //that we have calculated in the prepare() function
  //create a random instance
  RandomGenerator random = RandomGenerator::getRandomInstance();
  
  float selectionProbability = 0.0f;
  
  std::vector<LuminaireProbability*>::iterator it;
  
  //create a canonical random number between 0 and 1
  selectionProbability = static_cast<float>(random.generateDouble(0,1));
    
  //A LITTLE WORK AROUND BECAUSE THE STL find_if is not working here
  //VITA-LINUX LAB
  for(unsigned int i = 0; i < mLightIntersectables.size();++i)
    {
      if(mLightIntersectables[i]->mCumulativeDistributionValue > selectionProbability)
	{
	  mSelectedLuminaire = mLightIntersectables[i];
	  break;
	}
    }
  
  //THE FOLLOWING SNIPPET WORKS FINE IN MY SYSTSEM
  /*
    CompareToCDF cdf;
    //store the canonical random value
    cdf.mRandomValue = selectionProbability;
    
    it = std::find_if(mLightIntersectables.begin(),mLightIntersectables.end(),cdf);
    
    assert(it != mLightIntersectables.end());
    
    //store the selected luminaire
    mSelectedLuminaire = *it;
  */
  
  
  //put the assertion here instead
  assert(mSelectedLuminaire != 0);
  
  //so the luminaire is selected mat it be triangle or sphere
  //and we call the virtual function to get the sampling direction from the
  //light source
  return mSelectedLuminaire->mIntersectable->sampleRay();   
}
