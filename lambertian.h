#ifndef LAMBERTIAN_H
#define LAMBERTIAN_H

#include "brdf.h"


class Lambertian : public BRDF
{
 public:

  Lambertian();

  Lambertian(const Lambertian& lamb);
  
  Lambertian& operator= (const Lambertian& rhs);
		
  virtual ~Lambertian(void);  


  virtual Color f(const Intersection& sr, const Vector3D& wo, const Vector3D& wi) const;
		
  virtual Color sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi) const;

  virtual Color sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi, float& pdf) const;
		
  virtual Color rho(const Intersection& sr, const Vector3D& wo) const;

  //set the diffuse reflectivity
  //the value is between [0-1]
  void setReflectivity(const float kd);

  float getReflectivity() const;

  void setCd(const Color& c);
  
  void setCd(const float r, const float g, const float b);
		

private:
   
   //diffuse reflection coefficient
   float   kd;
   //diffuse color
   Color   cd;
};


#endif
