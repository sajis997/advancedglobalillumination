#include <omp.h>
#include <cassert>

#include "pathtracer.h"
#include "defines.h"
#include "scene.h"
#include "camera.h"
#include "ray.h"
#include "intersection.h"
#include "material.h"
#include "timer.h"
#include "image.h"

#include "randomgenerator.h"

#include "sampler.h"
#include "arealight.h"


PathTracer::PathTracer(Scene *scene, Image *img)
  :Raytracer(scene,img), indirectPaths(0)
{
  
  
}

PathTracer::~PathTracer()
{
  
  
}

void PathTracer::computeImage()
{
   std::cout << "Path tracing..." << std::endl;
   
   //map the samples to hemisphere with 
   //cosine weighted density function
   //mSampler->mapSamplesToHemisphere(1);
   
   Timer timer;
   
   Color c;

   //get the width and height of the image
   int width = mImage->getWidth();
   int height = mImage->getHeight();
   
   // Loop over all pixels in the image
   #pragma omp parallel for schedule(dynamic,1), private(c)
   for (int y = 0; y < height; y++)
     {
       for (int x = 0; x < width; x++)
         {
           // Raytrace the pixel at (x,y).
           c = tracePixel(x,y);
           
           // Store the result in the image.
           mImage->setPixel(x,y,c);
         }

       #pragma omp critical
       // Print progress approximately every 5%.
       if ((y+1) % (height/20) == 0 || (y+1) == height)
         std::cout << (100*(y+1)/height) << "% done" << std::endl;
     }
   
   std::cout << "Done in " << timer.stop() << " seconds" << std::endl;

}



   
Color PathTracer::tracePixel(int x, int y)
{
   Color pixelColor;
   Point3D samplePoint;
   
   float px,py;
   Ray ray;

   unsigned int primaryRayCounter = 0;

   //get the number of samples
   unsigned int numSamples = mSampler->getNumberSamples();
   
   for(unsigned int i = 0; i < numSamples; i++)
   {
      //get the sample point in the unit square
      samplePoint = mSampler->sampleUnitSquare();
      
      //update the pixel point to one of the sample points
      px = x + samplePoint.x;
      py = y + samplePoint.y;

      //create the primary ray through the sample point
      //each new sample create a new ray 
      ray = mCamera->getRay(px,py);

      //trace with the initial depth of 5
      //this is done intentinally to reduce errors
      //after depth 5 we use the russian roulette
      pixelColor += trace(ray,5,1);
     	
   }

   //average the radiance value with the number of samples taken
   pixelColor /= numSamples;
   
   return pixelColor;
}

//equivalent to the Li() in PBRT
//EQUIVALENT TO THE rad() in AGI
Color PathTracer::trace(const Ray& ray,int depth,int E) 
{

   Color outputColor;
   
   //declare the intersection
   //for the nearest intersection
   //inside the scene
   Intersection intersection;
   
   
   //check the intersection and store the
   //intersection information
   bool hit = mScene->intersect(ray,intersection);
   
   //dummy light vector for the emissive material
   Vector3D dummyLightVec;
   
   
   if(hit)
   {

      if(intersection.mMaterial->emissive())
	 return E * intersection.mMaterial->evalBRDF(intersection,dummyLightVec) * intersection.mMaterial->getEmissionFactor();
      else
	 return computeRadiance(intersection,depth);
      //the scene is hit
      //the flag 'E' is the inclusion of the direct lighting
      //if the intersected object is the emissive one

      //outputColor = E * ((intersection.mMaterial->emissive()) ? ( intersection.mMaterial->evalBRDF(intersection,dummyLightVec) * intersection.mMaterial->getEmissionFactor()) : Color(0.0f,0.0f,0.0f)) +
      //	 computeRadiance(intersection,depth);
   }
   else
      //return black or background color if there is no nearest
      //primitive intersection
      outputColor = Color(0.0,0.0,0.0);
      //outputColor = mScene->getBackground(ray.dir);
   
   return outputColor;
}


Color PathTracer::computeRadiance(const Intersection &intersection,int depth)
{
  //get the color from the direct lighting 
  Color directLighting = computeDirectIllumination(intersection);

  //get the indirect lighting computation
  Color indirectLighting = computeIndirectIllumination(intersection,depth);

  //combine the direct and indirect lighting
  return directLighting + indirectLighting;
}


Color PathTracer::computeDirectIllumination(const Intersection &intersection)
{
  //declare the color to store the radiance
  //from the direct illumination
  Color outputColor(0.0f,0.0f,0.0f);
  
  //pointer to the area light
  AreaLight *areaLight;
  
  //incoming the radiance is basically the emissive radiance from the direct light source
  Color incomingRadiance,BRDF;
  
  Intersection lightIntersection;
  Ray lightRay;
  
  //initialize the shadow hit to false
  bool lightHit = false;
  
  
  //get the area light -  in  the scene
  //the area light interface one or sevral
  //luminaire intersectables
  areaLight = mScene->getAreaLight();

  //if the arealight is not found
  //simply return BLACK
  if(!areaLight)
     return Color(0.0f,0.0f,0.0f);

/*  
  ///////////////// POINT LIGHT ////////////////
  //pointer to the point light

  PointLight *pointLight;


  //get the point light from the scene
  pointLight = mScene->getLight(0);
  
  Ray shadowRay;
  Vector3D lightVec;
  float incidentAngle;
  bool shadowHit = false;

  shadowRay = intersection.getShadowRay(pointLight);


  //check if the shadow ray intersect 
  //any intersectables in the scene
  //before reaching the light source
  shadowHit = mScene->intersect(shadowRay);


  if(!shadowHit)
    {
      //get the incoming radiance of the light
      incomingRadiance = pointLight->getRadiance();
      
      //calculate the light vector - the irradiance
      lightVec = pointLight->getWorldPosition() - intersection.mPosition;
      lightVec.normalize();
      
      //calculate the incident angle
      incidentAngle = std::max(lightVec.dot(intersection.mNormal),0.0f);
      
      //get the BRDF
      BRDF = intersection.mMaterial->evalBRDF(intersection,lightVec);
      
      //accumulate the color for all the lights 
      outputColor += incomingRadiance * BRDF * incidentAngle;
    }

  ///////////////// POINT LIGHT ////////////////
  */
  
  
  //////////////////////// AREA LIGHT ///////////////////////////////
  
  //now sample this area light
  //you might consider several samples from differetn light sources

  //get the ray that intends to intersect the luminaire
  //here we sample the light explicitly
  lightRay = intersection.getShadowRay(areaLight);
  
  //check if the shadow ray intersect the scene more specifically
  //the luminaire
  lightHit = mScene->intersect(lightRay,lightIntersection);
  
  //check if the shadow ray hit the luminaire or not
  if(lightHit && lightIntersection.mMaterial->emissive())
    {
      //get the probability density function
      float pdf = areaLight->getPDF(lightIntersection,intersection);

      if(pdf > 0.0f)
	{
	  //calculate the radiance 
	  //value only if the pdf value 
	  //is greater than 0

	  pdf = 1.0f/pdf;

	  //calculate the visible point
	  Point3D visiblePoint = lightIntersection.mPosition - intersection.mPosition;
	  
	  //initialize a vector from the visible point
	  Vector3D vectorToLight(visiblePoint);
	  
	  //normalize the vector to the light
	  vectorToLight.normalize();
	  	  
	  //the emissive radiance from the light source
	  incomingRadiance = areaLight->getRadiance();
	  
	  //get the radiance the transfer value
	  float transfer = radianceTransfer(lightIntersection,intersection);
	  
	  //get the BRDF of the nearest interset intersection point
	  BRDF = intersection.mMaterial->evalBRDF(intersection,vectorToLight);

	  //output radiance is the result of the BRDF * Direct incoming radiance from the light source
	  // * radiance transfer function * probability distribution function
	  outputColor = BRDF * incomingRadiance * transfer * pdf;
	}      
    }
  
  
  //we return the direct light
  return  outputColor;
}

//calculate the radiance transfer between the two visible points
float PathTracer::radianceTransfer(const Intersection &shadowIntersection,const Intersection &intersection)
{
  
  //initialize the point from the light sample point and the shaded point in the scene
  Point3D visiblePoint = shadowIntersection.mPosition - intersection.mPosition;
  
  //initialize a vector from the vector
  Vector3D vectorToLight(visiblePoint);
  
  //get the length squared
  float lengthSquared = vectorToLight.length2();
  
  //normalize the vector to the light
  vectorToLight.normalize();
  
  //calculate the radiance transfer    
  //make sure to clamp the cosine to zero if they are negative
  float transfer = (std::max(intersection.mNormal.dot(vectorToLight),0.0f) * 
		    std::max(shadowIntersection.mNormal.dot(-vectorToLight),0.0f)) /  lengthSquared;  

  //make sure that the radiance transfer is positive
  return (transfer >= 0.0f) ? transfer : 0.0f;
}



Color PathTracer::computeIndirectIllumination(const Intersection &is,int depth)
{
   Color out;
   Color reflectedColor;
   Color refractedColor;
   Color directColor;
   
   Ray reflectedRay;
   Ray refractedRay;
   
   //declare a random instance  
   RandomGenerator random = RandomGenerator::getRandomInstance();
   
   
   //!absorbtion probability - conception is russin roulette
   
   //declare a good starting point as follows
   //which means that there is a 90% chance of continuing
   //and 10% chance of getting absorbed
   float alpha = 0.1;
   
   float probability = 0.0f;
   
   float pdf = 0.0f;
   
   float incidentAngle = 0.0f;
   
   Vector3D importanceSampleVector;

   //store the reflectivity and
   //refractivity coefficient
   float reflectivityCoefficient = 0.0;
   float refractivityCoefficient = 0.0;

   //hold the value that decide
   //which ray to trace
   float whichRayToTrace = 0.0f;
   
   //get the reflectivity coefficient of the nearest hit in the scene object
   reflectivityCoefficient = is.mMaterial->getReflectivity(is);
   
   //get the refractivity coefficient of the nearest hit in the scene object
   refractivityCoefficient = is.mMaterial->getTransparency(is);
   
   
   ///////////////////////////////// recursion depth of 5 /////////////////////////
   if(depth != 0)
   {
      
      //get a canonical random number to decide which ray to trace
      whichRayToTrace = static_cast<float>(random.generateDouble(0,1));

      //there weill be only one ray path to consider
      //so use russian roulette to decide which one
      
      if(whichRayToTrace < refractivityCoefficient)
      {
	 refractedRay = is.getRefractedRay();
	 refractedColor = trace(refractedRay,depth - 1,0);	      
      }
      else if(whichRayToTrace < reflectivityCoefficient)
      {
	 reflectedRay = is.getReflectedRay();
	 reflectedColor = trace(reflectedRay,depth -1,0);	      
      }
      else if(whichRayToTrace < (1 - reflectivityCoefficient - refractivityCoefficient))
      {	  
	 //importance sampling
	 importanceSampleVector = sampleCosine(is,pdf);
	 
	 if(pdf > 0.0f )
	 {
	    
	    //initiate a new ray based on the intersection
	    //the outbound ray
	    Ray newRay(is.mPosition  + is.mNormal * 0.001f,importanceSampleVector,1e-4f);
	    
	    incidentAngle = std::max(is.mNormal.dot(newRay.dir),0.0f);
	    
	    directColor = (trace(newRay,depth - 1,0) * is.mMaterial->evalBRDF(is,newRay.dir) * incidentAngle) / pdf;
	 }
      }
      
      out = directColor * (1 - reflectivityCoefficient - refractivityCoefficient) +
	 (reflectedColor * reflectivityCoefficient) +
	 (refractedColor * refractivityCoefficient);	        
      
   }
   else
   {
      //get the probability value
      //this random number will 
      //decide if the ray absorbed 
      //or should be continued
      probability =  static_cast<float>(random.generateDouble(0,1));
      
      
      //alpha is set to 0.1
      //if the generaed random number is less than alpha
      //we return BLACK
      //OTHERWISE WE CONTINUE TRACING
      if(probability <  alpha)
      {
	 //absorbed - so return black
	 return Color(0.0f,0.0f,0.0f);
      }
      else
      {          	  
	 //get a canonical random number to decide which ray to trace
	 float whichRayToTrace = static_cast<float>(random.generateDouble(0,1));
	 
	 
	 if(whichRayToTrace < refractivityCoefficient)
	 {
	    refractedRay = is.getRefractedRay();
	    refractedColor = trace(refractedRay,0,0);	      
	 }
	 else if(whichRayToTrace < reflectivityCoefficient)
	 {
	    reflectedRay = is.getReflectedRay();
	    reflectedColor = trace(reflectedRay,0,0);	      
	 }
	 else if(whichRayToTrace < (1 - reflectivityCoefficient - refractivityCoefficient))
	 {	  
	    //importance sampling
	    importanceSampleVector = sampleCosine(is,pdf);
	    
	    if(pdf > 0.0f )
	    {	       
	       //initiate a new ray based on the intersection
	       //the outbound ray
	       Ray newRay(is.mPosition  + is.mNormal * 0.001f,importanceSampleVector,1e-4f);
	       
	       incidentAngle = std::max(is.mNormal.dot(newRay.dir),0.0f);
	       
	       directColor = (trace(newRay,0,0) * is.mMaterial->evalBRDF(is,newRay.dir) * incidentAngle) / pdf;

	       //directColor = Color(0.0f,1.0f,0.0f);
	    }
	 }
	 
	 out = directColor * (1 - reflectivityCoefficient - refractivityCoefficient) +
	    (reflectedColor * reflectivityCoefficient) +
	    (refractedColor * refractivityCoefficient);	 
      }      
      
      out /= (1 - alpha);
   }
   return out;
}


/** Returns a random direction on the hemisphere centered around the 
*	intersection point's normal. The returned direction must be normalized
*	and should be taken from the cosine-weighted probability distribution
*	function. The probability of the sample should be returned in pdf.
*/
/*
Vector3D PathTracer::sampleCosine(const Intersection& is, float& pdf) const
{
   
   Vector3D d;
   
   //get the random instance
   RandomGenerator rand = RandomGenerator::getRandomInstance();
   
   float azimuth = 2 * M_PI * static_cast<float>(rand.generateDouble(0,1));
   
   //generate  a random number between 0 and 1
   float r2 = static_cast<float>(rand.generateDouble(0,1));

   float x = cos(azimuth) * sqrtf(std::max(1.0f - r2,0.0f));
   float y = sin(azimuth) * sqrtf(std::max(1.0f - r2,0.0f));
   float z = sqrtf(std::max(r2,0.0f));

   //compute the world space for the random vector
   Vector3D tangent = is.mNormal.cross(Vector3D(1.0f,0.0f,0.0f));

   if(tangent.length() < 0.1)
      tangent = is.mNormal.cross(Vector3D(0.0f,1.0f,0.0f));

   Vector3D bitangent = is.mNormal.cross(tangent);

   //transformed random direction
   d = x * tangent + y * bitangent + z * is.mNormal;
   d.normalize();
   

   //   float cos_theta = std::max(d.dot(is.mNormal),0.0f);
   float cos_theta = d.dot(is.mNormal);

   //cosine lobe pdf!
   pdf = sqrtf(cos_theta)/M_PI;
   

   return d;     
}

*/


Vector3D PathTracer::sampleCosine(const Intersection& is, float& pdf) const
{
   
   Vector3D d;
   
   //get the random instance
   RandomGenerator random = RandomGenerator::getRandomInstance();

   float random_phi = 2 * M_PI * static_cast<float>(random.generateDouble(0,1));
   
   //generate  a random number between 0 and 1
   float random_theta = static_cast<float>(random.generateDouble(0,1));
   

   float x = cos(random_phi) * std::sqrt(random_theta);
   float y = sin(random_phi) * std::sqrt(random_theta);
   float z = std::sqrt(std::max(0.0f,1 - random_theta));


   
   Vector3D w = is.mNormal;
   Vector3D u = (( fabs(w.x) > .1 ? Vector3D(0,1,0) : Vector3D(1,0,0)) % w).normalize();
   Vector3D v = w % u;

   d =( u * x + v * y + w * z ).normalize();
   


   float cos_theta = std::max(d.dot(is.mNormal),0.0f);
   
   //cosine lobe pdf!
   pdf = cos_theta/M_PI;

   return d;     
}
