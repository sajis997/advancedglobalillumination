#include "photontracer.h"


/*

#include <omp.h>
#include <cassert>

#include "defines.h"
#include "scene.h"
#include "camera.h"
#include "ray.h"
#include "intersection.h"
#include "material.h"
#include "timer.h"
#include "image.h"

#include "randomgenerator.h"

#include "sampler.h"
#include "arealight.h"


PhotonTracer::PhotonTracer(Scene *scene, Image *img)
   : Raytracer(scene,img)
{
   //clear the map
   mCausticPhotonStorage.clear();

   mGlobalPhotonStorage.clear();
   
   //emit photon from the light source
   //store in the photon map
   preprocess();
}

PhotonTracer::~PhotonTracer()
{
   //clear the map
   mCausticPhotonStorage.clear();

   mGlobalPhotonStorage.clear();
}

void PhotonTracer::computeImage()
{

}



void PhotonTracer::preprocess()
{

   //declare a timer object
   //to track the time of BVH construction
   Timer bvhTimer;
   
   //get the area light
   AreaLight *areaLight = mScene->getAreaLight();

   //make sure that we have the area light
   assert(areaLight != 0);

   //get the ray from the area light
   //since the area light contains set of shapes
   Ray ray = areaLight->getSampleRay();
   
   //get the radiance of the area light
   Color lightColor = areaLight->getRadiance();

   unsigned int causticPhotonCounter = 0;
   unsigned int globalPhotonCounter = 0;

   //scale the power of the light with the number of caustic photons
   Color causticLightColor = lightColor/ray_pm_emitted_caustics;
   
   while(causticPhotonCounter < ray_pm_emitted_caustics)
   {
      //get the ray from the area light
      ray = areaLight->getSampleRay();

      traceCausticPhoton(ray,causticLightColor);

      causticPhotonCounter++;      
   }

   //so all the caustic photons are accumulated
   //now it is time to build the tree
   mCausticsTree.build(mCausticPhotonStorage);

   Color globalLightColor = lightColor / ray_pm_emitted_radiance;

   while(globalPhotonCounter < ray_pm_emitted_radiance)
   {
      ray = areaLight->getSampleRay();

      traceGlobalPhoton(ray,globalLightColor);

      globalPhotonCounter++;
   }

   mRadianceTree.build(mGlobalPhotonStorage);


   std::cout << "KD-tree construction done in " << bvhTimer.stop() << " seconds" << std::endl;   
   
}

void PhotonTracer::traceGlobalPhoton(const Ray &ray,Color &color)
{
   Intersection is;

   bool hit = mScene->intersect(ray,is);

   float specularReflectivityCoefficient = 0.0;
   float refractivityCoefficient = 0.0;
   float diffuseReflectivityCoefficient = 0.0;

   //get the random instance
   RandomGenerator random = RandomGenerator::getRandomInstance();
   
   
   if(hit)
   {
      //first we generate the caustic photons, store them
      //and build a kdtree out of it

      
	SOME ISSUES ABOUT PHOTON
	 - PHOTONS ARE created at the light sources in the model
	 - the lights are the typical computer graphics light source
	 - a large number of photons is typically emitted from each light source
	 - the power of the light source is divided among all the emitted photons
	 - and each photon therefore transports a fraction of the light source power
	 - IMPORTANT - the power of the photons is proportional only to the number
	   of emitted photons
         - PHOTON TRACING WORKS EXACTLY THE SAME WAY AS RAY TRACING, EXCEPT THE FACT THAT
	   PHOTONS PROPAGATE FLUX WHEREAS RAYS GATHER RADIANCE
      


      //the light color is propagated at the intersection point
      //color *= is.mMaterial->evalBRDF(is,-ray.dir);
      
      float whichRayToTrace = static_cast<float>(random.generateDouble(0,1));
      
      //get the reflectivity coefficient of the nearest hit in the scene object
      //the following gives the specular reflection 
      specularReflectivityCoefficient = is.mMaterial->getReflectivity(is);
      
      //get the refractivity coefficient of the nearest hit in the scene object
      refractivityCoefficient = is.mMaterial->getTransparency(is);

      diffuseReflectivityCoefficient = 1 - specularReflectivityCoefficient - refractivityCoefficient;

      if(whichRayToTrace < specularReflectivityCoefficient)
      {
	 //get the specularly reflected ray
	 Ray reflectedRay = is.getReflectedRay();

	 //trace the reflected photon recursively
	 traceGlobalPhoton(reflectedRay,color);
      }
      else if(whichRayToTrace < refractivityCoefficient)
      {
	 //get the refracted ray
	 Ray refractedRay = is.getRefractedRay();

	 traceGlobalPhoton(refractedRay,color);
      }
      else if(whichRayToTrace < diffuseReflectivityCoefficient)
      {
         //populate the map
	 mGlobalPhotonStorage.insert(make_pair(is.mPosition,Photon(ray.dir,color)));


	 //should i trace again or just absorb

	 float tracingProbability = static_cast<float>(random.generateDouble(0,1));

	 if(tracingProbability >= 0.1)
	 {
	    Vector3D d;
	    
	    //get the random instance
	    RandomGenerator random = RandomGenerator::getRandomInstance();
	    
	    float random_phi = 2 * M_PI * static_cast<float>(random.generateDouble(0,1));
	    
	    //generate  a random number between 0 and 1
	    float random_theta = static_cast<float>(random.generateDouble(0,1));
	    
	    
	    float x = cos(random_phi) * std::sqrt(random_theta);
	    float y = sin(random_phi) * std::sqrt(random_theta);
	    float z = std::sqrt(std::max(0.0f,1 - random_theta));
	    
	    
	    
	    Vector3D w = is.mNormal;
	    Vector3D u = (( fabs(w.x) > .1 ? Vector3D(0,1,0) : Vector3D(1,0,0)) % w).normalize();
	    Vector3D v = w % u;
	    
	    d =( u * x + v * y + w * z ).normalize();
	    
	    
	    //generate the random direction for the diffuse surface and trace ray in that direction
	    Ray newRay(is.mPosition  + is.mNormal * 0.001f,d,1e-4f);


	    traceGlobalPhoton(newRay,color);
	 }	 
      }      
   }    
}

void PhotonTracer::traceCausticPhoton(const Ray &ray,Color &color)
{
   Intersection is;
   
   float specularReflectivityCoefficient = 0.0;
   float diffuseReflectivityCoefficient = 0.0;
   float refractivityCoefficient = 0.0;

   //get the random instance
   RandomGenerator random = RandomGenerator::getRandomInstance();

   //declare a good starting point as follows
   //which means that there is a 90% chance of continuing
   //and 10% chance of getting absorbed
   float alpha = 0.1;
   

   bool hit = mScene->intersect(ray, is);
   
   if(hit)
   {
      //first we generate the caustic photons, store them
      //and build a kdtree out of it

      
	SOME ISSUES ABOUT PHOTON
	 - PHOTONS ARE created at the light sources in the model
	 - the lights are the typical computer graphics light source
	 - a large number of photons is typically emitted from each light source
	 - the power of the light source is divided among all the emitted photons
	 - and each photon therefore transports a fraction of the light source power
	 - IMPORTANT - the power of the photons is proportional only to the number
	   of emitted photons
         - PHOTON TRACING WORKS EXACTLY THE SAME WAY AS RAY TRACING, EXCEPT THE FACT THAT
	   PHOTONS PROPAGATE FLUX WHEREAS RAYS GATHER RADIANCE
       
      //the light color is propagated at the intersection point
      //SINCE I AM USING THE RUSSIAN ROULETTE HERE, WE DO NOT NEED TO
      //MODIFY THE POWER OF THE REFLECTED PHOTON HERE
      //SO COMMENT THE FOLLOWING LINE

      //NEW FINDINGS, ACCIRDING TO THE DISSERTATION BY jAROSZ
      //THE POWER OF THE PHOTON HAS TO BE SCALED

      //i m wrong, i am using russian roulette and
      //i am not scaling the power of the photon
      //UNCOMMENT AGAIN
      //color *= is.mMaterial->evalBRDF(is,-ray.dir);
           
      //get the specular reflectivity coefficient of the nearest hit in the scene object
      //the following gives the specular reflection 
      specularReflectivityCoefficient = is.mMaterial->getReflectivity(is);
      
      //get the refractivity coefficient of the nearest hit in the scene object
      refractivityCoefficient = is.mMaterial->getTransparency(is);

      //calculate the diffuse reflectivity coefficient from the other above componenet we have found
      diffuseReflectivityCoefficient = 1 - specularReflectivityCoefficient - refractivityCoefficient;
      


      float whichRayToTrace = static_cast<float>(random.generateDouble(0,1));

      
      if(whichRayToTrace < refractivityCoefficient)
      {
	 //get the refracted ray
	 Ray refractedRay = is.getRefractedRay();
	 
	 traceCausticPhoton(refractedRay,color);
	 
      }
      else if(whichRayToTrace < specularReflectivityCoefficient)
      {
	 //get the specularly reflected ray
	 Ray reflectedRay = is.getReflectedRay();
	 
	 //trace the reflected photon recursively
	 traceCausticPhoton(reflectedRay,color);
      }
      else if(whichRayToTrace < diffuseReflectivityCoefficient)
      {
	 //diffuse material do not generate caustics
	 
	 //store the photon and absorb
	 mCausticPhotonStorage.insert(make_pair(is.mPosition,Photon(ray.dir,color)));
      }	            
   }   
}

*/
