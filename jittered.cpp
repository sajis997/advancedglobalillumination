#include "jittered.h"

#include "randomgenerator.h"

Jittered::Jittered()
   : Sampler()
{
   //not calling the generateSamples() here
   //because by default it will call the default
   //sampling that shoots rays at the middle of the pixel
}

Jittered::Jittered(const int numSamples)
   : Sampler(numSamples)
{
   //generate the jittered samples
   generateSamples();
}

Jittered::Jittered(const int numSamples, const int m)
  : Sampler(numSamples,m)
{
  generateSamples();
}

Jittered::Jittered(const Jittered &js)
  : Sampler(js)
{
  generateSamples();
}

Jittered& Jittered::operator=(const Jittered &rhs)
{
   if (this == &rhs)
      return (*this);
   
   Sampler::operator= (rhs);
   
   return (*this);
}

Jittered::~Jittered()
{

}


void Jittered::generateSamples()
{
  unsigned int n = static_cast<unsigned int>(sqrt(mNumSamples));
  
  
  RandomGenerator random = RandomGenerator::getRandomInstance(); 

  ///////////////////////////////////////////////////////////////////////////////
  //unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  
  //std::uniform_real_distribution<float> distribution(j,n-1);
  //std::minstd_rand0 generator(seed);
  
  //unsigned int k = distribution(generator);
  ///////////////////////////////////////////////////////////////////////////////
  
  for(unsigned int p = 0; p < mNumSets; p++)
    for(unsigned int j= 0 ; j < n; j++)
      for(unsigned int k = 0; k < n; k++)
        {
          Point3D point((k + random.generateFloat())/n,(j + random.generateFloat())/n,1.0f );
          
          mSamples.push_back(point);
        }
}
