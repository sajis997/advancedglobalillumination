#ifndef HITPOINT_INFO_H
#define HITPOINT_INFO_H

#include "aabb.h"
#include "hitpoint.h"


class HitPointInfo
{
   HitPointInfo(const HitPoint &hitPoint, const AABB &aabb)
      : mHitPoint(const_cast<HitPoint*>(&hitPoint)),
	mHitPointBound(aabb)	
   {
      
   }

   HitPoint *mHitPoint;

   AABB mHitPointBound;
};


#endif
