#ifndef HDRPROBESCENE_H
#define HDRPROBESCENE_H

#include "scene.h"
#include "camera.h"
#include "sampler.h"


void buildHDRProbeScene(Scene *scene);

void setupHDRProbeCamera(Camera *camera);


#endif
