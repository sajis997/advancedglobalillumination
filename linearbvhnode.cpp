#include "linearbvhnode.h"



LinearBVHNode::LinearBVHNode():
   n_objs(0),
   leaf(false)
{
}

LinearBVHNode::~LinearBVHNode()
{
}


bool LinearBVHNode::isLeaf() const
{
   return leaf;
}

int LinearBVHNode::getSplitAxis() const
{
   return splitAxis;
}


unsigned int LinearBVHNode::getIndex() const
{
   return index;
}

unsigned int LinearBVHNode::getNumberObjects() const
{
   return n_objs;
}

AABB &LinearBVHNode::getAABB()
{
   return bbox;
}


void LinearBVHNode::setAABB(AABB &box)
{
   bbox = box;
}


void LinearBVHNode::makeLeaf(unsigned int i, unsigned int n_objects)
{
   leaf = true;
   //index to the primitive
   index = i;
   n_objs = n_objects;
}

//the current node will the interior node
void LinearBVHNode::makeNode(unsigned int i,
			     unsigned int n_objects,
			     int axis)
{
   //flag the node as the interior node
   leaf = false;
 
   //index to the right node
   index = i;

   //number of objects under this node
   n_objs = n_objects;

   //the coordinate axis along
   //which the primitives are sorted
   splitAxis = axis;
}

