#include <cassert>

#include "defines.h"
#include "brdf.h"
#include "multijittered.h"

BRDF::BRDF(void)
  : mSampler(NULL)
{

}

BRDF::BRDF(const BRDF &brdf)
{
  if(brdf.mSampler)
    mSampler = brdf.mSampler;
  else
    mSampler = NULL;
}



BRDF&	BRDF::operator= (const BRDF& rhs)
{
	if (this == &rhs)
		return (*this);
  
	if (mSampler) 
    {
      delete mSampler;
      mSampler = NULL;
    }
  
	if (rhs.mSampler)
		mSampler	= rhs.mSampler;
  
	return (*this);
}

BRDF::~BRDF(void)
{
  if(mSampler)
    {
      delete mSampler;
      mSampler = NULL;
    }
}

void BRDF::setSampler(Sampler* sPtr)
{
  assert(sPtr != NULL);
  
  //set the pointer
  mSampler = sPtr;
}



