/*
 *  triangle.h
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-02-22.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "intersectable.h"

class Mesh;
class Sampler;
/**
 * Class representing a single triangle. 
 * This class is used by Mesh to represent the triangles in the mesh.
 * It has functions for computing the triangle's face normal, area,
 * and for intersection testing with a ray.
 */
class Triangle : public Intersectable
{	
public:
   /// \cond INTERNAL_CLASS
   
   /// Internal class representing the vertex indices.
   struct vertex
   {
      int p; ///< Position index.
      int n; ///< Normal index.
      int t; ///< Auxiliary index (texture coordinate).
   };
   
   /// \endcond // INTERNAL_CLASS
   
   Triangle();
   Triangle(Mesh* owner, const vertex& vtx1, const vertex& vtx2, const vertex& vtx3, Material *mtl);
   
   Vector3D getFaceNormal() const;

   virtual float getArea() const;

   void prepare();

   ///////////////ADDED BY SAJJAD ////////////////
   void setSampler(Sampler *sampler);
   //////////////////////////////////////////////
   
   // Implementation of the Intersectable interface:
   bool intersect(const Ray& ray) const;
   bool intersect(const Ray& ray, Intersection& isect) const;
   void getAABB(AABB& bb) const;
   UV calculateTextureDifferential(const Point3D& p, const Vector3D& dp) const;
   Vector3D calculateNormalDifferential(const Point3D& p, const Vector3D& dp, bool isFrontFacing) const;

   /////////////// added and over-ridden by sajjad ////////////
   bool sampleDirection(const Intersection&,Vector3D&);

   float getPDF(const Intersection&,
		const Intersection&);

   Ray sampleRay();

   /////////////////////////////////////////////////////////////
   
   //get the point3d at the vertex index i
   const Point3D& getVtxPosition(int i) const;

   //get the vertex normal at the index i
   const Vector3D& getVtxNormal(int i) const;
   const UV& getVtxTexture(int i) const;

   //get the material for  the triangle
   //if the tringle is initialize while reading the 
   //.obj file then the mMaterial will contin NULL
   Material *getMaterial() const {return mMaterial;}
   
protected:
   Vector3D mPlanes[3];
   Vector3D mPlaneOffsets;
   Mesh* mMesh;			///< Ptr to the mesh this triangle belongs to.
   vertex mVtx[3];		///< The three vertices of the triangle.	

   
   Material *mMaterial;		///< Material of the triangle.

   Sampler *mSampler;
   
   friend class Mesh;

   float mPDF;
};

#endif
