#include "bvhnode.h"
#include <cassert>

BVHNode::BVHNode()
    : m_leaf(false), // initially we are creating all the nodes to be the interior node
      m_left(NULL),  //both the left and right child nodes are NULL initially
      m_right(NULL),
      m_numberObjects(0) // no intersectables assigned initially
{

}

BVHNode::~BVHNode()
{
    if(m_left)
        delete m_left;
    if(m_right)
        delete m_right;
}

void BVHNode::setAABB(AABB &bound)
{
    m_bbox = bound;
}

void BVHNode::makeLeft(BVHNode *left,unsigned int objects)
{
    //make sure that the sent left node is not NULL
    assert(left != 0);

    //assign the left child node
    m_left = left;

    //it is not leaf anymore, it is an interior node now
    m_leaf = false;

    //set the number of objects for the left child node
    m_left->setNumberObjects(objects);
}

BVHNode* BVHNode::getLeft() const
{
    return m_left;
}

void BVHNode::makeRight(BVHNode *right,unsigned int objects)
{
    assert(right != 0);

    m_right = right;
    m_leaf = false;
    m_right->setNumberObjects(objects);
}

BVHNode* BVHNode::getRight() const
{
    return m_right;
}

bool BVHNode::isLeaf() const
{
    return m_leaf;
}


//make the current node the leaf node
void BVHNode::makeLeaf(unsigned int left_index,unsigned int intersectables)
{
    m_index = left_index; //set the index to the left intersectable
    m_numberObjects = intersectables;
    m_leaf = true;
}


AABB& BVHNode::getAABB()
{
    return m_bbox;
}

unsigned int BVHNode::getIndex() const
{
    return m_index;
}

unsigned int BVHNode::getNumberObjects() const
{
    return m_numberObjects;
}

void BVHNode::setNumberObjects(unsigned int intersectables)
{
    m_numberObjects = intersectables;
}
