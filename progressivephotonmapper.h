#ifndef PROGRESSIVE_PHOTON_MAPPER_H
#define PROGRESSIVE_PHOTON_MAPPER_H

#include "defines.h"
#include "raytracer.h"
#include "matrix.h"


using namespace std;

class Ray;
class Intersection;
class BVHAccelerator;
class HitPoint;

class ProgressivePhotonMapper : public Raytracer
{
public:
   ProgressivePhotonMapper(Scene *scene, Image *img);

   virtual void computeImage();

   ~ProgressivePhotonMapper();

protected:

   //the following initiate the
   //rays from the eye
   void forwardTracingPass();

   //trace light ray from the light source
   void photonTracingPass();

   //similar to the typical whitted ray tracing pass
   Color traceFromEye(int x,int y);

   Color traceFromLight(const Ray &photonRay,
			const Color &photonFLux,
			int photonBounceCounter);

   //compute the direct illumination
   Color computeDirectIllumination(const Intersection &intersection);

private:

   float radianceTransfer(const Intersection &shadowIntersection,const Intersection &intersection);

   Color trace(const Ray &ray,const int &x,const int &y,const float &weight,int depth);

   Vector3D sampleCosine(const Intersection& is, float& pdf) const;   

   void adjustRadiusAndFlux();
   
   void setPixelPerPass(unsigned int);

   BVHAccelerator *mBVHAccelerator;

   //vector to hold all the hit points
   std::vector<HitPoint*> mHitPoints;
};


#endif
