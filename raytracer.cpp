/*
 *  raytracer.cpp
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-02-13.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */

#include "defines.h"
#include "scene.h"
#include "image.h"
#include "camera.h"
#include "ray.h"
#include "intersection.h"
#include "material.h"
#include "timer.h"
#include "raytracer.h"
#include "sampler.h"
#include "thinlens.h"

/**
 * Creates a raytracer.
 * @param scene Point3Der to the scene.
 * @param img Point3Der to an Image object where the output will be stored.
 */
Raytracer::Raytracer(Scene* scene, Image* img) : mScene(scene), mImage(img), mCamera(0)
{
   if (!mScene || !mImage)
      throw std::runtime_error("(Raytracer::Raytracer) null pointer");
   
   if (mScene->getNumberOfCameras() < 1)
      throw std::runtime_error("scene has no camera");
   
   // Use the first camera.
   mCamera = mScene->getCamera(0); 
   
   //set the sampler to null
   mSampler = 0;   
}



void Raytracer::setSampler(Sampler *sampler)
{
   if(mSampler != 0)
   {
      delete mSampler;
      mSampler = 0;
   }
   
   mSampler = sampler;
}

