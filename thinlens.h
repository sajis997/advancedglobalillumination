#ifndef THIN_LENS_H
#define THIN_LENS_H

#include "camera.h"

class Image;
class Ray;
class Sampler;

/*
  distnace to the view plane is not arbitrary
  and ray origin is randomly chosen 
 */
class ThinLens : public Camera
{
 public :
  ThinLens(Image *img);
  virtual ~ThinLens();
  
  void setSampler(Sampler *sampler);
  
  void setRadius(float r);
  
  void setFocalPlaneDistance(float distance);
  
	virtual Ray getRay(float x, float y) const;
  
 protected:
  
  Sampler *mSampler;
  float mFocalPlaneDistance;
  float mRadius;  
  
  
};

#endif
