#ifndef HIT_POINT_H
#define HIT_POINT_H

#include "intersection.h"
#include "color.h"



class HitPoint
{
public:
   HitPoint();

   ~HitPoint();

   /// Write elements to an output stream.
   friend std::ostream& operator<<(std::ostream& os, const HitPoint& h);	   

   Intersection mIntersection;
   int mPixelX;
   int mPixelY;
   float mPixelWeight;
   float mRadius;
   Color mDirectIllumination;

   float mPhotonCount;
   int mNewPhotonCount;
   Color mTotalFlux;


};




#endif
