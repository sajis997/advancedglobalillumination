#ifndef COMPARE_POINTS_H
#define COMPARE_POINTS_H

#include "matrix.h"
#include "hitpoint.h"

class ComparePoints
{

   //store the sorting dimension
   unsigned int mSortingDimension;

   float mCenterValue;
   
public:

   ComparePoints(float center, unsigned int axis)
      : mSortingDimension(axis),
	mCenterValue(center)
   {

   }
   


   bool operator()(HitPoint *p)
   {
      Point3D point = p->mIntersection.mPosition;
      
      return (point)(mSortingDimension) < mCenterValue;;
   }
   
};

#endif
