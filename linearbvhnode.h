#ifndef LINEARBVHNODE_H
#define LINEARBVHNODE_H


#include "aabb.h"

class LinearBVHNode
{
private:
   
   //if interior node , index to the right child node
   //if leaf node, index to the first intersectable of the primitives vector
   unsigned int index;
   
   //number of primitives under this node
   unsigned int n_objs;

   //hold the split axis for the node
   int splitAxis;
   
   //boolean flag indicator if it is a leaf node or not
   bool leaf;

   //bounding box of this node
   AABB bbox;
   
public:
   LinearBVHNode();
   
   ~LinearBVHNode();

   int getSplitAxis() const;
   
   bool isLeaf() const;

   //get the index to the right child node
   unsigned int getIndex() const;
   
   unsigned int getNumberObjects() const;
   
   AABB &getAABB();

   
   void setAABB(AABB &box);

   void makeLeaf(unsigned int i, unsigned int n_objects);

   //with the following function the current node
   //decides the right child index and we do not need
   //to worry about the left child since it sits right
   //next to the current node in the node list
   void makeNode(unsigned int i,
		 unsigned int n_objects,
		 int axis);   
};

#endif // LINEARBVHNODE_H
