#include "perfectspecular.h"


PerfectSpecular::PerfectSpecular()
   : BRDF(),
     kr(0.0f),
     cr(Color(1.0f,1.0f,1.0f))
{
}

PerfectSpecular::PerfectSpecular(const PerfectSpecular &perspec)
   : BRDF(perspec),
     kr(perspec.kr),
     cr(perspec.cr)
{

}

PerfectSpecular& PerfectSpecular::operator= (const PerfectSpecular &rhs)
{
   if(this == &rhs)
      return (*this);


   BRDF::operator= (rhs);

   kr = rhs.kr;
   cr = rhs.cr;

   return (*this);
}

PerfectSpecular::~PerfectSpecular()
{

}


float PerfectSpecular::getReflectivity() const
{
   return kr;
}

void PerfectSpecular::setReflectivity(float r)
{
   kr = r;
}



Color PerfectSpecular::f(const Intersection& sr, const Vector3D& wo, const Vector3D& wi) const
{
   return Color(0.0f,0.0f,0.0f);
}

Color PerfectSpecular::sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi) const
{
   return Color(0.0f,0.0f,0.0f);
}

Color PerfectSpecular::sample_f(const Intersection& sr, const Vector3D& wo, Vector3D& wi, float& pdf) const
{
   return Color(0.0f,0.0f,0.0f);
}

Color PerfectSpecular::rho(const Intersection& sr, const Vector3D& wo) const
{
   return Color(0.0f,0.0f,0.0f);
}
