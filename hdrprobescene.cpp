#include "hdrprobescene.h"
#include "diffuse.h"
#include "phong.h" 
#include "sphere.h"
#include "mesh.h"
#include "emissive.h"
#include "lightprobe.h"
#include "checker.h"

void buildHDRProbeScene(Scene *scene)
{
   //create a white diffuse surface
   Diffuse* red = new Diffuse(Color(0.7f, 0.1f, 0.1f));
   Diffuse* blue = new Diffuse(Color(0.1f, 0.1f, 0.7f));
   Diffuse* white = new Diffuse(Color(0.7f, 0.7f, 0.7f));
   Diffuse* black = new Diffuse(Color(0.0f, 0.0f, 0.0f));

   Checker* matteBlueWhite = new Checker(black, white, 12, 12);
   //matteBlueWhite->setReflectivity(0.5f);
   
   Diffuse* sphereWhite = new Diffuse(Color(1.0f, 1.0f, 1.0f));
   sphereWhite->setTransparency(0.9);
   sphereWhite->setIndexOfRefraction(1.5);

   Phong *spherePhong = new Phong(Color(0.7f, 0.7f, 0.7f),
				  Color(0.9f, 0.9f, 0.9f),
				  100.001,0.9);   
    
  
  //the ground to stand on for the rest of the meshes
  Mesh* ground = new Mesh("data/plane.obj", matteBlueWhite);
  ground->setScale(50.0f);


  /*
  Mesh *smoothCube = new Mesh("data/smoothcube.obj",spherePhong); 
  smoothCube->setScale(1.8f);// scale by a factor 0.8
  smoothCube->setRotation(0, -150, 0);	// rotate -70 degrees around Y
  smoothCube->setTranslation(Vector3D(-23.5f, 16.5f, 0.0f));   
  */

  Mesh* elephant = new Mesh("data/elephant.obj", sphereWhite);	// load mesh
  elephant->setScale(7.8f);							
  elephant->setRotation(0, 145, 0);						
  elephant->setTranslation(Vector3D(17.5f, 0.0f, 11.0f));

  scene->add(ground);
  scene->add(elephant);

  //scene->add(smoothCube);
  
}

void setupHDRProbeCamera(Camera *camera)
{
  
   Vector3D up(0.0f, 1.0f, 0.0f);
   Point3D pos(0.0f, 40.0f, 190.0f);
   Point3D target(0.0f, 60.0f, 0.0f);
   camera->setLookAt(pos, target, up, 52.0f);
  

   /*
   Point3D pos = Point3D(22, 24, 26);				// camera position
   Point3D target = Point3D(0, 2, 0);				// camera target
   Vector3D up = Vector3D(0, 1, 0);				// up vector
   
   camera->setLookAt(pos, target, up, 52.0f);		// setup camera with fov 52 degrees
   */
}
