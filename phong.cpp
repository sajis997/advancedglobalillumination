/*
 *  phong.cpp
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-03-06.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */
#include "defines.h"
#include "intersection.h"
#include "phong.h"

using namespace std;


	
/** Constructs a Phong material with diffuse color d,
*	specular color s, and exponent (shininess) e,
*	and (optionally) reflectivity r and transparency t,
*	and index of refraction n.
*/
Phong::Phong(const Color& d, const Color& s, float exp, float r, float t, float e ,float n)
  //initialize the material with the reflectance, transparency, and index of refraction
: Material(r,t,n)
, mDiffColor(d)
, mSpecColor(s)
, mExponent(e)
{
}


/** Destructor. Does nothing. */	
Phong::~Phong() { }
	

/** Returns the BRDF at the intersection is for the light direction L. */
Color Phong::evalBRDF(const Intersection& is, const Vector3D& L)
{
   const Vector3D& N = is.mNormal;
   const Vector3D& V = is.mView;
	
   // Compute diffuse weight (kd)
   float kd = N * L;						// kd = dot(N,L)
   if(kd<0.0f) kd = 0.0f;					// if kd<0 the light is on the back-side -> return black	
   
   // Compute reflected view direction and specular weight (ks).
   Vector3D R = (2.0f*(N*V))*N - V;
   float ks = R * L;						// ks = dot(R,L)
   if(ks<0.0f) ks = 0.0f;					// clip to 0 if negative
   ks = std::pow(ks, mExponent);			// ks = ks^exponent
   
   return kd*mDiffColor + ks*mSpecColor;	// return weighted color (diffuse+specular)
}

