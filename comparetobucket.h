#ifndef COMPARETOBUCKET_H
#define COMPARETOBUCKET_H

#include <cassert>
#include "aabb.h"


class CompareToBucket
{
public:
   CompareToBucket(int split, int num, int d, const AABB &b)
      : centroidBounds(b),
	splitBucket(split),
	nBuckets(num),
	dim(d)
   {

   }


   bool operator()(BVHPrimitiveInfo *p) const
   {
      int b = nBuckets * ((p->mCentroid(dim) - centroidBounds.mMin(dim)) /
			   (centroidBounds.mMax(dim) - centroidBounds.mMin(dim)));

      if(b == nBuckets)
	 b = nBuckets - 1;

      assert(b >= 0 && b < nBuckets);
      return b <= splitBucket;
   }


   int splitBucket;
   int nBuckets;
   int dim;
   AABB centroidBounds;
};


#endif
