#ifndef RANDOM_GENERATOR
#define RANDOM_GENERATOR

class RandomGenerator
{
 public:
  
  static RandomGenerator &getRandomInstance()
  {
    static RandomGenerator random;

    return random;
  }
  
  //return double in the range of [low...max]
  double generateDouble(double low,double max);
  
  float generateFloat();
  
  /*
    returns an interger in the range [low....max]
    i.e low inclusive and excluding max
  */
  unsigned long generateInt(unsigned long low,unsigned long max);
  unsigned long generateInt();
  
  static void setSeed(int seed);
  
 private:
  RandomGenerator();
  
  static int initializer;
  static unsigned long seedValue;
  unsigned long lcgRandom;
  static unsigned long M,A,C;
};

#endif
