/*
 *  intersection.cpp
 *  prTracer
 *
 *  Created by Rasmus Barringer on 2011-02-04.
 *  Copyright 2011 Lund University. All rights reserved.
 *
 */
#include <cmath>
#include "intersection.h"
#include "intersectable.h"
#include "arealight.h"


Ray::Differential Intersection::calculatePositionDifferential() const
{
   //get the ray direction
   const Vector3D& D = mRay.dir;

   //get the normal at the hit point
   const Vector3D& N = mNormal;
   
   Ray::Differential dp;
   
   Vector3D negDproj = -D / D.dot(N);
   Vector3D t;
   
   t = mRay.dp.dx + mHitTime*mRay.dd.dx;
   dp.dx = t + t.dot(N)*negDproj;
   
   t = mRay.dp.dy + mHitTime*mRay.dd.dy;
   dp.dy = t + t.dot(N)*negDproj;
   
   return dp;
}

Ray Intersection::getReflectedRay() const
{
   //we get the ray direction
   //from it we device the
   //view direction
   const Vector3D& D = mRay.dir;

   //normal from the hit point
   Vector3D N = mNormal;
   N.normalize();

   // DONE: Implement reflection.
   // --------

   // Resulting reflection vector
   Vector3D R = 2*(N*mView)*N - mView; 
   
   R = R.normalize();
   
   // --------
   


   Ray::Differential dp = calculatePositionDifferential();
   Ray::Differential dd;
   Vector3D dn;
   
   float dDotN = D.dot(N);
   
   dn = mObject->calculateNormalDifferential(mPosition, dp.dx, mFrontFacing);
   dd.dx = mRay.dd.dx - 2.0f*(dDotN*dn + (mRay.dd.dx.dot(N) + D.dot(dn))*N);
   
   dn = mObject->calculateNormalDifferential(mPosition, dp.dy, mFrontFacing);
   dd.dy = mRay.dd.dy - 2.0f*(dDotN*dn + (mRay.dd.dy.dot(N) + D.dot(dn))*N);
   
   return Ray(mPosition + N*0.001f, R, dp, dd);
}

Ray Intersection::getRefractedRay() const
{
   //device the view direction
   const Vector3D& D = mRay.dir;

   Vector3D N = mNormal;
   
   float iEta = 1.0f;
   float tEta = mMaterial->getIndexOfRefraction();
   
   if (!mFrontFacing)
      swap(iEta, tEta); // Inside.
   
   float eta = iEta / tEta;
   
   // DONE: Implement refraction.
   // --------
   float r = mView * N;
   float c = 1 - ((eta * eta) * (1- (r*r)));

   Vector3D T;
   
   if(c < 0)
      //return the reflection ray instead
      T = 2*(N*mView)*N - mView;   
   else
      //calculate and return the refraction ray
      T = (eta * D)  + ((eta * r - sqrt(c)) * N); // Resulting refraction vector

   T.normalize();
   // --------
   

   float mu = eta*N.dot(D) - N.dot(T);
   
   Ray::Differential dp = calculatePositionDifferential();
   Ray::Differential dd;
   Vector3D dn;
   
   float dmu;
   float dmu0 = (eta - eta*eta*D.dot(N) / T.dot(N));
   
   dn = mObject->calculateNormalDifferential(mPosition, dp.dx, mFrontFacing);
   dmu = dmu0 * (mRay.dd.dx.dot(N) + D.dot(dn));
   dd.dx = eta*mRay.dd.dx - (mu*dn + dmu*N);
   
   dn = mObject->calculateNormalDifferential(mPosition, dp.dy, mFrontFacing);
   dmu = dmu0 * (mRay.dd.dy.dot(N) + D.dot(dn));
   dd.dy = eta*mRay.dd.dy - (mu*dn + dmu*N);
   
   return Ray(mPosition - N*0.001f, T, dp, dd);
}

Ray Intersection::getShadowRay(PointLight *light) const
{
   // Un-normalized light vector - means the vector to the light
   Vector3D lightVec = light->getWorldPosition() - mPosition;
   
   // Length of light vector - we don't want to go beyond this in our shadow ray tests
   //in other words it is the tmax
   float len = lightVec.length();
   
   // Ray from hit point towards the light. Note that lightVec will be normalized by the Ray constructor
   //the ray origin is 
   return Ray(mPosition + mNormal * 0.001f, lightVec, 0.0f, len);
}


Ray Intersection::getShadowRay(AreaLight *light) const
{

   Vector3D sampleDirection;
   
   
   //Un-normalized light vector - means the vector to the light
   //the light's world position is the sample point over the light's surface
   //mPosition is the hit point in the world coordinates
   bool sampleFound = light->calculateSampleDirection(*this,sampleDirection);

   
   //Ray from hit point towards the light. Note that lightVec will be
   //normalized by the Ray constructor
   //the ray origin is 

   //get the length to the sample direction
   //   float length = sampleDirection.length();

   //TEST - does not make any difference in the ARTIFACTS even if we
   //adjust the mPosition
   //TEST - tMin is greater than 0.0f and i got rid of those artifacts
   //as suggested in Peter Shirley's book
   //AT LAST THE FOLLOWING ADDITION of mNormal and the 0.001
   //HELP ME TO GET AWAY FROM THOSE CRAZY ARTIFACTS
   //HORRAH!!!!!
   return Ray(mPosition + mNormal  * 0.001f, sampleDirection, 1e-4f);      
}


