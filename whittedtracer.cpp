/*
 *  whittedtracer.cpp
 *  asrTracer
 *
 *  Created by Petrik Clarberg on 2006-03-10.
 *  Copyright 2006 Lund University. All rights reserved.
 *
 */
#include <omp.h>

#include "defines.h"
#include "scene.h"
#include "camera.h"
#include "ray.h"
#include "intersection.h"
#include "material.h"
#include "whittedtracer.h"
#include "timer.h"
#include "image.h"
#include "randomgenerator.h" 
#include "jittered.h"
/**
 * Creates a Whitted raytracer. The parameters are passed on to the base class constructor.
 */
WhittedTracer::WhittedTracer(Scene* scene, Image* img) : Raytracer(scene,img)
{
}


WhittedTracer::~WhittedTracer()
{
}

/**
 * Raytraces the scene by calling tracePixel() for each pixel in the output image.
 * The function tracePixel() is responsible for computing the output color of the
 * pixel. This should be done in a sub class derived from the Raytracer base class.
 */
void WhittedTracer::computeImage()
{
   std::cout << "Raytracing..." << std::endl;
   Timer timer;
   
   Color c;
   int width = mImage->getWidth();
   int height = mImage->getHeight();
   
   // Loop over all pixels in the image
#pragma omp parallel for schedule(dynamic,1), private(c)
   for (int y = 0; y < height; y++)
   {
      for (int x = 0; x < width; x++)
      {
	 // Raytrace the pixel at (x,y).
	 c = tracePixel(x,y);
         
	 // Store the result in the image.
	 mImage->setPixel(x,y,c);
      }
      
#pragma omp critical
      // Print progress approximately every 5%.
      if ((y+1) % (height/20) == 0 || (y+1) == height)
         std::cout << (100*(y+1)/height) << "% done" << std::endl;
   }
   
   std::cout << "Done in " << timer.stop() << " seconds" << std::endl;
}

/**
 * Compute the color of the pixel at (x,y) by raytracing. 
 * The default implementation here just traces through the center of
 * the pixel.
 */
Color WhittedTracer::tracePixel(int x, int y)
{
   /*
     Color pixelColor = Color(0.0f, 0.0f, 0.0f);
     
     float cx = (float)x + 0.5f;
     float cy = (float)y + 0.5f;
     
     // Let the camera setup the ray.
     Ray ray = mCamera->getRay(cx,cy);
     
     return trace(ray, 0);
   */
   
   /*
     return superSampling(x,y,20);
   */
   
   Color pixelColor;
   Point3D samplePoint;
   
   float px,py;
   Ray ray;
   
   unsigned int numSamples = mSampler->getNumberSamples();

   /*
     for all the sample points inside the pixel's
     unit square arae
    */
   for(unsigned int i = 0; i < numSamples; i++)
   {
      /*
        each call to the following function returns a new
        position within the unit square pixel
      */
      samplePoint = mSampler->sampleUnitSquare();
      
      //update the pixel point
      px = static_cast<float>(x) + samplePoint.x;
      py = static_cast<float>(y) + samplePoint.y;
      
      //get the ray at this position 
      ray = mCamera->getRay(px,py);
      
      //trace the ray
      pixelColor += trace(ray,4);
   }
   
  pixelColor /= numSamples;

  return pixelColor;  
}

/**
 * Computes the radiance returned by tracing the ray r.
 */
Color WhittedTracer::trace(const Ray& ray, int depth)
{

  //output color will be returned
  Color outputColor;
  
  //declare an intersection
  //which will have the intersection
  //information of the scene
  //which in turn call the
  //intersection information
  //for the accelerator
  Intersection intersection;
  
  //declare the rays to be traced
  Ray reflectedRay, refractedRay;
  
  Color reflectedColor, refractedColor;
  
  float reflectivityCoefficient = 0.0;
  
  float refractivityCoefficient = 0.0;
  
  //check the intersection and store the
  //intersection information, the following 
  //function in turn call the accelerator's 
  //intersect() function
  bool hit = mScene->intersect(ray,intersection);

  
  if(hit)
    {
      //the nearest hit in the scene
      
      //get the reflectivity coefficient of the nearest hit in the scene object
      reflectivityCoefficient = intersection.mMaterial->getReflectivity(intersection);
      
      //get the refractivity coefficient of the nearest hit in the scene object
      refractivityCoefficient = intersection.mMaterial->getTransparency(intersection);
      
      reflectedRay = intersection.getReflectedRay();
      
      if(depth != 0)
        {
          reflectedColor += trace(reflectedRay, depth - 1);
        }

      refractedRay = intersection.getRefractedRay();
      
      if(depth != 0)
        {
          refractedColor += trace(refractedRay,depth - 1);
        }
      
      
      outputColor =  ((1 - reflectivityCoefficient - refractivityCoefficient) *  computeDirectIllumination(intersection)) + (reflectivityCoefficient * reflectedColor) + (refractivityCoefficient * refractedColor);
    }
  else
    outputColor = mScene->getBackground(ray.dir);
  //outputColor = Color(0.0,0.0,0.0);
  
  return outputColor;
}


///////////////////// THE FOLLOWING ARE ADDED BY THE SAJJAD ///////////////////////
Color WhittedTracer::computeDirectIllumination(const Intersection &intersection)
{
   //the output color will be stored here
   Color outputColor;

   //store the pointer to the point light
   PointLight *pointLight;
   
   Color incomingRadiance,BRDF;
   Vector3D lightVec;
   float incidentAngle;
   Ray shadowRay;
   
   bool shadowHit = false;

   //get the number of lights
   unsigned int numberOfLights = mScene->getNumberOfLights();
   
   for(unsigned int i = 0; i < numberOfLights;++i)
   {
      //get the light - i in  the scene
      pointLight = mScene->getLight(i);

	
      //get the shadow ray
      shadowRay = intersection.getShadowRay(pointLight);

       
      //check if the shadow ray intersect 
      //any intersectables in the scene
      //before reaching the light source
      shadowHit = mScene->intersect(shadowRay);
      
      if(!shadowHit)
      {
	 //get the incoming radiance of the light
	 incomingRadiance = pointLight->getRadiance();
	 
	 //calculate the light vector - the irradiance
	 lightVec = pointLight->getWorldPosition() - intersection.mPosition;
	 lightVec.normalize();
         
	 //calculate the incident angle
	 incidentAngle = std::max(lightVec.dot(intersection.mNormal),0.0f);
         
	 //get the BRDF
	 BRDF = intersection.mMaterial->evalBRDF(intersection,lightVec);
         
	 //accumulate the color for all the lights 
	 outputColor += incomingRadiance * BRDF * incidentAngle;
      }
   }
   
   return outputColor;
}


Color WhittedTracer::superSampling(int x,int y,unsigned int N)
{
  //declare a local ray 
  Ray localRay;
  
  //holds the accumulated pixel color
  Color pixelColor;
  
  unsigned int numTraceCalls = 0;
  
  RandomGenerator random = RandomGenerator::getRandomInstance();
  
  //define the step of the iterator
  double step = 1/static_cast<double>(N);
  
  //pixel position at (x,y) will be fragmented
  //so we declare different variable for each of them
  double fragmentX = static_cast<double>(x);
  double fragmentY = static_cast<double>(y);

  double r = 0.0;
  
  //declare variable to hold the 
  //fragmented pixel position
  double px = 0.0;
  double py = 0.0;
  
  for(unsigned int i = 0; i < N; ++i)
    {
      fragmentX = static_cast<double>(x);
      
      for(unsigned int j = 0; j < N;++j )
        {
          //generate the random number 
          //within the range [0,step)
          r = random.generateDouble(0,step);
          
          //following the stratified random sampling
          //the initial position is updated with the 
          //updated randomized position
          px = fragmentX + r;
          py = fragmentY + r;
          
          //now shoot ray through this position
          localRay = mCamera->getRay(static_cast<float>(px),static_cast<float>(py));
          
          pixelColor += trace(localRay,3);
          
          numTraceCalls++;
          
          //go to the next fragment
          fragmentX += step;
        }
      
      fragmentY += step;
    }
  
  
  float numSamples = static_cast<float>(N*N);
  
  pixelColor /= numSamples;

  //std::cout << pixelColor << std::endl;
  
  return pixelColor;
}
//////////////////////////////////////////////////////////////////////////////////   
