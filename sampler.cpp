#include "sampler.h"

#include "randomgenerator.h"

Sampler::Sampler()
   :mNumSamples(1), //take only one sample per pixel
    mNumSets(83),
    count(0),
    jump(0)
{
  //reserve the size for the number of samples
  mSamples.reserve(mNumSamples * mNumSets);
  
  setupShuffledIndices();
}

Sampler::Sampler(unsigned int num)
  :mNumSamples(num), //take only one sample per pixel
   mNumSets(83),
   count(0),
   jump(0)   
{
  //reserve the size for the number of samples
  mSamples.reserve(mNumSamples * mNumSets);

  setupShuffledIndices();
}

Sampler::Sampler(unsigned int num,unsigned int num_sets)
  :mNumSamples(num), //take only one sample per pixel
   mNumSets(num_sets),
   count(0),
   jump(0)      
{
  //reserve the size for the number of samples
  mSamples.reserve(mNumSamples * mNumSets);
  
  setupShuffledIndices();
}

Sampler::Sampler(const Sampler& s)
  : mNumSamples(s.mNumSamples),
    mNumSets(s.mNumSets),
    mSamples(s.mSamples),
    mShuffledIndices(s.mShuffledIndices),
    count(s.count),
    jump(s.jump)
{
  //reserve the size for the number of samples
  mSamples.reserve(mNumSamples * mNumSets);
  
  setupShuffledIndices();
}

Sampler& Sampler::operator= (const Sampler& rhs)
{
  
  if(this == &rhs)
    return (*this);
  
  mNumSamples = rhs.mNumSamples;
  mNumSets = rhs.mNumSets;
  mSamples = rhs.mSamples;
  mShuffledIndices = rhs.mShuffledIndices;
  count = rhs.count;
  jump = rhs.jump;
  
  return (*this);
}

Sampler::~Sampler(void)
{
  
}

void Sampler::setNumSets(int numSets)
{
  mNumSets = numSets;
}

//get the number of samples
unsigned int Sampler::getNumberSamples() const
{
  return mNumSamples;
}


//setup the randomly shuffled indices
void Sampler::setupShuffledIndices()
{
  mShuffledIndices.reserve(mNumSamples * mNumSets);
  
  vector<unsigned int> indices;
  
  /*
    insert the indices into the vector indices
  */
  for(unsigned int j = 0; j < mNumSamples;j++)
    indices.push_back(j);
  
  
  for(unsigned int p = 0; p < mNumSets;p++)
    {
      std::random_shuffle(indices.begin(),indices.end());
      
      for(unsigned int j = 0; j < mNumSamples;j++)
        mShuffledIndices.push_back(indices[j]);
    }
}


//get the next sample on the unit square
Point3D Sampler::sampleUnitSquare()
{  
  RandomGenerator random = RandomGenerator::getRandomInstance();

  ////////////////////////////////////////////////////////////////////////////////
  //unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();

  //std::uniform_int_distribution<unsigned int> distribution(0,mNumSets - 1);
  //std::minstd_rand0 generator(seed);
  ////////////////////////////////////////////////////////////////////////////////
  
  if(count % mNumSamples == 0)
    jump = (static_cast<unsigned int>(random.generateInt()) % mNumSets) * mNumSamples;
    //jump = distribution(generator) * mNumSamples;
  
  unsigned int index =  jump + mShuffledIndices[(jump + count++) % mNumSamples];
  
  return mSamples[index];
}

// get next sample on unit disk
Point3D Sampler::sampleUnitDisk()
{
  RandomGenerator random = RandomGenerator::getRandomInstance();

  ////////////////////////////////////////////////////////////////////////////////
  //unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  
  //std::uniform_int_distribution<unsigned int> distribution(0,mNumSets - 1);
  //std::minstd_rand0 generator(seed);
  ////////////////////////////////////////////////////////////////////////////////
  // start of a new pixel
  if (count % mNumSamples == 0)  									
    jump = (static_cast<unsigned int>(random.generateInt()) % mNumSets) * mNumSamples;
    //jump = distribution(generator) * mNumSamples;
  
  unsigned int index =  jump + mShuffledIndices[(jump + count++) % mNumSamples];
  
  return mDiskSamples[index];
}



// get the next sample on the unit hemisphere
Point3D Sampler::sampleUnitHemisphere()
{
  RandomGenerator random = RandomGenerator::getRandomInstance();

  /////////////////////////////////////////////////////////////////////////////
  //unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  
  //std::uniform_int_distribution<unsigned int> distribution(0,mNumSets - 1);
  //std::minstd_rand0 generator(seed);
  /////////////////////////////////////////////////////////////////////////////
  
  if (count % mNumSamples == 0)	// start of a new pixel
    jump = (static_cast<unsigned int>(random.generateInt()) % mNumSets) * mNumSamples;
  //jump = distribution(generator) * mNumSamples;
  
  unsigned int index = jump + mShuffledIndices[(jump + count++) % mNumSamples];
  
  return mHemisphereSamples[index];
}


/*
  the mapping is independant of the sampling technique used
*/
void Sampler::mapSamplesToUnitDisk(void)
{
  int size = mSamples.size();
  float r, phi;		// polar coordinates
  Point3D sp; 		// sample point on unit disk
   
  mDiskSamples.reserve(size);
   
  for (int j = 0; j < size; j++) 
    {
      // map sample point to [-1, 1] X [-1,1]
      
      sp.x = 2.0 * mSamples[j].x - 1.0;	
      sp.y = 2.0 * mSamples[j].y - 1.0;
      
      if (sp.x > -sp.y) 
        {			// sectors 1 and 2
          if (sp.x > sp.y) 
            {		// sector 1
              r = sp.x;
              phi = sp.y / sp.x;
            }
          else 
            {					// sector 2
              r = sp.y;
              phi = 2 - sp.x / sp.y;
            }
        }
      else 
        {						// sectors 3 and 4
          if (sp.x < sp.y) 
            {		// sector 3
              r = -sp.x;
              phi = 4 + sp.y / sp.x;
            }
          else 
            {					// sector 4
              r = -sp.y;
              if (sp.y != 0.0)	// avoid division by zero at origin
                phi = 6 - sp.x / sp.y;
              else
                phi  = 0.0;
            }
        }
      
      phi *= M_PI / 4.0;
      
      ////////// ADDED BY SAJJAD /////////////
      sp.x = r * cos(phi);
      sp.y = r * sin(phi);
      ///////////////////////////////////////
      
      /*
	mDiskSamples[j].x = r * cos(phi);
	mDiskSamples[j].y = r * sin(phi);
      */
      
      mDiskSamples.push_back(sp);      
    }
	
  /*
    we do not need the samples any more 
    since from now on we shall only be using 
    the disk samples
  */
  mSamples.erase(mSamples.begin(), mSamples.end());
}


// map_samples_to_hemisphere

// Maps the 2D sample points to 3D points on a unit hemisphere with a cosine power
// density distribution in the polar angle
void Sampler::mapSamplesToHemisphere(float exp)
{
  //get the size of the samples that we initially generated
  unsigned int size = mSamples.size();
  
  //reserve space for the hemispherical samples
  mHemisphereSamples.reserve(mNumSamples * mNumSets);
  
  for (unsigned int j = 0; j < size; j++) 
    {
      float cos_phi = cos(2.0 * M_PI * mSamples[j].x);
      float sin_phi = sin(2.0 * M_PI * mSamples[j].x);	
      float cos_theta = pow((1.0 - mSamples[j].y), 1.0 / (exp + 1.0));
      float sin_theta = sqrt(1.0 - cos_theta * cos_theta);
      
      float pu = sin_theta * cos_phi;
      float pv = sin_theta * sin_phi;
      float pw = cos_theta;
      
      mHemisphereSamples.push_back(Point3D(pu, pv, pw)); 
    }
}

//the mapping is over the unit sphere at the origin
void Sampler::mapSamplesToSphere(void)
{
   float r1, r2;
   float x, y, z;
   float r, phi;

   //reserve the space for the spherical samples
   mSphereSamples.reserve(mNumSamples * mNumSets);   
   
   for (int j = 0; j < mNumSamples * mNumSets; j++)
     {
       //get the original sample points
       r1  = mSamples[j].x;
       r2  = mSamples[j].y;
       
       z   = 1.0 - 2.0 * r1;
       r   = sqrt(1.0 - z * z);
       phi = 2 * M_PI * r2;
       x   = r * cos(phi);
       y   = r * sin(phi);
       
       mSphereSamples.push_back(Point3D(x, y, z)); 
     }   
}

Point3D Sampler::sampleSphere()
{
  /////////////////////////////////////////////////////////////    
  RandomGenerator random = RandomGenerator::getRandomInstance();

  //////////////////////////////////////////////////////////////////////////////
  //unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
  
  //std::uniform_int_distribution<unsigned int> distribution(0,mNumSets - 1);
  //std::minstd_rand0 generator(seed);
  ///////////////////////////////////////////////////////////////////////////////

  // start of a new pixel
  if (count % mNumSamples == 0) 
    jump = (static_cast<unsigned int>(random.generateInt()) % mNumSets) * mNumSamples;
  //jump = distribution(generator) * mNumSamples;
    
  unsigned int index =  jump + mShuffledIndices[(jump + count++) % mNumSamples];
  
  return mSphereSamples[index];  
}


