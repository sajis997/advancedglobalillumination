#ifndef SAMPLER_H
#define SAMPLER_H

#include "defines.h"
#include "matrix.h"

using namespace std;

class Sampler
{

protected:
   //number of sample points in the pattern
   unsigned int mNumSamples;
   //number of sample sets stored
   unsigned int mNumSets;

   //sample points on a unit square
   vector<Point3D> mSamples;

   //shuffled sampled array indices
   vector<unsigned int> mShuffledIndices;


   //sample points on the unit disk
   vector<Point3D> mDiskSamples;

   vector<Point3D> mHemisphereSamples;

   //transform the unit square samples to unit sphere
   //store them all in the following vector structure
   vector<Point3D> mSphereSamples;

   //the current number of sample points used
   unsigned int count;

   //random index jump
   unsigned int jump;
   
public:   
   Sampler();
   Sampler(unsigned int num);
   Sampler(unsigned int num,unsigned int num_sets);

   Sampler(const Sampler& s);						
   
   Sampler& operator= (const Sampler& rhs);				
   
   virtual ~Sampler(void);

   void setNumSets(int numSets);

   //get the number of samples
   unsigned int getNumberSamples() const;
   
   //will be over-ridden by one of the sub-classes
   virtual void generateSamples() = 0;

   //setup the randomly shuffled indices
   void setupShuffledIndices();

   //get the next sample on the unit square
   Point3D sampleUnitSquare();


   // get next sample on unit disk
   Point3D sampleUnitDisk();

   // get the next sample on the unit hemisphere
   Point3D sampleUnitHemisphere();


   Point3D sampleSphere();
   
   /*
     the mapping is independant of the sampling technique used
     check the listing 10.2 for more
   */
   void mapSamplesToUnitDisk(void);


   void mapSamplesToHemisphere(float);

   void mapSamplesToSphere(void);

};


#endif
