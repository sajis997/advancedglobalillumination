#ifndef BVH_ACCELERATOR_H
#define BVH_ACCELERATOR_H


#include "defines.h"
#include "rayaccelerator.h"
#include "bvhprimitiveinfo.h"


using namespace std;

class LinearBVHNode;
class HitPoint;
class HitPointInfo;


class BVHAccelerator : public RayAccelerator
{
private:
   
   std::vector<BVHPrimitiveInfo*> m_objects;

   //list of linearized node tree
   std::vector<LinearBVHNode*> m_nodeList;


   //list of hit points
   std::vector<HitPoint*> m_hitPoints;

   //pointer to the root of the  BVH
   LinearBVHNode *root;

   unsigned int m_maxPrimitivePerNode;

   unsigned int m_numberNodes;

   enum SplitMethod
   {
       SPLIT_MIDDLE,
       SPLIT_EQUAL_COUNTS,
       SPLIT_SAH
   };


   SplitMethod m_splitMethod;

   unsigned int build_recursive(unsigned int left_index,unsigned int right_index,int depth);

   unsigned int build_recursive_hit_points(unsigned int left_index,unsigned int right_index,int depth);
   
   void print_rec(LinearBVHNode *node,int depth);

public:

   BVHAccelerator(unsigned int = 4, const string& = "sah");

   ~BVHAccelerator();

   virtual void build(const std::vector<Intersectable*> &objects);

   void buildForHitPoints(const std::vector<HitPoint*> &hitPoints);

   //to find the nearest hitpoints of the photon intersection
   //we send the following inpute paremeters
   //1st - the photon intersection information
   //2nd - vector that stores the integer index of 
   //the hitpoint in the hit point array
   bool foundNearestHitPoints(const Intersection&,
			      const float &searchRadius,
			      vector<HitPoint*>&);
   
   virtual bool intersect(const Ray &ray);
   virtual bool intersect(const Ray &ray,Intersection &intersection);


   //some debug information
   void print();
};

#endif
